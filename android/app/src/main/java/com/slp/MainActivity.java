package com.slp;

import android.os.Bundle;
import com.facebook.react.ReactActivity;
import org.devio.rn.splashscreen.SplashScreen;
import android.content.Intent;
import android.content.res.Configuration;

public class MainActivity extends ReactActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme); // Now set the theme from Splash to App before setContentView
        setContentView(R.layout.launch_screen); // Then inflate the new view
        SplashScreen.show(this, R.style.SplashScreenTheme); // here
        super.onCreate(savedInstanceState);
    }

    @Override
   public void onConfigurationChanged(Configuration newConfig) {
               super.onConfigurationChanged(newConfig);
               Intent intent = new Intent("onConfigurationChanged");
               intent.putExtra("newConfig", newConfig);
               this.sendBroadcast(intent);
           }

    @Override
    public void onNewIntent(Intent intent) {
        setIntent(intent);
        super.onNewIntent(intent);
    }

    @Override
    protected String getMainComponentName() {
        return "slp";
    }
}
