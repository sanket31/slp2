package com.slp;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.helpshift.reactlibrary.RNHelpshiftPackage;
import com.brentvatne.react.ReactVideoPackage;
import com.i18n.reactnativei18n.ReactNativeI18n;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.freshchat.consumer.sdk.react.RNFreshchatSdkPackage;
import io.invertase.firebase.RNFirebasePackage;
import com.rumax.reactnative.pdfviewer.PDFViewPackage;
import org.wonday.orientation.OrientationPackage;
import com.reactnativecommunity.netinfo.NetInfoPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.imagepicker.ImagePickerPackage;
import com.react.rnspinkit.RNSpinkitPackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import org.reactnative.camera.RNCameraPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import org.devio.rn.splashscreen.SplashScreenReactPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.agontuk.RNFusedLocation.RNFusedLocationPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      
      return Arrays.<ReactPackage>asList(new MainReactPackage(),
            new RNHelpshiftPackage(),
            new ReactVideoPackage(),
            new ReactNativeI18n(),
            new RNGestureHandlerPackage(),
            new RNFreshchatSdkPackage(),
            new RNFirebasePackage(),
            new RNFirebaseMessagingPackage(),
            new RNFirebaseNotificationsPackage(),
            new PDFViewPackage(),
            new RNFusedLocationPackage(),
            new OrientationPackage(),
            new NetInfoPackage(),
            new RNCWebViewPackage(),
            new RNDeviceInfo(),
            new ImagePickerPackage(),
            new RNSpinkitPackage(),
            new AsyncStoragePackage(),
            new RNCameraPackage(),
            new VectorIconsPackage(), 
            new SplashScreenReactPackage());
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
