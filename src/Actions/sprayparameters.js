import {
  SET_BASIC_SETTINGS,
  SET_LOADER,
  GET_RECOMMENDATION,
  SET_RECOMMENDATION_ERROR,
  SET_ADVANCE_SETTINGS,
  SAVE_QUESTION_ONE
} from "./types";
import { TechSupportApi } from "../config/api";
import axios from "axios";
export const setBasicSettings = value => {
  return dispatch =>
    dispatch({
      type: SET_BASIC_SETTINGS,
      payload: value
    });
};

export const setAdvanceSettings = value => {
  return dispatch =>
    dispatch({
      type: SET_ADVANCE_SETTINGS,
      payload: value
    });
};
export const setLoader = value => {
  return dispatch =>
    dispatch({
      type: SET_LOADER,
      payload: value
    });
};
export const getRecommendation = (access_token, value, navigation) => {
  return async dispatch => {
    const sort_value = value.sort((a, b) => a.qid - b.qid)
    const data = {
      access_token: access_token,
      options: sort_value
    };
    console.log(data);
    await axios({
      url: TechSupportApi.getRecommendationData,
      method: "post",
      data: data,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        console.log('response....', res)
        if (res.status === 200) {

          dispatch({
            type: GET_RECOMMENDATION,
            payload: res.data.data
          });
          navigation.navigate("GetRecommendation");
        } else {
          dispatch({
            type: SET_RECOMMENDATION_ERROR,
            payload: res.message
          });
        }
      })
      .catch(err => {
        console.log("recommendation response error", err);
        dispatch({
          type: SET_RECOMMENDATION_ERROR,
          payload: err.message
        });
      });
  };
};
export const saveQuestionOne = value => {
  return dispatch =>
    dispatch({
      type : SAVE_QUESTION_ONE,
      payload: value
    });
};