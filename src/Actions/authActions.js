import { Alert } from "react-native";
import axios from "axios";
import i18n from "i18next";
import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER,
  REGISTER_USER,
  REGISTER_USER_FAIL,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_PROFILE,
  NAME_CHANGED,
  LAST_NAME_CHANGED,
  NUMBER_CHANGED,
  COMPANY_NAME_CHANGED,
  ADD1_CHANGED,
  ADD2_CHANGED,
  CITY_CHANGED,
  STATE_CHANGED,
  ZIP_CHANGED,
  REFERRAL_CHANGED,
  SHOW_AUTH_MODAL,
  HIDE_AUTH_MODAL,
  TERMS_CHECKED,
  TERMS_CHECKED_FALSE,
  EMAIL_LOG_CHANGED,
  PASS_LOG_CHANGED,
  COMPANY_LIST,
  RESET_AUTH_DATA,
  AUTH_TOKEN_DATA
} from "./types";
import AsyncStorage from "@react-native-community/async-storage";
import { BASE_URL, authApi } from "../config/api";

export const resetData = () => {
  return dispatch => dispatch({
    type: RESET_AUTH_DATA,
  });
};

export const saveTokenData = (tokenData) => ({
  type: AUTH_TOKEN_DATA,
  payload: tokenData
});

export const showAuthModal = () => dispatch =>
  dispatch({
    type: SHOW_AUTH_MODAL,
  });

export const hideAuthModal = () => dispatch =>
  dispatch({
    type: HIDE_AUTH_MODAL,
  });

export const emailChanged = text => ({
  type: EMAIL_CHANGED,
  payload: text,
});

export const emailLogChanged = text => ({
  type: EMAIL_LOG_CHANGED,
  payload: text,
});

export const passwordLogChanged = text => ({
  type: PASS_LOG_CHANGED,
  payload: text,
});

export const passwordChanged = text => ({
  type: PASSWORD_CHANGED,
  payload: text,
});

export const nameChanged = text => ({
  type: NAME_CHANGED,
  payload: text,
});
export const lastNameChanged = text => ({
  type: LAST_NAME_CHANGED,
  payload: text,
});

export const numberChanged = text => ({
  type: NUMBER_CHANGED,
  payload: text,
});

export const companyNameChanged = text => ({
  type: COMPANY_NAME_CHANGED,
  payload: text,
});

export const addr1Changed = text => ({
  type: ADD1_CHANGED,
  payload: text,
});

export const addr2Changed = text => ({
  type: ADD2_CHANGED,
  payload: text,
});

export const cityChanged = text => ({
  type: CITY_CHANGED,
  payload: text,
});

export const stateChanged = text => ({
  type: STATE_CHANGED,
  payload: text,
});

export const zipChanged = text => ({
  type: ZIP_CHANGED,
  payload: text,
});

export const referralChanged = text => ({
  type: REFERRAL_CHANGED,
  payload: text,
});

export const toggleCheckBox = () => ({
  type: TERMS_CHECKED,
});

export const unCheckBox = () => ({
  type: TERMS_CHECKED_FALSE,
});

// const referralGen = (email, num) => {
//   const alpha = email
//     .match(/[^\W_]/gm)
//     .slice(0, 3)
//     .join("")
//     .toLowerCase();
//   const numero = num.slice(num.length - 3, num.length);
//   console.log(alpha, numero);
//   return alpha + numero;
// };

export const getCompanyNames = () => {
  return dispatch => {
    axios
      .get(BASE_URL + authApi.companyList)
      .then(res => {
        const list = res.data.data.company_names;
        console.log("companniess list", res.data);
        dispatch({
          type: COMPANY_LIST,
          payload: list,
        });
      })
      .catch(err => {
        console.log("error", err);
      });
  };
};

export const loginUser = (
  email,
  password,
  uniqueId,
  deviceOS,
  fcmToken,
  navigation
) => {
  console.log("STAE", email, password);
  return dispatch => {
    dispatch({
      type: LOGIN_USER,
    });
    const data = {
      email: email,
      password: password,
      device_id: uniqueId,
      device_type: deviceOS,
      device_fcm_token: fcmToken,
    };
    axios
      .post(BASE_URL + authApi.login, data, {
        headers: {
          "Content-Type": "application/json",
          'Accept-Language': i18n.language.includes('es') ? 'es' : null
        },
      })
      .then(async (res) => {
        console.log("Login Res", res);
        AsyncStorage.setItem("userAccessToken", res.data.data.access_token);
        AsyncStorage.setItem("userIdToken", (res.data.data.user_id).toString());
       await dispatch({
          type: LOGIN_USER_SUCCESS,
          payload: res.data.data,
        });
        // setTimeout(() => {
          navigation.navigate("Main");
        // }, 200);
      })
      .catch(error => {
        console.log("ERROr Login", error.response);
        dispatch({
          type: LOGIN_USER_FAIL,
          payload: error.response !== undefined ? error.response.data.message : 'NETWORK ERROR' 
        });
      });
  };
};

export const registerUser = (
  name,
  lastname,
  email,
  number,
  companyName,
  password,
  add1,
  add2,
  city,
  state,
  zip,
  referral
) => dispatch => {
  dispatch({
    type: REGISTER_USER,
  });
  const data = {
    full_name: name +  ' ' + lastname,
    email,
    password,
    phone: number,
    company_name: companyName,
    address: {
      add_line1: add1,
      add_line2: add2,
      city,
      state,
      country: "USA",
      zip_code: zip,
    },
  };
  const dataWithRef = {
    ...data,
    referred_by: referral
  };
  axios
    .post(BASE_URL + authApi.register, referral === '' ? data : dataWithRef, {
      headers: {
        'Accept-Language': i18n.language.includes('es') ? 'es' : null
      }
    })
    .then(res => {
      console.log("register response", res);
      dispatch({
        type: REGISTER_USER_SUCCESS,
      });
    })
    .catch(err => {
      console.log(err, "errr");
      dispatch({
        type: REGISTER_USER_FAIL,
        payload: err.response !== undefined ? err.response.data.message : 'NETWORK ERROR' 
      });
    });
};
