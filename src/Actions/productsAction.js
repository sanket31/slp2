import axios from "axios";
import {
  PRODUCTS_LIST_DATA,
  PRODUCTS_LIST_ERROR,
  PRODUCT_DETAIL_FETCHED,
  PRODUCT_DETAIL_ERROR,
  RESET_PRODUCT_DETAIL,
} from "./types";
import { ProductsApi } from "../config/api";
import i18n from "i18next";

export const fetchProductsList = access_token => {
  return async dispatch => {
    await axios
      .get(ProductsApi.productList(access_token), {
        headers: {
          "Accept-Language": i18n.language.includes("es") ? "es" : null,
        },
      })
      .then(productListRes => {
        console.log("Product List", productListRes.data);
        dispatch({
          type: PRODUCTS_LIST_DATA,
          payload: productListRes.data.data,
        });
      })
      .catch(productListErr => {
        console.log("productListErr", productListErr.response);
        dispatch({
          type: PRODUCTS_LIST_ERROR,
          payload:
            productListErr.response !== undefined
              ? productListErr.response.data.message
              : "NETWORK ERROR",
        });
      });
  };
};

export const fetchProductDetails = (productId, access_token) => {
  return async dispatch => {
    await axios
      .get(ProductsApi.productDetail(productId, access_token), {
        headers: {
          "Accept-Language": i18n.language.includes("es") ? "es" : null,
        },
      })
      .then(productDetailRes => {
        console.log("productDetailRes", productDetailRes.data.data);
        dispatch({
          type: PRODUCT_DETAIL_FETCHED,
          payload: productDetailRes.data.data,
        });
      })
      .catch(productDetailErr => {
        console.log("productDetailErr", productDetailErr);
        dispatch({
          type: PRODUCT_DETAIL_ERROR,
          payload: productDetailErr.response.data,
        });
      });
  };
};

export const resetProductDetail = () => {
  return {
    type: RESET_PRODUCT_DETAIL,
  };
};
