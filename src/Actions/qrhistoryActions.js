import axios from "axios";
import {
  QRHISTORY_LIST_DATA,
  QRHISTORY_LIST_ERROR,
  QRHISTORY_LOADER,
  SELECT_HISTORY_DETAILS,
  CLEAR_SELECT_HISTORY_DETAILS
} from "./types";
import { QrHistoryApi } from "../config/api";
import i18n from "i18next";

export const fetchQrHistoryList = access_token => {
    return async dispatch => {
      dispatch({
        type: QRHISTORY_LOADER
      });
      await axios
        .get(QrHistoryApi.qrHistoryList(access_token),{
          headers: {
          'Accept-Language': i18n.language.includes('es') ? 'es' : null
          }
        })
        .then(qrhistorylist => {
          console.log("qrhistoryList....", qrhistorylist.data.data.qr_history);
          dispatch({
            type: QRHISTORY_LIST_DATA,
            payload: qrhistorylist.data.data.qr_history,
          });
        })
        .catch(qrHistoryErr => {
          console.log("qrHistoryErr...", qrHistoryErr.response)
          dispatch({
            type: QRHISTORY_LIST_ERROR,
            payload: qrHistoryErr.response !== undefined ? qrHistoryErr.response.data.message : 'NETWORK ERROR' 
          });
        });
    };
  };
  export const selectHistoryList = (data) => {
    return dispatch =>
      dispatch({
        type: SELECT_HISTORY_DETAILS,
        payload : data
      });
  };
  export const clearSelectHistoyList = () => {
    return dispatch => 
    dispatch({
      type: CLEAR_SELECT_HISTORY_DETAILS,
      payload : ''
    });
  }