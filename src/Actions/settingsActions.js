import axios from "axios";
import { JOBLIST_ERROR, GET_JOBLIST, SET_JOBLIST_LOADER } from "./types";
import { settingsApi } from "../config/api";

export const setJoblistLoader = value => ({
  type : SET_JOBLIST_LOADER,
  payload : value
})
export const getAssignedJobList = access_token => {
  return async dispatch => {
    await axios
      .get(settingsApi.getUserJobList(access_token), {
        headers: {
          // "Accept-Language": i18n.language.includes("es") ? "es" : null,
          "Content-Type": "application/x-www-form-urlencoded"
        }
      })
      .then(res => {
        if (res.status === 200) {
          console.log("Success", res);
          dispatch({
            type: GET_JOBLIST,
            payload: res.data.data
          });
        } else {
          console.log("failed", res);
          dispatch({
            type: JOBLIST_ERROR,
            payload: playload.message
          });
        }
      })
      .catch(error => {
        console.log("error", res);
        dispatch({
          type: JOBLIST_ERROR,
          payload:
            error.response !== undefined
              ? error.response.data.message
              : "NETWORK ERROR"
        });
      });
  };
};

export const getJobDetails = (access_token, jobId) => {
  return dispatch => {
    console.log(access_token, jobId);
    axios(settingsApi.getJobDetails(access_token, jobId), {
      method: "GET",
      headers: {
        "Content-type": "application/json"
      }
    })
      .then(res => {
        console.log("JOB DETAILS", res);
        if (res.data.status == "success") {
          dispatch({
            type: "GET_JOBDETAILS",
            payload: res.data.data
          });
        }
      })
      .catch(error => console.log("JOB DETAILS ERROR", error));
  };
};

export const resetJobDetails = () => ({
  type:'RESET_JOBDETAILS'
})
