import axios from "axios";
import { StackActions, NavigationActions } from "react-navigation";
import { create } from "apisauce";
import { Platform } from "react-native";
import i18n from "i18next";
import {
  BARREL_DATA,
  SHOW_MODAL,
  HIDE_MODAL,
  SCAN_USER_FAIL,
  BARREL_SEARCH,
  PRODUCT_ACTIVATION_DATA,
  ACTIVATION_FAIL,
  PRODUCT_SAVE_DATA,
  DONT_SPLIT_DATA,
  DONT_SPLIT_ERROR,
  SPLIT_USER_DATA,
  SPLIT_USER_LIST_ERROR,
  SPLIT_USERS_SUCCESS,
  SPLIT_USERS_FAIL,
  USER_NAME_CHANGED_FOR_SPLIT,
  USER_REWARDS_DATA,
  USER_REWARDS_FAIL,
  GET_REWARDS_LOADER,
  CANCEL_QR,
  CANCEL_QR_SUCCESS,
  CANCEL_QR_FAIL,
  PRODUCT_ACTIVATION_LOADER,
  SPLIT_USERS_LOADER,
  GET_SPLIT_USER_LOADER,
  UPLOAD_PROGRESS_ACTIVATION,
  DASHBOARD_DATA,
  QRHISTORY_LIST_DATA,
  QRHISTORY_LIST_ERROR,
  CHANGE_ROUTE_TECH_SUPPORT
} from "./types";
import { BASE_URL, QRdata, homeApi } from "../config/api";
import { NavigationService } from "../navigator";

const api = create({
  baseURL: BASE_URL
});

const createFormData = (photo, body, is_photo_selected) => {
  console.log("is selected field", is_photo_selected);
  console.log("form data called");
  const data = new FormData();
  if (is_photo_selected === true) {
    console.log("sending image field");
    console.log("photouri.....", photo.uri);
    if (photo !== null) {
      data.append("image", {
        name: "slpImage",
        type: "image/jpeg",
        uri:
          Platform.OS === "android"
            ? photo.uri
            : photo.uri.replace("file://", "")
      });
    }

    Object.keys(body).forEach(key => {
      data.append(key, body[key]);
    });
    return data;
  } else {
    console.log("did not send image field");
    return body;
  }
};

export const fetchQRdata = (
  qrData,
  location,
  access_token,
  navigation
) => async dispatch => {
  dispatch({
    type: BARREL_SEARCH
  });

  const data = {
    access_token,
    QR_ID: qrData.QR_ID,
    MERCHANT_ID: qrData.MERCHANT_ID,
    PRODUCT_ID: qrData.PRODUCT_ID,
    scanned_location: location
  };
  console.log("qrdata.,,,,", data);
  await axios
    .post(BASE_URL + QRdata.scanQR, data, {
      headers: {
        "Accept-Language": i18n.language.includes("es") ? "es" : null
      }
    })
    .then(res => {
      console.log("qr res", res);
      dispatch({ type: BARREL_DATA, payload: res.data.data });
      navigation.navigate("SprayPoints");
    })
    .catch(err => {
      console.log("error QR", err.response.data);
      dispatch({ type: SCAN_USER_FAIL, payload: err.response.data });
    });
};

export const activateProduct = (
  access_token,
  product_id,
  save_and_continue,
  qr_id,
  a_side_batch,
  start_drum_temp,
  a_side_set_temp,
  b_side_set_temp,
  hose_set_temp,
  pressure_set,
  mixing_chamber_size,
  photoData,
  navigation,
  is_photo_selected
) => async dispatch => {
  dispatch({
    type: PRODUCT_ACTIVATION_LOADER
  });
  // const data = new FormData();

  const barrelData = {
    access_token,
    product_id,
    save_and_continue,
    qr_id,
    a_side_batch,
    start_drum_temp,
    a_side_set_temp,
    b_side_set_temp,
    hose_set_temp,
    pressure_set,
    mixing_chamber_size
  };

  const formData = await createFormData(
    photoData,
    barrelData,
    is_photo_selected
  );
  console.log("final Data Sended.......", formData);

  setTimeout(
    () =>
      api
        .post(QRdata.activateProduct, formData, {
          onUploadProgress: e => {
            console.log(e);
            const progress = (e.loaded / e.total) * 100;
            console.log(progress);
            dispatch({
              type: UPLOAD_PROGRESS_ACTIVATION,
              payload: Math.round(progress)
            });
          }
        })
        .then(response => {
          dispatch({
            type: PRODUCT_SAVE_DATA
          });

          dispatch({
            type: QRHISTORY_LIST_DATA,
            payload: response.data.data.rewards_history
          });

          console.log("activate SAVE AND continue", response.data.data);
        })
        .catch(error => {
          console.log("error activate", error.message);
          dispatch({
            type: ACTIVATION_FAIL,
            payload: error.response.data
          });
          dispatch({
            type: QRHISTORY_LIST_ERROR,
            payload: error.response.data
          });
        }),
    1000
  );
};

export const dontSplitAction = (
  access_token,
  product_id,
  save_and_continue,
  qr_id,
  a_side_batch,
  start_drum_temp,
  a_side_set_temp,
  b_side_set_temp,
  hose_set_temp,
  pressure_set,
  mixing_chamber_size,
  photoData,
  navigation,
  user_rewards_id,
  is_photo_selected
) => async dispatch => {
  dispatch({
    type: PRODUCT_ACTIVATION_LOADER
  });
  let barrelData = {
    access_token,
    product_id,
    save_and_continue,
    qr_id,
    a_side_batch,
    start_drum_temp,
    a_side_set_temp,
    b_side_set_temp,
    hose_set_temp,
    pressure_set,
    mixing_chamber_size
  };
  const barrelData2 = {
    ...barrelData,
    user_rewards_id
  };
  if (user_rewards_id) {
    barrelData = barrelData2;
  }
  console.log("sended data dontsplit........", barrelData);

  const formData = createFormData(photoData, barrelData, is_photo_selected);
  setTimeout(
    () =>
      api
        .post(QRdata.activateProduct, formData, {
          onUploadProgress: e => {
            console.log(e);
            const progress = (e.loaded / e.total) * 100;
            console.log(progress);
            dispatch({
              type: UPLOAD_PROGRESS_ACTIVATION,
              payload: Math.round(progress)
            });
          }
        })
        .then(async resAct => {
          console.log("res dontsplit activate.......", resAct.data);
          const data = {
            access_token: access_token,
            product_id: resAct.data.data.product_id,
            user_rewards_id: resAct.data.data.user_rewards_id
          };
          await axios
            .post(BASE_URL + QRdata.dontSplit, data, {
              headers: {
                "Accept-Language": i18n.language.includes("es") ? "es" : null
              }
            })
            .then(async resDontSplit => {
              console.log("response dont split", resDontSplit.data);
              //  getUserRewards(access_token);
              const rewardsDataArray = [];
              console.log("response User Rewards", resDontSplit.data);
              resDontSplit.data.data.rewards_history.forEach(item => {
                if (item.qr_status === "Completed") {
                  rewardsDataArray.push(item);
                }
              });
              dispatch({
                type: DONT_SPLIT_DATA,
                payload: rewardsDataArray
              });
              delete resDontSplit.data.data.rewards_history;
              dispatch({
                type: DASHBOARD_DATA,
                payload: resDontSplit.data.data
              });
            })
            .catch(error => {
              console.log("error dont split", error.response.data);
              dispatch({
                type: DONT_SPLIT_ERROR,
                payload: error.response.data
              });
            });
        })
        .catch(err => {
          console.log("dont spilt error2:", err);
        }),
    1000
  );
};

export const splitGetUserAction = (
  access_token,
  navigation,
  product_id,
  save_and_continue,
  qr_id,
  a_side_batch,
  start_drum_temp,
  a_side_set_temp,
  b_side_set_temp,
  hose_set_temp,
  pressure_set,
  mixing_chamber_size,
  photoData,
  screen,
  user_rewards_id,
  is_photo_selected
) => async dispatch => {
  dispatch({
    type: PRODUCT_ACTIVATION_LOADER
  });
  await axios
    .get(BASE_URL + QRdata.splitGetUser + access_token)
    .then(res => {
      let barrelData = {
        access_token,
        product_id,
        save_and_continue,
        qr_id,
        a_side_batch,
        start_drum_temp,
        a_side_set_temp,
        b_side_set_temp,
        hose_set_temp,
        pressure_set,
        mixing_chamber_size
      };
      let barrelData2 = {
        ...barrelData,
        user_rewards_id
      };
      if (user_rewards_id) {
        barrelData = barrelData2;
      }
      console.log("Sending Data split.....", barrelData);
      const formData = createFormData(photoData, barrelData, is_photo_selected);
      dispatch({
        type: SPLIT_USER_DATA,
        payload: res.data.data
      });
      if (screen === "ScanScreen") {
        navigation.navigate("SplitScreen", {
          splitActivationData: formData,
          screendata: screen
        });
      } else {
        navigation.navigate("SplitScreenQrHistory", {
          splitActivationData: formData,
          screendata: screen
        });
      }
    })
    .catch(error => {
      console.log("error get user", error.response.data);
      dispatch({
        type: SPLIT_USER_LIST_ERROR,
        payload: error.response.data
      });
    });
};

export const userNameChanged = text => ({
  type: USER_NAME_CHANGED_FOR_SPLIT,
  payload: text
});

export const splitUserSuccess = text => ({
  type: SPLIT_USERS_SUCCESS,
  payload: text
});

export const setDashboardData = text => ({
  type: DASHBOARD_DATA,
  payload: text
});

export const splitUserFail = text => ({
  type: SPLIT_USERS_FAIL,
  payload: text
});

export const submitSplitAction = (
  usersData,
  barrelData,
  access_token,
  splitActivationData
) => async dispatch => {
  dispatch({
    type: SPLIT_USERS_LOADER
  });
  // await axios
  //   .post(BASE_URL + QRdata.activateProduct, splitActivationData, {
  //     headers: {
  //       Accept: "application/json",
  //     },
  //   }
  api
    .post(QRdata.activateProduct, splitActivationData, {
      onUploadProgress: e => {
        console.log(e);
        const progress = (e.loaded / e.total) * 100;
        console.log(progress);
        dispatch({
          type: UPLOAD_PROGRESS_ACTIVATION,
          payload: Math.round(progress)
        });
      }
    })
    .then(async resSplitAct => {
      console.log("response split activation", resSplitAct.data.data);
      let percentData = [];
      usersData.forEach(item => {
        percentData.push({
          user: item.id,
          percentage: parseInt(item.percentage, 10)
        });
      });
      const data = {
        access_token: access_token,
        product_id: resSplitAct.data.data.product_id,
        user_rewards_id: resSplitAct.data.data.user_rewards_id,
        users: percentData
      };
      await axios
        .post(BASE_URL + QRdata.split, data, {
          headers: {
            "Accept-Language": i18n.language.includes("es") ? "es" : null
          }
        })
        .then(responseSplit => {
          console.log("response Split", responseSplit.data);
          // getUserRewards(access_token);
          const rewardsDataArray = [];
          console.log("response User Rewards", responseSplit.data);
          responseSplit.data.data.rewards_history.forEach(item => {
            if (item.qr_status === "Completed") {
              rewardsDataArray.push(item);
            }
          });
          dispatch({
            type: SPLIT_USERS_SUCCESS,
            payload: rewardsDataArray
          });
          delete responseSplit.data.data.rewards_history;
          dispatch({
            type: DASHBOARD_DATA,
            payload: responseSplit.data.data
          });
        })
        .catch(err => {
          console.log("error Split Action", err.response.data);
          dispatch({
            type: SPLIT_USERS_FAIL,
            payload: err.response.data
          });
        });
    })
    .catch(err => {
      console.log("split error2", err);
    });
  // .finally(async () => {
  //   await axios
  //     .get(BASE_URL + homeApi.dashboard + access_token)
  //     .then(dashboardRes => {
  //       console.log("dashboard", dashboardRes);
  //       dispatch({
  //         type: DASHBOARD_DATA,
  //         payload: dashboardRes.data.data,
  //       });
  //     })
  //     .catch(err => {
  //       console.log("dashboard error", err.response.data);
  //     });
  // });
};

export const getUserRewards = access_token => async dispatch => {
  dispatch({
    type: GET_REWARDS_LOADER
  });
  await axios
    .get(BASE_URL + QRdata.userRewards + access_token, {
      headers: {
        "Accept-Language": i18n.language.includes("es") ? "es" : null
      }
    })
    .then(res => {
      const rewardsDataArray = [];
      console.log("response User Rewards", res.data);
      res.data.data.rewards_history.forEach(item => {
        rewardsDataArray.push(item);
      });
      dispatch({
        type: USER_REWARDS_DATA,
        payload: rewardsDataArray
      });
    })
    .catch(err => {
      dispatch({
        type: USER_REWARDS_FAIL,
        payload:
          err.response !== undefined
            ? err.response.data.message
            : "NETWORK ERROR"
      });
    });
};

export const cancelQrRequest = (
  token,
  qr_id,
  screen,
  navigation
) => async dispatch => {
  dispatch({
    type: CANCEL_QR
  });
  const data = {
    access_token: token,
    qr_id: qr_id
  };
  await axios
    .post(BASE_URL + QRdata.cancelQr, data)
    .then(res => {
      console.log("Respornse", res.data);
      dispatch({
        type: CANCEL_QR_SUCCESS
      });
      if (screen === "QrHistory") {
        navigation.popToTop();
      } else {
        NavigationService.navigate("HomeScreen");
      }
    })
    .catch(err => {
      console.log("error response", err.response.data);
      dispatch({
        type: CANCEL_QR_FAIL
      });
    });
};

export const showModal = () => dispatch => dispatch({ type: SHOW_MODAL });

export const hideModal = () => dispatch => dispatch({ type: HIDE_MODAL });

export const changeRouteTechSupport = text => dispatch =>
  dispatch({ type: CHANGE_ROUTE_TECH_SUPPORT, payload: text });
