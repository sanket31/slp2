import React from "react";
import { Modal, View, TouchableOpacity, Image } from "react-native";
import { Images } from "../assets/images";

export default ImageViewer = props => {
  return (
    <Modal visible={props.modalVisible} style={{ flex: 1 }} transparent>
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          zIndex: 1,
          backgroundColor: "rgba(51,51,51,0.7)"
        }}
      >
        <TouchableOpacity
          onPress={() => props.modalHandler()}
          style={{ zIndex: 1, width: "98%", marginBottom: -18 }}
        >
          <Image
            source={Images.close}
            style={{ height: 30, width: 30, alignSelf: "flex-end" }}
            
          />
        </TouchableOpacity>
        <Image
          source={{ uri: props.url }}
          style={{ zIndex: 0, width: "90%", height: 300, alignSelf:'center' }}
        />
      </View>
    </Modal>
  );
};
