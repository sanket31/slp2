import React from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  FlatList,
  ImageBackground,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import { fetchProductsList } from "../../../Actions";
import { connect } from "react-redux";
import { Images } from "../../../assets/images";
import i18n from "../../../i18n";
import NoConnectionModalProduct from "../../../utility/NoConnectionModal";

class Product extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: (
        <Text
          style={{
            color: "white",
            fontSize: 20,
            alignSelf: "center",
            marginLeft: "auto",
            marginRight: "auto",
            fontFamily: "Comfortaa-Bold"
          }}
        >
          {i18n.t("title:products")}
        </Text>
      ),
      headerStyle: {
        backgroundColor: "rgb(41,34,108)",
        borderBottomWidth: 0,
        height: hp("7%"),
        elevation: 0
      }
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      refreshing: false
    };
  }

  async componentDidMount() {
    if (this.props.accessToken) {
      await this.props.fetchProductsList(this.props.accessToken);
    }
  }

  refreshData = async () => {
    if (this.props.accessToken) {
      await this.props.fetchProductsList(this.props.accessToken);
    }
  };

  render() {
    return (
      <ImageBackground
        source={require("../../../assets/loginLayer.jpg")}
        style={{ flex: 1 }}
      >
        <View style={{ alignItems: "center" }}>
          <View style={styles.cardStyle}>
            <NoConnectionModalProduct
              onPress={this.refreshData}
              errorMessage={this.props.productListError}
              isVisible={this.props.productListFail}
            />
            {this.props.productListData.length >0 ? (
              <FlatList
                data={this.props.productListData}
                initialNumToRender={10}
                extraData={this.props.productListData}
                refreshing={this.state.refreshing}
                onRefresh={() => this.refreshData()}
                renderItem={({ item }) => {
                  return (
                    <View style={{ flex: 1 }}>
                      <TouchableOpacity
                        style={{
                          flex: 1,
                          flexDirection: "row"
                        }}
                        onPress={() =>
                          this.props.navigation.navigate("ProductDetails", {
                            productId: item.id
                          })
                        }
                      >
                        <Image
                          source={{ uri: item.image }}
                          style={styles.imageStyle}
                        />
                        <View style={{ flexDirection: "column" }}>
                          <Text style={styles.titleStyle}>
                            {item.product_name}
                          </Text>
                          <Text style={styles.subtitleStyle}>
                            {i18n.t("title:rewardPoints")} : {item.total_points}
                          </Text>
                        </View>
                      </TouchableOpacity>
                      <View style={styles.marginStyle} />
                    </View>
                  );
                }}
                keyExtractor={item => item.id.toString()}
                showsVerticalScrollIndicator={false}
              />
            ) : !this.props.productListData ? (
              <ActivityIndicator
                size={"large"}
                color={"rgb(41,34,108)"}
                style={{ marginTop: "auto", marginBottom: "auto" }}
              />
            ) :(
              <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Text> No products are available at the moment.</Text>
              </View>
            ) }
          </View>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  titleStyle: {
    fontSize: 15,
    fontFamily: "Comfortaa-Bold",
    color: "rgb(51,51,51)",
    marginVertical: 5
  },
  cardStyle: {
    height: "97.2%",
    width: "90%",
    padding: "2%",
    marginBottom: "4%",
    paddingTop: hp("3%"),
    elevation: 6,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2
  },
  imageStyle: {
    height: 75,
    width: 75,
    marginRight: 10,
    marginLeft: 10,
    borderRadius: 10
  },
  subtitleStyle: {
    fontSize: 13,
    fontFamily: "Comfortaa-Bold",
    flexShrink: 1,
    flexWrap: "wrap"
  },
  marginStyle: {
    margin: hp("2%"),
    borderWidth: 0.6,
    opacity: 0.1
  }
});

const mapStateToProps = ({ auth, products }) => {
  const { accessToken } = auth;
  const { productListData, productListError, productListFail } = products;
  return { accessToken, productListData, productListError, productListFail };
};

const actionCreater = {
  fetchProductsList
};

export default connect(mapStateToProps, actionCreater)(Product);
