import React from "react";
import {
  Text,
  View,
  Image,
  FlatList,
  ImageBackground,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity
} from "react-native";
import { Images } from "../../../assets/images";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import { connect } from "react-redux";
import { fetchProductDetails, resetProductDetail } from "../../../Actions";
import { MainStyles } from "../../styles";
import i18n from "../../../i18n";

class ProductDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data1: []
    };
  }

  async componentDidMount() {
    if (this.props.accessToken) {
      await this.props.fetchProductDetails(
        this.props.navigation.state.params.productId,
        this.props.accessToken
      );
    }
  }

  async componentWillUnmount() {
    await this.props.resetProductDetail();
  }

  getPdfName = (fileUri, type) => {
    switch (type) {
      case "technical":
        return fileUri.replace(
          "https://slp-static.s3.amazonaws.com/media/product/tech_data/",
          ""
        );
      case "application":
        return fileUri.replace(
          "https://slp-static.s3.amazonaws.com/media/product/app_data/",
          ""
        );
      case "safety":
        return fileUri.replace(
          "https://slp-static.s3.amazonaws.com/media/product/safety_data/",
          ""
        );
      default:
        return type;
    }
  };

  render() {
    console.log("PROPS", this.props);
    console.log('dta.....', this.props.productDetail)
    return (
      <ImageBackground
        source={require("../../../assets/loginLayer.jpg")}
        style={MainStyles.container}
      >
        <View
          style={[MainStyles.cardStyle, { padding: "4%", paddingTop: "4%" }]}
        >
          {this.props.productDetail ? (
            <ScrollView
              style={{ flex: 1 }}
              showsVerticalScrollIndicator={false}
            >
              <Image
                source={{ uri: this.props.productDetail.image }}
                style={{
                  height: hp("40%"),
                  width: wp("84%"),
                  alignSelf: "center"
                }}
                resizeMode="stretch"
              />
              <View style={{ marginTop: hp("1%") }}>
                <Text style={MainStyles.titleStyle}>{i18n.t("productDetails:productName")}</Text>
                <Text style={MainStyles.subtitleStyle}>
                  {this.props.productDetail.product_name}
                </Text>
                <View style={{ margin: hp("0.5%") }} />
                <View style={MainStyles.borderStyle} />
              </View>

              <View style={{ marginTop: hp("1%") }}>
                <Text style={MainStyles.titleStyle}>{i18n.t("productDetails:manufacturerName")}</Text>
                <Text style={MainStyles.subtitleStyle}>
                  {" "}
                  {this.props.productDetail.full_name}
                </Text>
                <View style={{ margin: hp("0.5%") }} />
                <View style={MainStyles.borderStyle} />
              </View>

              <View style={{ marginTop: hp("1%") }}>
                <Text style={MainStyles.titleStyle}>{i18n.t("productDetails:description")}</Text>
                <Text style={[MainStyles.subtitleStyle, { marginLeft: 5 }]}>
                  {this.props.productDetail.description}
                </Text>
                <View style={{ margin: hp("0.5%") }} />
                <View style={MainStyles.borderStyle} />
              </View>

              <View style={{ marginTop: hp("1%"), marginLeft: "1%" }}>
                {this.props.productDetail.technical_datasheet.length > 0 ||
                this.props.productDetail.application_guideline.length > 0 ||
                this.props.productDetail.safety_datasheet.length > 0 ? (
                  <Text style={[MainStyles.titleStyle, { marginLeft: 0 }]}>
                    {i18n.t("productDetails:guidelines")}
                  </Text>
                ) : null}
                <View style={{ margin: hp("0.2%") }} />
                {this.props.productDetail.technical_datasheet.length > 0 ? (
                  <Text style={MainStyles.subtitleStyle}>
                    {i18n.t("techSupport:techDataSheet")}
                  </Text>
                ) : null}
                <View style={{ margin: hp("0.5%") }} />
                <FlatList
                  data={this.props.productDetail.technical_datasheet}
                  renderItem={({ item }) => (
                    <View style={{ marginBottom: 10 }}>
                      <View
                        style={{
                          width: wp("35%"),
                          margin: hp("1.5%"),
                          marginLeft: 0,
                          marginTop: 0,
                          justifyContent: "center",
                        }}
                      >
                        <TouchableOpacity
                          onPress={() =>
                            this.props.navigation.navigate("PDF", {
                              pdfUrl: item
                            })
                          }
                        >
                          <Image
                            source={Images.pdf}
                            style={MainStyles.productImage}
                            resizeMode="contain"
                          />
                        </TouchableOpacity>
                      </View>
                      <Text style={{ textAlign: 'center', width:wp('20%') }}>
                          {this.getPdfName(item, "technical")}
                        </Text>
                    </View>
                  )}
                  keyExtractor={(item, index) => index.toString()}
                  horizontal
                  showsHorizontalScrollIndicator={false}
                />
                {this.props.productDetail.application_guideline.length > 0 ? (
                  <Text style={MainStyles.subtitleStyle}>
                    {i18n.t("techSupport:appGuideline")}
                  </Text>
                ) : null}
                <View style={{ margin: hp("0.5%") }} />
                <FlatList
                  data={this.props.productDetail.application_guideline}
                  renderItem={({ item }) => (
                    <View style={{ marginBottom: 10 }}>
                      <View
                        style={{
                          width: wp("35%"),
                          margin: hp("1.5%"),
                          marginLeft: 0,
                          marginTop: 0,
                          justifyContent: "center",
                        }}
                      >
                        <TouchableOpacity
                          onPress={() =>
                            this.props.navigation.navigate("PDF", {
                              pdfUrl: item
                            })
                          }
                        >
                          <Image
                            source={Images.pdf}
                            style={MainStyles.productImage}
                            resizeMode="contain"
                          />
                        </TouchableOpacity>
                      </View>
                      <Text style={{ textAlign: 'center', width:wp('20%') }}>
                          {this.getPdfName(item, "application")}
                        </Text>
                    </View>
                  )}
                  keyExtractor={(item, index) => index.toString()}
                  horizontal
                  showsHorizontalScrollIndicator={false}
                />
                <View style={{ margin: hp("0.2%") }} />
                {this.props.productDetail.safety_datasheet.length > 0 ? (
                  <Text style={MainStyles.subtitleStyle}>
                    {i18n.t("techSupport:safetyDataSheet")}
                  </Text>
                ) : null}
                <View style={{ margin: hp("0.5%") }} />
                <FlatList
                  data={this.props.productDetail.safety_datasheet}
                  renderItem={({ item }) => (
                    <View style={{ marginBottom: 10 }}>
                      <View
                        style={{
                          width: wp("35%"),
                          margin: hp("1.5%"),
                          marginLeft: 0,
                          marginTop: 0,
                          justifyContent: "center",
                        }}
                      >
                        <TouchableOpacity
                          onPress={() =>
                            this.props.navigation.navigate("PDF", {
                              pdfUrl: item
                            })
                          }
                        >
                          <Image
                            source={Images.pdf}
                            style={MainStyles.productImage}
                            resizeMode="contain"
                          />
                        </TouchableOpacity>
                      </View>
                      <Text style={{ textAlign: 'center', width:wp('20%') }}>
                          {this.getPdfName(item, "safety")}
                        </Text>
                    </View>
                  )}
                  keyExtractor={(item, index) => index.toString()}
                  horizontal
                  showsHorizontalScrollIndicator={false}
                />
              </View>
            </ScrollView>
          ) : (
            <ActivityIndicator
              size={"large"}
              color={"rgb(41,34,108)"}
              style={{ marginTop: "auto", marginBottom: "auto" }}
            />
          )}
        </View>
      </ImageBackground>
    );
  }
}

const mapStateToProps = ({ auth, products }) => {
  const { accessToken } = auth;
  const { productDetail, productDetailError } = products;
  return { accessToken, productDetail, productDetailError };
};

const actionCreater = {
  fetchProductDetails,
  resetProductDetail
};

export default connect(
  mapStateToProps,
  actionCreater
)(ProductDetails);
