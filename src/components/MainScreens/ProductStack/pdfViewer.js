import React from "react";
import PDFView from "react-native-view-pdf/lib/index";
import { View,BackHandler, ActivityIndicator, Text, TouchableOpacity, Image } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default class PDF extends React.Component {
   

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }

  componentDidMount() {
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.props.navigation.goBack() // works best when the goBack is async
      return true;
    });
  }

  render() {
    const resources = {
      url: this.props.navigation.state.params.pdfUrl,
    };
    const resourceType = "url";
    return (
      <View style={{ flex: 1 }}>
        {/* Some Controls to change PDF resource */}
        {this.state.loading ? (
          <View
            style={{
              marginTop: hp("40%"),
              zIndex: 1,
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <ActivityIndicator size={"large"} color={"rgb(41,34,108)"} />
            <Text>Please Wait..</Text>
          </View>
        ) : null}
        <PDFView
          // fadeInDuration={250.0}
          style={{ flex: 1 }}
          resource={resources[resourceType]}
          resourceType={resourceType}
          onLoad={() => {
            this.setState({ loading: false });
            console.log(`PDF rendered from ${resourceType}`);
          }}
          onError={error => console.log("Cannot render PDF", error)}
        />
      </View>
    );
  }
}
