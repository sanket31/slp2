import React from "react";
import {
  Text,
  View,
  ImageBackground,
  Image,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  ActivityIndicator
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import { Images } from "../../../assets/images";
import { MainStyles } from "../../styles";
import { connect } from "react-redux";
import { fetchQrHistoryList, selectHistoryList } from "../../../Actions/qrhistoryActions";
import moment from "moment";
import { NavigationEvents } from "react-navigation";
import { SelectedQrHistoryApi } from "../../../config/api";
import axios from "axios";
import { NavigationService } from "../../../navigator";
import Modal from "@kalwani/react-native-modal";
import Spinner from "react-native-spinkit";
import i18n from "i18next";
import NoConnectionModalQrHistory from "../../../utility/NoConnectionModal";

class QRHistory extends React.PureComponent {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: (
        <Text
          style={{
            color: "white",
            fontSize: 20,
            alignSelf: "center",
            marginLeft: "auto",
            marginRight: "auto",
            fontFamily: "Comfortaa-Bold"
          }}
        >
          {i18n.t("title:qrHistory")}
        </Text>
      ),
      headerStyle: {
        backgroundColor: "rgb(41,34,108)",
        borderBottomWidth: 0,
        height: hp("7%"),
        elevation: 0
      }
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      handleQrCodeLoader: false,
      qrid: 0,
      refreshing: false,
      langReward: ""
    };
  }
  componentDidMount() {
    this.setState({ langReward: i18n.language });
    // if (this.props.accessToken) {
    //   this.props.dispatch(fetchQrHistoryList(this.props.accessToken));
    // }
  }

  handleQrCode = id => {
    console.log("id...", id);
    this.setState(
      {
        handleQrCodeLoader: true
      },
      () => {
        axios
          .get(
            SelectedQrHistoryApi.selectedQrHistoryApi(
              this.props.accessToken,
              id
            )
          )
          .then(qr_data => {
            console.log('on click qrdata........', qr_data)
            this.props.navigation.navigate("ProductActivationHistory", {
              qrdata: qr_data.data.data
            });
            this.props.dispatch(selectHistoryList(qr_data.data.data))
            this.setState({
              handleQrCodeLoader: false
            });
          })
          .catch(err => {
            console.log(err)
            this.setState({
              handleQrCodeLoader: false
            });
          });
      }
    );
  };

  refreshData = () => {
    if (this.props.accessToken) {
      this.props.dispatch(fetchQrHistoryList(this.props.accessToken));
    }
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={require("../../../assets/loginLayer.jpg")}
          style={{ flex: 1, alignItems: "center" }}
        >
          <NavigationEvents
            onWillFocus={() => {
              if (i18n.language !== this.state.langReward) {
                this.setState({ langReward: i18n.language });
                this.refreshData();
              }
              //Call whatever logic or dispatch redux actions and update the screen!
            }}
          />
          <NoConnectionModalQrHistory
            onPress={this.refreshData}
            errorMessage={this.props.qrHistoryListError}
            isVisible={this.props.qrHistoryFail}
          />
          <View style={styles.cardStyle}>
            {this.props.qrHistoryListData.length > 0 ? (
              <FlatList
                data={this.props.qrHistoryListData}
                extraData={this.props.qrHistoryListData}
                initialNumToRender={5}
                refreshing={this.state.refreshing}
                onRefresh={() => this.refreshData()}
                renderItem={({ item }) => {
                  return (
                    <View style={{ flex: 1 }}>
                      <TouchableOpacity
                        style={{
                          flex: 1,
                          flexDirection: "row"
                        }}
                        onPress={() => {
                          item.qr_status === "Pending" ||
                          item.qr_status == "Pendiente"
                            ? this.handleQrCode(item.id)
                            : () => {};
                        }}
                      >
                        <Image
                          source={{ uri: item.qr_code.product_image }}
                          style={styles.imageStyle}
                        />
                        <View style={{ flexDirection: "column" }}>
                          <Text style={styles.titleStyle}>
                            {item.reward_name}
                          </Text>
                          <View
                            style={{ flexDirection: "row", marginVertical: 2 }}
                          >
                            <Text
                              style={{
                                fontFamily: "Comfortaa-Bold",
                                marginRight: "3%",
                                fontSize: 14
                              }}
                            >
                              {i18n.t("common:status")}:{""}
                            </Text>
                            <Text
                              style={{
                                fontSize: 14,
                                fontFamily: "Comfortaa-Bold",
                                color:
                                  item.qr_status == "Pending" ||
                                  item.qr_status == "Pendiente"
                                    ? "rgb(197,3,1)"
                                    : "rgb(20,203,4)"
                              }}
                            >
                              {item.qr_status}
                            </Text>
                          </View>

                          <View
                            style={{
                              flexDirection: "row",
                              justifyContent: "space-between",
                              marginVertical: 2
                            }}
                          >
                            <Text
                              style={{
                                fontFamily: "Comfortaa-Bold",
                                color: "rgb(102,102,102)",
                                fontSize: 12
                              }}
                            >
                              {moment
                                .utc(item.updated_at)
                                .local()
                                .format("YYYY-MM-DD hh:mm A")}
                            </Text>
                            <Text
                              style={{
                                fontFamily: "Comfortaa-Bold",
                                color: "rgb(102,102,102)",
                                fontSize: 12,
                                marginLeft: wp("10%")
                              }}
                            >
                              {item.reward_points
                                ? `${item.reward_points} PTS`
                                : null}
                            </Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                      <View style={styles.marginStyle} />
                    </View>
                  );
                }}
                keyExtractor={item => item.id.toString()}
                showsVerticalScrollIndicator={false}
              />
            ) : !this.props.qrHistoryListData ? (
              <ActivityIndicator
                size={"large"}
                color={"rgb(41,34,108)"}
                style={{ marginTop: "auto", marginBottom: "auto" }}
              />
            ) : (
              <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Text> {i18n.t("QRHistory:noHistory")} </Text>
              </View>
            )}
            <Modal
              onModalHide={() => {}}
              hideModalContentWhileAnimating
              animationIn={"flash"}
              animationOut={"flash"}
              isVisible={this.state.handleQrCodeLoader}
              style={styles.errorModalStyle}
            >
              <Spinner
                style={styles.spinner}
                isVisible={this.state.handleQrCodeLoader}
                size={50}
                type={"Circle"}
                color={"white"}
              />
            </Modal>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    height: "97.2%",
    width: "90%",
    padding: "2%",
    marginBottom: "4%",
    paddingTop: hp("3%"),
    elevation: 6,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2
  },
  titleStyle: {
    fontSize: 15,
    fontFamily: "Comfortaa-Bold",
    color: "rgb(51,51,51)",
    marginVertical: 2
  },
  imageStyle: {
    height: 75,
    width: 75,
    marginRight: 10,
    marginLeft: 10,
    borderRadius: 10
  },
  subtitleStyle: {
    fontSize: 13,
    fontFamily: "Comfortaa-Bold",
    marginBottom: "2%"
  },
  marginStyle: {
    margin: hp("2%"),
    borderWidth: 0.6,
    opacity: 0.1
  },
  errorModalStyle: {
    justifyContent: "center",
    alignItems: "center"
  }
});
const mapStateToProps = ({ auth, qrhistory, barrel }) => {
  const { accessToken } = auth;
  const {
    qrHistoryListData,
    qrHistoryListError,
    qrHistoryLoader,
    qrHistoryFail

  } = qrhistory;
  const { splitsucess, dontsplitsucess } = barrel;
  return {
    accessToken,
    qrHistoryLoader,
    qrHistoryListData,
    qrHistoryListError,
    splitsucess,
    dontsplitsucess,
    qrHistoryFail
  };
};
export default connect(mapStateToProps)(QRHistory);
