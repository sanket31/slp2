import React from "react";
import {
  Text,
  View,
  ImageBackground,
  StyleSheet,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Alert
} from "react-native";
import Modal from "react-native-modal";
import CheckBox from "react-native-check-box";
import RadioButton from "react-native-radio-button";
import AsyncStorage from "@react-native-community/async-storage";
import axios from "axios";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import { BASE_URL, authApi } from "../../../config/api";
import { connect } from "react-redux";
import { MainRouter, NavigationService } from "../../../navigator";
import { Images } from "../../../assets/images";
import i18n from "i18next";
import { withTranslation } from "react-i18next";
import { Freshchat } from "react-native-freshchat-sdk";
class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      checkedSpanish: false,
      checkedEnglish: false,
      loading: false,
      languageCode: "",
      language: ""
    };
  }

  async componentDidMount() {
    if (i18n.language.includes("es")) {
      this.setState({ checkedSpanish: true });
    } else if (i18n.language.includes("en")) {
      this.setState({ checkedEnglish: true });
    }
  }

  async onChangeLang(lang, changeLng) {
    i18n.changeLanguage(lang);
    try {
      await AsyncStorage.setItem("@APP:languageCode", lang);
    } catch (error) {
      console.log(` Hi Errorrrr : ${error}`);
    }
    console.log(i18n.dir());
    this.setState({ visible: false });
  }

  logOutConfirm = () => {
    Alert.alert(
      i18n.t("settings:signOut"),
      i18n.t("settings:signOutConfirm"),
      [
        {
          text: i18n.t("common:cancel"),
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: i18n.t("splitPoints:yes"),
          onPress: () => this.logOut(this.props.accessToken)
        }
      ],
      { cancelable: true }
    );
  };

  logOut = async userToken => {
    let userKeys = ["userAccessToken", "userIdToken"];
    this.setState({ loading: true });
    const token = {
      access_token: userToken
    };
    if (userToken !== null) {
      await axios
        .post(BASE_URL + authApi.logout, token)
        .then(res => {
          console.log("logout", res);
          this.setState({ loading: false });
          AsyncStorage.multiRemove(userKeys);
          Freshchat.resetUser();
          NavigationService.navigate("Login");
        })
        .catch(err => {
          this.setState({ loading: false });
          console.log("logoutError", err.response.data);
        });
    } else {
      return () => {
        this.setState({ loading: false });
      };
    }
  };

  changeLang = (lang, changeLng) => {
    if (changeLng == "Spanish") {
      this.setState({
        checkedSpanish: true,
        checkedEnglish: false,
        languageCode: lang,
        language: changeLng
      });
    }
    if (changeLng == "English") {
      this.setState({
        checkedEnglish: true,
        checkedSpanish: false,
        languageCode: lang,
        language: changeLng
      });
    }
  };

  render() {
    const { t, i18n, navigation } = this.props;

    return (
      <View style={{ flex: 1 }}>
        <Modal
          style={{ flex: 1, alignItems: "center" }}
          isVisible={this.state.visible}
        >
          <View
            style={{
              backgroundColor: "white",
              height: 227,
              width: 295,
              paddingLeft: "5%",
              paddingRight: "5%",
              // padding: '4%',
              // paddingTop:'15%',
              borderRadius: 8,
              justifyContent: "center"
            }}
          >
            <Text style={[styles.textStyle2, { textAlign: "center" }]}>
              {t("lang")}
            </Text>
            <View style={{ paddingTop: "5%", paddingBottom: "7%" }}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  margin: "auto"
                }}
              >
                <Image
                  source={Images.spanishFlag}
                  resizeMode="contain"
                  style={styles.imageModalStyle}
                />
                <Text
                  style={[
                    styles.textStyle2,
                    { fontSize: 15, marginRight: "auto" }
                  ]}
                >
                  {t("common:actions.toggleToSpanish")}
                </Text>

                <RadioButton
                  animation={"bounceIn"}
                  isSelected={this.state.checkedSpanish}
                  onPress={() => this.changeLang("es", "Spanish")}
                  innerColor="rgb(41,34,108)"
                  outerColor="rgb(41,34,108)"
                  size={12}
                />
              </View>
              <View
                style={{
                  borderBottomWidth: 0.6,
                  marginTop: 17,
                  marginBottom: 17,
                  opacity: 0.3
                }}
              />
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  margin: "auto"
                }}
              >
                <Image
                  source={Images.englishFlag}
                  resizeMode="contain"
                  style={styles.imageModalStyle}
                />
                <Text
                  style={[
                    styles.textStyle2,
                    { fontSize: 15, marginRight: "auto" }
                  ]}
                >
                  {t("common:actions.toggleToEnglish")}
                </Text>

                <RadioButton
                  animation={"bounceIn"}
                  isSelected={this.state.checkedEnglish}
                  onPress={() => this.changeLang("en", "English")}
                  innerColor="rgb(41,34,108)"
                  outerColor="rgb(41,34,108)"
                  size={12}
                />
              </View>
            </View>
            <TouchableOpacity
              onPress={() =>
                this.onChangeLang(this.state.languageCode, this.state.language)
              }
              style={styles.button}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  marginBottom: "2%",
                  // marginTop:'auto',
                  fontSize: 17,
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {t("common:submit")}
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
        <View style={{ flex: 1 }}>
          <ImageBackground
            source={require("../../../assets/loginLayer.jpg")}
            style={{ flex: 1 }}
          >
            <View
              style={{
                flex: 1,
                alignItems: "center"
              }}
            >
              <View style={styles.cardStyle}>
                <View style={{ height: "10%", justifyContent: "center" }}>
                  <TouchableOpacity
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      alignItems: "center"
                    }}
                    onPress={() => {
                      if (this.props.user_profile_data) {
                        this.props.navigation.navigate("Profile");
                      } else {
                        return null;
                      }
                    }}
                  >
                    <Image
                      source={Images.profile}
                      resizeMode="contain"
                      style={styles.imageStyle}
                    />
                    <Text
                      style={[
                        styles.textStyle,
                        {
                          color: this.props.user_profile_data ? "black" : "grey"
                        }
                      ]}
                    >
                      {t("settings:profile")}
                    </Text>
                    <Image
                      source={Images.arrow}
                      style={{ height: "72%", marginLeft: "auto" }}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                </View>
                <View style={{ borderBottomWidth: 0.6, opacity: 0.2 }} />
                {/* add new field */}
                <View style={{ height: "10%", justifyContent: "center" }}>
                  <TouchableOpacity
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      alignItems: "center"
                    }}
                    onPress={() => {
                      this.props.navigation.navigate("AssignedJobs");
                    }}
                  >
                    <Image
                      source={Images.jobs}
                      resizeMode="contain"
                      style={styles.imageStyle}
                    />
                    {/* <Text
                      style={[
                        styles.textStyle,
                        {
                          color: this.props.user_profile_data ? "black" : "grey"
                        }
                      ]}
                    >
                      {t("settings:AssignedJobs")}
                    </Text> */}
                    <Text style={styles.textStyle}>{t("settings:AssignedJobs")}</Text>
                    <Image
                      source={Images.arrow}
                      style={{ height: "72%", marginLeft: "auto" }}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                </View>
                <View style={{ borderBottomWidth: 0.6, opacity: 0.2 }} />
                <View style={{ height: "10%", justifyContent: "center" }}>
                  <TouchableOpacity
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      alignItems: "center"
                    }}
                    onPress={() =>
                      this.props.navigation.navigate("ReferralScreen")
                    }
                  >
                    <Image
                      source={Images.contest}
                      resizeMode="contain"
                      style={styles.imageStyle}
                    />
                    {/* <Text style={styles.textStyle}>{t("contest:contest")}</Text> */}
                    <Text style={styles.textStyle}>Referral</Text>
                    <Image
                      source={Images.arrow}
                      style={{ height: "72%", marginLeft: "auto" }}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                </View>
                <View style={{ borderBottomWidth: 0.6, opacity: 0.2 }} />
                <View style={{ height: "10%", justifyContent: "center" }}>
                  <TouchableOpacity
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      alignItems: "center"
                    }}
                    onPress={() =>
                      this.props.navigation.navigate("LeaderBoard", {
                        route: "LeaderBoard"
                      })
                    }
                  >
                    <Image
                      source={Images.leaderBoardIcon}
                      resizeMode="contain"
                      style={styles.imageStyle}
                    />
                    <Text style={styles.textStyle}>
                      {t("settings:leaderboard")}
                    </Text>
                    <Image
                      source={Images.arrow}
                      style={{ height: "72%", marginLeft: "auto" }}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                </View>

                <View style={{ borderBottomWidth: 0.6, opacity: 0.2 }} />
                <View style={{ height: "10%", justifyContent: "center" }}>
                  <TouchableOpacity
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      alignItems: "center"
                    }}
                    onPress={() => this.setState({ visible: true })}
                  >
                    <Image
                      source={Images.language}
                      resizeMode="contain"
                      style={styles.imageStyle}
                    />
                    <Text style={styles.textStyle}>{t("settings:lang")}</Text>
                    <Image
                      source={Images.arrow}
                      style={{ height: "72%", marginLeft: "auto" }}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                </View>

                <View style={{ borderBottomWidth: 0.6, opacity: 0.2 }} />
                <View style={{ height: "10%", justifyContent: "center" }}>
                  <TouchableOpacity
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      alignItems: "center"
                    }}
                    onPress={() =>
                      this.props.navigation.navigate("PrivacyPolicy")
                    }
                  >
                    <Image
                      source={Images.privacy}
                      resizeMode="contain"
                      style={styles.imageStyle}
                    />
                    <Text style={styles.textStyle}>
                      {t("settings:privacy")}
                    </Text>
                    <Image
                      source={Images.arrow}
                      style={{ height: "72%", marginLeft: "auto" }}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                </View>
                <View style={{ borderBottomWidth: 0.6, opacity: 0.2 }} />
                <View style={{ height: "10%", justifyContent: "center" }}>
                  <TouchableOpacity
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      alignItems: "center"
                    }}
                    onPress={() =>
                      this.props.navigation.navigate("TermsAndCond")
                    }
                  >
                    <Image
                      source={Images.termsAndCond}
                      resizeMode="contain"
                      style={styles.imageStyle}
                    />
                    <Text style={styles.textStyle}>{t("settings:terms")}</Text>
                    <Image
                      source={Images.arrow}
                      style={{ height: "72%", marginLeft: "auto" }}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                </View>
                <View style={{ borderBottomWidth: 0.6, opacity: 0.2 }} />
                <View style={{ height: "10%", justifyContent: "center" }}>
                  <TouchableOpacity
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      alignItems: "center"
                    }}
                    onPress={() => this.props.navigation.navigate("AboutUs")}
                  >
                    <Image
                      source={Images.aboutUs}
                      resizeMode="contain"
                      style={styles.imageStyle}
                    />
                    <Text style={styles.textStyle}>
                      {t("settings:aboutUs")}
                    </Text>
                    <Image
                      source={Images.arrow}
                      style={{ height: "72%", marginLeft: "auto" }}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                </View>
                <View style={{ borderBottomWidth: 0.6, opacity: 0.2 }} />

                <View style={{ height: "10%", justifyContent: "center" }}>
                  <TouchableOpacity
                    style={{ flexDirection: "row", alignItems: "center" }}
                    onPress={this.logOutConfirm}
                  >
                    <Image
                      source={Images.signOut}
                      resizeMode="contain"
                      style={styles.imageStyle}
                    />
                    <Text style={styles.textStyle}>
                      {t("settings:signOut")}
                    </Text>
                    {this.state.loading ? (
                      <ActivityIndicator
                        size={"large"}
                        style={styles.activityStyle}
                        color={"rgb(41,34,108)"}
                      />
                    ) : null}
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </ImageBackground>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    height: "97%",
    width: "90%",
    padding: "4%",
    marginBottom: "4%",
    paddingTop: 8,
    paddingBottom: 0,
    elevation: 6,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2
  },
  imageStyle: {
    height: "100%",
    width: "7%"
  },
  imageModalStyle: {
    height: 26,
    width: 36,
    alignSelf: "center"
  },
  textStyle2: {
    fontSize: 15,
    color: "black",
    marginLeft: "3%",
    fontFamily: "Comfortaa-Bold"
  },

  textStyle: {
    fontSize: 15,
    color: "black",
    marginLeft: "3%",
    fontFamily: "Comfortaa-Bold"
  },
  borderStyle: {
    borderWidth: 0.6,
    opacity: 0.05,
    marginTop: "2%"
  },
  button: {
    height: 47,
    width: 255,
    borderRadius: 8,
    justifyContent: "center",
    alignSelf: "center",
    // alignItems: 'center',
    backgroundColor: "rgb(41,34,108)",
    marginBottom: "2%"
  },
  activityStyle: {
    alignSelf: "flex-start",
    marginLeft: "auto"
  }
});

const mapStateToProps = ({ auth, userprofile }) => {
  const { user_profile_data } = userprofile;
  const { userData, accessToken, userId } = auth;
  return { userData, accessToken, userId, user_profile_data };
};

export default connect(mapStateToProps)(
  withTranslation(
    ["settings", "common"],
    { wait: true },
    { withRef: true }
  )(Settings)
);
