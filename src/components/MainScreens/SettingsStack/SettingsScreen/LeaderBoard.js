/* eslint-disable no-nested-ternary */
import React from "react";
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TextInput,
  Image,
  TouchableOpacity,
  FlatList,
  Platform,
  ActivityIndicator
} from "react-native";
import axios from "axios";
import { connect } from "react-redux";
import { settingsApi, contestAPI } from "../../../../config/api";
import { Images } from "../../../../assets/images";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import i18n from "../../../../i18n";

class LeaderBoard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      leaderBoardData: [],
      loading: false,
      loadingError: false
    };
  }

  async componentDidMount() {
    this.setState({ loading: true, loadingError: false });
    let route = this.props.navigation.state.params.route;
    console.log("porps", route);
    if (route == "LeaderBoard") {
      await axios
        .get(settingsApi.leaderBoard(this.props.accessToken), {
          headers: {
            "Accept-Language": i18n.language.includes("es") ? "es" : null
          }
        })
        .then(resLeaderBoard => {
          console.log("resLeaderBoard", resLeaderBoard.data);
          resLeaderBoard.data.data.forEach(itemData => {
            this.state.leaderBoardData.push(itemData);
          });
          setTimeout(() => {
            this.setState({ loading: false, loadingError: false });
          }, 1000);
        })
        .catch(leaderBoardError => {
          console.log("leaderBoardError", leaderBoardError);
          this.setState({ loading: false, loadingError: true });
        });
    } else {
      console.log("Rank");
      let contestId = this.props.navigation.state.params.contestId;
      await axios
        .get(contestAPI.rankList(contestId, this.props.accessToken), {
          headers: {
            "Accept-Language": i18n.language.includes("es") ? "es" : null
          }
        })
        .then(resLeaderBoard => {
          console.log("resLeaderBoard", resLeaderBoard.data);
          // resLeaderBoard.data.data.forEach((itemData) => {
          //   this.state.leaderBoardData.push(itemData);
          // });
          this.setState(
            { leaderBoardData: resLeaderBoard.data.Final_Winner },
            () => console.log(this.state.leaderBoardData)
          );
          setTimeout(() => {
            this.setState({ loading: false, loadingError: false });
          }, 1000);
        })
        .catch(leaderBoardError => {
          console.log("leaderBoardError", leaderBoardError);
          this.setState({ loading: false, loadingError: true });
        });
    }
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={require("../../../../assets/loginLayer.jpg")}
          style={{ flex: 1 }}
        >
          <View style={{ flex: 1, alignItems: "center" }}>
            <View style={styles.cardStyle}>
              {!this.state.loading ? (
                !this.state.loadingError ? (
                  <FlatList
                    data={this.state.leaderBoardData}
                    renderItem={({ item }) => {
                      return (
                        <View>
                          <View
                            style={{
                              flexDirection: "row",
                              flex: 1,
                              // justifyContent: "space-between",
                              alignItems: "center"
                            }}
                          >
                            <View style={{ width: "23%" }}>
                              <Image
                                source={{
                                  uri: item.img_thumbnail
                                    ? item.img_thumbnail
                                    : item.image
                                }}
                                style={styles.imageStyle}
                              
                              />
                            </View>
                            <View
                              style={{
                                flexDirection: "column",
                                width: "53%",
                                marginRight: "auto",
                                marginLeft: hp("1%")
                              }}
                            >
                              <Text style={styles.nameStyle}>
                                {item.full_name}
                              </Text>
                              <Text style={styles.emailStyle}>
                                {item.email}
                              </Text>
                            </View>
                            <View
                              style={{
                                width: "20%",
                                alignItems: "center",
                                flexDirection: "column",
                                marginLeft: "auto",
                                marginRight: hp("2%")
                              }}
                            >
                              <Text style={styles.nameStyle}>
                                {item.points}
                              </Text>
                              <Text style={styles.pointTextStyle}>points</Text>
                            </View>
                          </View>
                          <View
                            style={{
                              borderWidth: 0.6,
                              opacity: 0.1,
                              margin: hp("1.5%")
                            }}
                          />
                        </View>
                      );
                    }}
                    keyExtractor={(item, index) => index.toString()}
                    showsVerticalScrollIndicator={false}
                  />
                ) : (
                  <View
                    style={{
                      alignItems: "center",
                      justifyContent: "center",
                      marginTop: "auto",
                      marginBottom: "auto"
                    }}
                  >
                    <TouchableOpacity
                      onPress={() => {
                        this.componentDidMount();
                      }}
                    >
                      <Text style={{ color: "red", fontSize: 30 }}>RETRY!</Text>
                    </TouchableOpacity>
                  </View>
                )
              ) : (
                <ActivityIndicator
                  size={"large"}
                  color={"rgb(41,34,108)"}
                  style={{ marginTop: "auto", marginBottom: "auto" }}
                />
              )}
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    padding: "5%",
    paddingBottom: 20,
    paddingTop: 20,
    height: "95%",
    width: "90%",
    elevation: 6,
    backgroundColor: "white",
    borderRadius: 10,
    alignSelf: "center",
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2
  },
  nameStyle: {
    fontSize: 15,
    color: "rgb(51,51,51)",
    fontFamily: "Comfortaa-Bold",
    marginBottom:Platform.OS=='ios' ? 5:0
  },
  imageStyle: {
    height: 70,
    width:70,
    marginLeft: "auto",
    marginRight: "auto",
    borderRadius:34
  },
  emailStyle: {
    fontFamily: "Comfortaa-Bold",
    fontSize: 12,
    width:'95%'
  },
  pointTextStyle: {
    fontFamily: "Comfortaa-Bold",
    fontSize: 12
  }
});

const mapStateToProps = ({ auth }) => {
  const { accessToken } = auth;
  return { accessToken };
};

export default connect(mapStateToProps, {})(LeaderBoard);
