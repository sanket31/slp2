import React, { Fragment } from "react";
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  Platform
} from "react-native";
import { Images } from "../../../../assets/images";
import { getJobDetails } from "../../../../Actions/settingsActions";
import { connect } from "react-redux";
import ImageViewer from "../../../ImageViewer";
import axios from "axios";
import { ModalSpinner } from "../../../Spinner";
import { BASE_URL } from "../../../../config/api";

class JobDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = { visible: false, taskStatus: "", spinner: false };
  }

  componentDidMount() {
    const { jobId } = this.props.navigation.state.params;
    this.props.getJobDetails(this.props.accessToken, jobId);
  }

  modalHandler = () => {
    this.setState({ visible: !this.state.visible });
  };

  renderText = (url, type) => {
    if (type == "pdf") {
      return url.replace(
        "https://slp-static.s3.amazonaws.com/media/contractor/task_attachments/",
        ""
      );
    } else if (type == "image") {
      return url.replace(
        "https://slp-static.s3.amazonaws.com/media/contractor/task_attachments/",
        ""
      );
    }
  };

  sendRequest = status => {
    this.setState({ spinner: true });
    const { jobId } = this.props.navigation.state.params;

    axios(`${BASE_URL}user/tasks/${jobId}/`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      data: {
        access_token: this.props.accessToken,
        task_status:
          status.toLowerCase() == "to do"
            ? "Running"
            : status.toLowerCase() == "running"
            ? "Completed"
            : status.toLowerCase() == "Completed"
            ? "Completed"
            : status
      }
    })
      .then(res => {
        if (res.data.status == "success") {
          this.props.getJobDetails(this.props.accessToken, jobId);
        }
        setTimeout(() => this.setState({ spinner: false }), 3000);
      })
      .catch(error => {
        console.log(error);
        this.setState({ spinner: false });
      });
  };

  render() {
    const { jobDetails } = this.props;
    if (this.state.taskStatus !== jobDetails.task_status) {
      this.setState({ taskStatus: jobDetails.task_status });
    }
    console.log(this.state.taskStatus)
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={require("../../../../assets/loginLayer.jpg")}
          style={{ flex: 1 }}
        >
          <ModalSpinner
            visible={this.state.spinner}
            spinnerText={"Uploading.."}
          />
          <View
            style={{
              flex: 1,
              alignItems: "center"
            }}
          >
            <View style={styles.cardStyle}>
              {this.props.jobDetails ? (
                <Fragment>
                  <Text style={styles.textStyle}>Title</Text>
                  <Text style={styles.textStyle2}>{jobDetails.title}</Text>
                  <View
                    style={{
                      borderBottomWidth: 0.6,
                      opacity: 0.2,
                      marginTop: 10,
                      marginBottom: 10
                    }}
                  />
                  <Text style={styles.textStyle}>Description</Text>
                  <Text style={styles.textStyle2}>
                    {jobDetails.description}
                  </Text>
                  <View
                    style={{
                      borderBottomWidth: 0.6,
                      opacity: 0.2,
                      marginTop: 10,
                      marginBottom: 10
                    }}
                  />
                  {jobDetails.attachment.length > 0 ? (
                    <View style={{ marginVertical: 5, marginBottom: 30 }}>
                      <Text style={styles.textStyle}>Attachment</Text>
                      <View style={{ flexDirection: "row" }}>
                        {jobDetails.attachment.map((item, index) => (
                          <View key={index} style={{ marginRight: 15 }}>
                            {item.type == "image" ? (
                              <ImageViewer
                                modalVisible={this.state.visible}
                                modalHandler={this.modalHandler}
                                url={item.url}
                              />
                            ) : null}

                            {item.type == "pdf" ? (
                              <TouchableOpacity
                                onPress={() =>
                                  this.props.navigation.navigate("PDFLibrary", {
                                    pdfUrl: item.url
                                  })
                                }
                              >
                                <Image
                                  source={Images.pdf}
                                  style={{
                                    height: 70,
                                    width: 70,
                                    marginVertical: 5
                                  }}
                                />
                                <Text
                                  style={{ width: 70 }}
                                  ellipsizeMode="middle"
                                  numberOfLines={1}
                                >
                                  {this.renderText(item.url, item.type)}
                                </Text>
                              </TouchableOpacity>
                            ) : item.type == "image" ? (
                              <TouchableOpacity
                                onPress={() => this.modalHandler()}
                              >
                                <Image
                                  source={{ uri: item.url }}
                                  style={{
                                    height: 70,
                                    width: 70,
                                    marginVertical: 5,
                                    borderRadius: 8
                                  }}
                                />
                                <Text
                                  style={{ width: 70 }}
                                  ellipsizeMode="middle"
                                  numberOfLines={1}
                                >
                                  {this.renderText(item.url, item.type)}
                                </Text>
                              </TouchableOpacity>
                            ) : null}
                          </View>
                        ))}
                      </View>
                    </View>
                  ) : null}
                  <TouchableOpacity
                    style={styles.button}
                    onPress={() => {
                      if (this.state.taskStatus !== "Completed") {
                        this.sendRequest(this.state.taskStatus);
                      }
                    }}
                    disabled={this.state.taskStatus == "Completed"}
                    activeOpacity={0.6}
                  >
                    <Text
                      style={{
                        color: "white",
                        fontSize: 15,
                        fontFamily: "Comfortaa-Bold"
                      }}
                    >
                      {this.state.taskStatus == "To Do"
                        ? "Start Task"
                        : this.state.taskStatus == "Running"
                        ? "End Task"
                        : this.state.taskStatus}
                    </Text>
                  </TouchableOpacity>
                </Fragment>
              ) : (
                <View
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <ActivityIndicator size="large" />
                </View>
              )}
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  cardStyle: {
    height: "97%",
    width: "90%",
    // padding: "4%",
    marginBottom: "4%",
    paddingTop: 8,
    paddingBottom: 0,
    elevation: 6,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    paddingHorizontal: 20,
    paddingTop: 20
  },

  textStyle2: {
    fontSize: 15,
    color: "rgb(102,102,102)",
    fontFamily: "Comfortaa-Bold"
  },

  textStyle: {
    fontSize: 15,
    color: "black",
    fontFamily: "Comfortaa-Bold",
    marginVertical: Platform.OS == "ios" ? 10 : 0
  },
  button: {
    height: 47,
    width: "100%",
    borderRadius: 8,
    justifyContent: "center",
    alignSelf: "center",
    alignItems: "center",
    backgroundColor: "rgb(41,34,108)"
    // marginVertical: "2%"
  }
});

const mapStateToProps = ({ auth, Settings }) => {
  const { accessToken } = auth;
  const { jobDetails } = Settings;
  return { accessToken, jobDetails };
};

export default connect(mapStateToProps, { getJobDetails })(JobDetails);
