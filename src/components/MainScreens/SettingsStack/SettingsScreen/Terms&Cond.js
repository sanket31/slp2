import React from "react";
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  Image,
  ScrollView
} from "react-native";
import { Icon } from "react-native-elements";
import { Images } from "../../../../assets/images";
import { WebViewComponent } from "../../../WebView";
import { CmsPages } from "../../../../config/api";
import i18n from "../../../../i18n";

class TermsAndCond extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={require('../../../../assets/loginLayer.jpg')}
          style={{ flex: 1 }}
        >
          <View style={{ flex: 1, alignItems:'center' }}>
            <View style={styles.cardStyle}>
              <WebViewComponent url={i18n.language.includes("es") ? CmsPages.TermsAndConditionsSpanish: CmsPages.TermsAndConditions}/>
              {/* <ScrollView
                style={{ flex: 1 }}
                showsVerticalScrollIndicator={false}
              >
                <Text style={styles.descriptionText}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Praesent rutrum rhoncus dapibus. Aenean aliquam metus id
                  ligula scelerisque, eu molestie libero elementum. Fusce at
                  tellus lectus. Ut auctor velit at auctor aliquet. Ut mi magna,
                  ultricies sed auctor eget, mollis non diam. Donec ac mauris
                  enim. Nam vehicula auctor risus, non feugiat elit pulvinar
                  non. Vestibulum ultricies elementum libero nec maximus. Etiam
                  tortor magna, semper vitae orci eu, accumsan ultricies mauris.
                  Morbi aliquet varius semper. Suspendisse imperdiet dolor odio,
                  eget consectetur purus tincidunt quis. In dapibus dolor vel
                  erat posuere, a dictum elit scelerisque. Morbi ac eleifend
                  metus, sed ornare lorem. Morbi a tortor vitae neque venenatis
                  fermentum. Aliquam mattis, mauris porttitor tristique
                  vulputate, felis justo feugiat tortor, vel aliquam risus
                  tellus sit amet nunc. Phasellus blandit posuere gravida.
                  Mauris tincidunt tellus vel nunc pharetra, vulputate gravida
                  felis mollis. Aliquam ac ultricies nulla. Curabitur facilisis
                  non enim quis mollis. Donec id neque eget neque elementum
                  feugiat. Morbi a viverra nisl. Phasellus ac dui libero. Mauris
                  nec urna sed ante malesuada accumsan. Nam eget pulvinar felis.
                  Nullam laoreet magna ac euismod convallis. In hac habitasse
                  platea dictumst. Suspendisse varius leo eu ultrices iaculis.
                  Donec at semper felis. Donec ultricies dignissim ante, nec
                  scelerisque sapien porta in. Aenean accumsan interdum
                  consequat. Sed ut lorem vel nisl ornare faucibus. Fusce ut
                  eros ullamcorper, dictum magna ac, ullamcorper magna. Proin
                  gravida molestie nisi, vel egestas lacus. Class aptent taciti
                  sociosqu ad litora torquent per conubia nostra, per inceptos
                  himenaeos. Suspendisse feugiat congue elit, eu venenatis justo
                  dignissim maximus. Etiam egestas pellentesque volutpat. Morbi
                  egestas aliquet nulla nec ultrices. Phasellus in ultricies
                  eros. Etiam porta neque tincidunt eleifend tempus. Fusce
                  consequat sapien nisl. Aliquam rhoncus dolor molestie gravida
                  sagittis. Sed sapien arcu, pellentesque ut vehicula id,
                  interdum at dolor. Vivamus libero lectus, venenatis eget
                  laoreet egestas, finibus mollis orci.
                </Text>
              </ScrollView> */}
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    padding: "5%",
    paddingBottom: 0,
    height: "95%",
    width: "90%",
    elevation: 6,
    backgroundColor: "white",
    borderRadius: 10,
    alignSelf: "center",
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2
  },
  descriptionText: {
    marginTop: "5%"
  }
});

export default TermsAndCond;
