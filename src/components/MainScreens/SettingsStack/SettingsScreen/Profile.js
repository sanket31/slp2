import React from "react";
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Platform,
  ActivityIndicator
} from "react-native";
import SnackBar from "react-native-snackbar-component";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import { Images } from "../../../../assets/images";
import { connect } from "react-redux";
import ImagePickerProfile from "react-native-image-picker";
import i18n from "../../../../i18n";
// import ImagePicker from 'react-native-image-crop-picker';
import {
  saveProfile,
  enableLoader,
  resetSnackProfile
} from "../../../../Actions/profileActions";

class Profile extends React.Component {
  constructor(props) {  
    super(props);
    let { data } = this.props.user_profile_data;
    console.log("data......", data);
    this.state = {
      full_name: data.full_name,
      email: data.email,
      company_name: data.company_name,
      phone: data.phone,
      address: {
        add_line1: data.address.add_line1,
        add_line2: data.address.add_line2,
        city: data.address.city,
        state: data.address.state,
        zip_code: data.address.zip_code.toString()
      },
      image: data.image,
      access_token: this.props.accessToken,
      toggle_snackbar: false,
      snackbar_msg: ""
    };
  }
  handleInput = (text, inputname) => {
    console.log("input", text, inputname);

    this.setState({
      [inputname]: text
    });
  };
  handleAddress = (text, inputname) => {
    let address = {
      ...this.state.address,
      [inputname]: text
    };
    this.setState({
      address
    });
  };

  ValidateNumber = phone => {
    if (
      /^[+]?(1\-|1\s|1|\d{3}\-|\d{3}\s|)?((\(\d{3}\))|\d{3})(\-|\s)?(\d{3})(\-|\s)?(\d{4})$/g.test(
        phone
      )
    ) {
      return true;
    }
    return false;
  };

  handleSave = async () => {
    const { full_name, company_name, phone, address } = this.state;
    if (
      full_name.length > 0 &&
      company_name.length > 0 &&
      this.ValidateNumber(phone) &&
      address.add_line1.length > 0 &&
      address.city.length > 0 &&
      address.state.length > 0 &&
      address.zip_code.length > 0
    ) {
      this.props.enableLoader();
      const data = new FormData();
      if (
        this.props.user_profile_data.data.thumbnail_image != this.state.image
      ) {
        data.append("image", {
          name: "slpImage",
          type: "image/jpeg",
          uri:
            Platform.OS === "android"
              ? this.state.image
              : this.state.image.replace("file://", "")
        });
      }
      Object.keys(this.state).forEach(key => {
        if (key != "image") {
          if (key != "address") {
            data.append(key, this.state[key]);
          } else {
            data.append(key, JSON.stringify(this.state[key]));
          }
        }
      });
      console.log("profile data....", data);
      await this.props.saveProfile(data);
    } else {
      const checkData = [
        full_name,
        company_name,
        phone,
        address.add_line1,
        address.city,
        address.state,
        address.zip_code
      ];
      const checkItem = field => {
        return field.length === 0;
      };
      console.log("catatatatatatatatatata", checkData);
      if (checkData.some(checkItem)) {
        this.showtoast(i18n.t('profile:fieldEmpty'));
      } else {
        this.showtoast(i18n.t('profile:validNum'));
      }
    }
  };

  handleCameraClick = () => {
    const options = {
      title: "Select Avatar",
      quality: 0.2,
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    ImagePickerProfile.showImagePicker(options, response => {
      console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else {
        // const source = { uri: response.uri };

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        // console.log('response./....',response)
        this.setState({
          image: response.uri
        });
        // ImagePicker.openCropper({
        //   path: response.path,
        //   width: 300,
        //   height: 400,
        // }).then(image => {
        //   console.log('response...', image);
        //   // this.setState({
        //   //   image: response.uri,
        //   // });
        // });
      }
    });
  };

  showtoast = msg => {
    this.setState({
      toggle_snackbar: true,
      snackbar_msg: msg
    });
  };

  renderSnackMessage = () => {
    if (!this.props.getProfileSuccess) {
      return (
        <SnackBar
          visible={this.state.toggle_snackbar}
          accentColor="white"
          backgroundColor="rgb(41,34,108)"
          position="bottom"
          textMessage={this.state.snackbar_msg}
          actionHandler={() => {
            this.setState({
              toggle_snackbar: false,
              snackbar_msg: ""
            });
          }}
          actionText="OK"
        />
      );
    }
    return (
      <SnackBar
        visible={this.props.getProfileSuccess}
        accentColor="white"
        backgroundColor="rgb(41,34,108)"
        position="bottom"
        textMessage={i18n.t('profile:updateSuccess')}
        actionHandler={() => {
          this.props.resetSnackProfile();
        }}
        actionText="OK"
      />
    );
  };

  componentWillUnmount = () => {
    this.props.resetSnackProfile()
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={require("../../../../assets/loginLayer.jpg")}
          style={{ flex: 1, alignItems: "center" }}
        >
          <View style={styles.cardStyle}>
            {this.renderSnackMessage()}
            <ScrollView
              style={{ flex: 1 }}
              showsVerticalScrollIndicator={false}
            >
              <View style={{ flex: 1 }}>
                <View style={{ alignItems: "center" }}>
                  <Image
                    source={{ uri: this.state.image }}
                    style={{ height: 100, width: 100, borderRadius: Platform.OS =='ios' ? 50 :100 }}
                  />
                </View>
                <TouchableOpacity
                  onPress={this.handleCameraClick}
                  style={{
                    alignItems: "center",
                    marginLeft: wp("20%"),
                    transform: [{ translateY: -30 }, { translateX: -10 }]
                  }}
                >
                  <Image
                    source={Images.edit}
                    style={{ height: hp("4.5%"), width: wp("8%") }}
                    resizeMode='contain'
                  />
                </TouchableOpacity>
                {console.log(this.state.full_name.length)}
                <TextInput
                  placeholder="Name"
                  onChangeText={text => this.handleInput(text, "full_name")}
                  value={this.state.full_name}
                  style={styles.textInputStyle}
                />
                <TextInput
                  placeholder="Company Name"
                  onChangeText={text => this.handleInput(text, "company_name")}
                  value={this.state.company_name}
                  style={styles.textInputStyle}
                />
                <TextInput
                  placeholder="Email Address"
                  value={this.state.email}
                  style={[styles.textInputStyle, {color:'rgba(51,51,51,0.5)'}]}
                  editable={false}
                />
                <TextInput
                  placeholder="Phone Number"
                  value={this.state.phone}
                  onChangeText={text => this.handleInput(text, "phone")}
                  style={styles.textInputStyle}
                />
                <TextInput
                  placeholder="Address Line 1"
                  value={this.state.address.add_line1}
                  onChangeText={text => this.handleAddress(text, "add_line1")}
                  style={styles.textInputStyle}
                />
                <TextInput
                  placeholder="Address Line 2"
                  value={this.state.address.add_line2}
                  onChangeText={text => this.handleAddress(text, "add_line2")}
                  style={styles.textInputStyle}
                />
                <TextInput
                  placeholder="City"
                  value={this.state.address.city}
                  onChangeText={text => this.handleAddress(text, "city")}
                  style={styles.textInputStyle}
                />
                <TextInput
                  placeholder="State"
                  value={this.state.address.state}
                  onChangeText={text => this.handleAddress(text, "state")}
                  style={styles.textInputStyle}
                />
                <TextInput
                  placeholder="Zip Code"
                  value={this.state.address.zip_code}
                  onChangeText={text => this.handleAddress(text, "zip_code")}
                  style={styles.textInputStyle}
                />
                {/* <TextInput placeholder="Referral Code" onChangeText={(text) => this.handleInput(text, 'referral_code')} style={styles.textInputStyle} /> */}
                <View style={{ margin: hp("1%") }} />
                <TouchableOpacity
                  style={styles.button}
                  onPress={this.handleSave}
                >
                  {this.props.loader ? (
                    <ActivityIndicator size="large" color="#0000ff" />
                  ) : (
                    <Text
                      style={{
                        color: "white",
                        textAlign: "center",
                        fontSize: 15,
                        fontFamily: "Comfortaa-Bold"
                      }}
                    >
                      {i18n.t("common:save")}
                    </Text>
                  )}
                </TouchableOpacity>
                <View style={{ margin: hp("1%") }} />
              </View>
            </ScrollView>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    height: "95%",
    width: "90%",
    padding: "4%",
    paddingBottom: 0,
    paddingTop: 10,
    elevation: 5,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2
  },
  descriptionText: {
    marginTop: "5%"
  },
  textInputStyle: {
    height: hp("6%"),
    fontSize: 15,
    fontWeight: "normal",
    borderBottomWidth: 0.6,
    borderColor: "rgba(41,34,108,0.2)",
    fontFamily: "Comfortaa-Bold",
    padding: 0
  },
  borderStyle: {
    borderWidth: 0.6,
    opacity: 0.05,
    marginBottom: "3%"
  },
  button: {
    height: 47,
    width: "100%",
    borderRadius: 8,
    justifyContent: "center",
    backgroundColor: "rgb(41,34,108)"
  }
});

const mapStateToProps = ({ userprofile, auth }) => {
  const {
    user_profile_data,
    get_profile_error,
    loader,
    getProfileSuccess
  } = userprofile;
  const { userData, accessToken, userId } = auth;
  return {
    accessToken,
    userId,
    userData,
    user_profile_data,
    get_profile_error,
    loader,
    getProfileSuccess
  };
};
const actionCreater = {
  saveProfile,
  enableLoader,
  resetSnackProfile
};

export default connect(mapStateToProps, actionCreater)(Profile);
