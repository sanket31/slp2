import React from "react";
import {
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  Platform,
  Image,
  StyleSheet
} from "react-native";
import { Images } from "../../../../assets/images";

class ContestWinners extends React.Component {
  render() {
    return (
      <ImageBackground
        source={Images.loginLayer}
        style={{ flex: 1, alignItems: "center" }}
      >
        <View style={styles.cardStyle}></View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    height: "95%",
    width: "90%",
    padding: "4%",
    paddingBottom: 0,
    paddingTop: 10,
    elevation: 5,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2
  }
});

export default ContestWinners;
