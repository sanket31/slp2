import React, { Fragment } from "react";
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Image,
  ActivityIndicator
} from "react-native";
import { Images } from "../../../../assets/images";
import NoConnectionModalReward from "../../../../utility/NoConnectionModal";
import {
  getAssignedJobList,
  resetJobDetails,
  setJoblistLoader
} from "../../../../Actions/settingsActions";
import { connect } from "react-redux";
import { NavigationEvents } from "react-navigation";

class AssignedJobs extends React.Component {
  componentDidMount() {
    this.props.setJoblistLoader(true);
    this.props.getAssignedJobList(this.props.accessToken);
  }
  getJobList = () => {
    this.props.setJoblistLoader(true);
    this.props.getAssignedJobList(this.props.accessToken);
  };
  render() {
    const { joblist } = this.props;
    console.log('loader....', this.props.listloader);
    return (
      <View style={{ flex: 1 }}>
        <NoConnectionModalReward
          onPress={this.getJobList}
          errorMessage={this.props.rewardsFailMessage}
          isVisible={this.props.rewardsFail}
        />
        <NavigationEvents onDidFocus={() => this.props.resetJobDetails()} />
        <ImageBackground
          source={require("../../../../assets/loginLayer.jpg")}
          style={{ flex: 1 }}
        >
          <View
            style={{
              flex: 1,
              alignItems: "center"
            }}
          >
            <View style={styles.cardStyle}>
              {this.props.listloader ? (
                <ActivityIndicator
                  size={"large"}
                  color={"rgb(41,34,108)"}
                  style={{ marginTop: "auto", marginBottom: "auto" }}
                />
              ) : joblist.length <= 0 ? (
                <View style={{
                  flex: 1,
                  justifyContent : 'center',
                  alignItems : 'center'
                }}>
                <Text style={{
                  fontFamily : 'Comfortaa-Bold',
                  fontSize : 18,
                  fontWeight : 'bold',
                  color : 'rgb(51,51,51)'
                }}>No Task Available</Text>
                </View>
              ) : (
                joblist.map(item => {
                  return (
                    <Fragment key={item.id.toString()}>
                      <View style={{ height: "10%", justifyContent: "center" }}>
                        <TouchableOpacity
                          style={{
                            flexDirection: "row",
                            justifyContent: "space-between",
                            alignItems: "center"
                          }}
                          onPress={() => {
                            this.props.navigation.navigate("JobDetails", {
                              jobId: item.id
                            });``
                          }}
                        >
                          <Text style={[styles.textStyle]}>{item.title}</Text>
                          <Image
                            source={Images.arrow}
                            style={{ height: "72%", marginLeft: "auto" }}
                            resizeMode="contain"
                          />
                        </TouchableOpacity>
                      </View>
                      <View style={{ borderBottomWidth: 0.6, opacity: 0.2 }} />
                    </Fragment>
                  );
                })
              )}
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  cardStyle: {
    height: "97%",
    width: "90%",
    padding: "4%",
    marginBottom: "4%",
    paddingBottom: 0,
    elevation: 6,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2
  },
  imageStyle: {
    height: "100%",
    width: "7%"
  },
  imageModalStyle: {
    height: 26,
    width: 36,
    alignSelf: "center"
  },
  textStyle2: {
    fontSize: 15,
    color: "black",
    marginLeft: "3%",
    fontFamily: "Comfortaa-Bold"
  },

  textStyle: {
    fontSize: 15,
    color: "black",
    marginLeft: "3%",
    fontFamily: "Comfortaa-Bold"
  },
  borderStyle: {
    borderWidth: 0.6,
    opacity: 0.05,
    marginTop: "2%"
  },
  button: {
    height: 47,
    width: 255,
    borderRadius: 8,
    justifyContent: "center",
    alignSelf: "center",
    // alignItems: 'center',
    backgroundColor: "rgb(41,34,108)",
    marginBottom: "2%"
  },
  activityStyle: {
    alignSelf: "flex-start",
    marginLeft: "auto"
  }
});

const mapStateToProps = ({ auth, Settings }) => {
  const { userData, accessToken, userId } = auth;
  const { joblist, listloader } = Settings;
  return { userData, accessToken, userId, joblist, listloader };
};
const actionCreater = {
  getAssignedJobList,
  resetJobDetails,
  setJoblistLoader
};

export default connect(mapStateToProps, actionCreater)(AssignedJobs);
