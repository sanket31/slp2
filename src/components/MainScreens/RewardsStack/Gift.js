import React from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  ImageBackground,
  Platform,
  StyleSheet,
  FlatList,
  ActivityIndicator,
  TextInput
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import { Images } from "../../../assets/images";
import RNPickerSelect from "react-native-picker-select";
import { MainStyles } from "../../styles";
import Modal from "@kalwani/react-native-modal";
import axios from "axios";
import Spinner from "react-native-spinkit";
import { connect } from "react-redux";
import i18n from "../../../i18n";
import {BASE_URL} from '../../../config/api'

var API_KEY =
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJTSEEyNTYifQ==.VFAwbUpwc05SMXQzeWhiR3BIbVcvOGNuMG1TajhqMmVadmEvbG5RZFNqMkNBcmtVZmhVTVJSK0svY3pVOFlvcFU0S0NSbmw1ZG8zM0ZHWDFTSW0rQnVTS016OGJHVTF0VGEzRGZKZUs5dllYNEg3WThsOUhvZGZZTWhVOTIwZnc=.Ss+f4HDj+CRCt8PIIvP1F1LoKeDsUA7f7QM0wMLNxok=";

class Gift extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: (
        <View
          style={{
            flexDirection: "row",
            flex: 1,
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <TouchableOpacity
            style={{ flex: i18n.language.includes("es") ? 0 : 0.1 }}
            onPress={() => navigation.goBack()}
          >
            <Image
              source={require("../../../assets/settingsIcon/back.png")}
              resizeMode="contain"
              style={{
                height: hp("3%"),
                marginLeft: i18n.language.includes("es") ? 0 : hp("2%"),
                alignSelf: "center",
                marginRight: "auto",
                marginTop: "7%"
              }}
            />
          </TouchableOpacity>
          <Text
            style={{
              flex: i18n.language.includes("es") ? 0 : 0.8,
              color: "white",
              fontSize: i18n.language.includes("es") ? 16 : 20,
              alignSelf: "center",
              marginLeft: "auto",
              marginRight: "auto",
              textAlign: "center",
              fontFamily: "Comfortaa-Bold"
            }}
          >
            {i18n.t("common:SLP")}
          </Text>
          <View style={{ flex: 0.1 }} />
        </View>
      ),
      headerLeft: null,
      headerStyle: {
        backgroundColor: "rgb(41,34,108)",
        borderBottomWidth: 0,
        height: hp("7%"),
        elevation: 0
      }
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      modalImage: "",
      modalName: "",
      modalGiftVisible: false,
      brands: "",
      minPrice: "",
      maxPrice: "",
      allowedPrice: [],
      spinner: false,
      inputPrice: "",
      agreeToSubmit: false,
      selectedPrice: "",
      message: "",
      errorMessage: "Something went wrong!",
      brandCode: "",
      refreshing: false
    };
  }

  componentDidMount() {
    axios
      .get(
        "https://api-testbed.giftbit.com/papi/v1/brands?&limit=100&region=2",
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: API_KEY
          }
        }
      )
      .then(res => {
        console.log("MOHAMMAD, IITIAN", res.data);
        this.setState({ brands: res.data.brands });
      });
  }

  retrieveBrand = (image, name, brandCode) => {
    console.log("BRAND CODE", brandCode);
    this.setState({
      minPrice: "",
      maxPrice: "",
      allowedPrice: "",
      spinner: !this.state.spinner
    });
    axios
      .get(`https://api-testbed.giftbit.com/papi/v1/brands/${brandCode}`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: API_KEY
        }
      })
      .then(res => {
        let brand = res.data.brand;
        if (brand.max_price_in_cents && brand.min_price_in_cents) {
          this.setState({
            minPrice: brand.min_price_in_cents,
            maxPrice: brand.max_price_in_cents
          });
        }
        if (brand.allowed_prices_in_cents) {
          let data = brand.allowed_prices_in_cents.map(item => {
            return {
              label: `$${item / 100}`,
              value: `$${item / 100}`
            };
          });
          this.setState({ allowedPrice: data });
        }
        this.setState({
          modalImage: image,
          modalName: name,
          brandCode: brandCode,
          description: res.data.brand.description,
          spinner: !this.state.spinner,
          modalVisible: !this.state.modalVisible
        });
      });
  };

  submitGift = () => {
    if (this.state.selectedPrice && this.state.brandCode) {
      let selectedPrice = this.state.selectedPrice.split("$");
      let giftData = [
        {
          brands: this.state.brandCode,
          cents: parseInt(selectedPrice[1]) * 100
        }
      ];
      this.setState({ spinner: true });
      axios(`${BASE_URL}giftbit/${this.props.userId}`, {
        method: "POST",
        headers: {
          "Accept-Language": i18n.language.includes("es") ? "es" : null
        },
        data: {
          access_token: this.props.accessToken,
          giftbit_data: giftData
        }
      })
        .then(res => {
        
          if (res.data.status_code == 200) {
            console.log('submitted successfully.....')
            this.setState({
              message: res.data.message,
              modalVisible: false,
              spinner: false,
              modalGiftVisible: true,
              inputPrice: "",
              selectedPrice: "",
              errorMessage: ""
            });
          }
        })
        .catch(res => {
          console.log("SUM........", res);
          this.setState({
            errorMessage: res.response.data.message,
            modalVisible: false,
            spinner: false,
            modalGiftVisible: true,
            inputPrice: "",
            selectedPrice: "",
            message: ""
          });
        });
    }

    if (this.state.maxPrice && this.state.minPrice) {
      let minPrice = this.state.minPrice / 100;
      let maxPrice = this.state.maxPrice / 100;
      if (
        this.state.inputPrice >= minPrice &&
        this.state.inputPrice <= maxPrice
      ) {
        let giftData = [
          {
            brands: this.state.brandCode,
            cents: parseInt(this.state.inputPrice) * 100
          }
        ];
        this.setState({ agreeToSubmit: false });
        this.setState({ spinner: true });
        console.log("INUT ", giftData);
        axios(`${BASE_URL}giftbit/${this.props.userId}`, {
          method: "POST",
          headers: {
            "Accept-Language": i18n.language.includes("es") ? "es" : null
          },
          data: {
            access_token: this.props.accessToken,
            giftbit_data: giftData
          }
        })
          .then(res => {
            console.log("PICKER ", res.data);
            if (res.data.status_code == 200) {
              this.setState({
                message: res.data.message,
                modalVisible: false,
                spinner: false,
                modalGiftVisible: true,
                inputPrice: "",
                selectedPrice: "",
                errorMessage: ""
              });
            }
          })
          .catch(res => {
            console.log("CATCH", res.response.data.message);
            this.setState({
              errorMessage: res.response.data.message,
              modalVisible: false,
              spinner: false,
              modalGiftVisible: true,
              inputPrice: "",
              selectedPrice: "",
              message: ""
            });
          });
      } else {
        this.setState({ agreeToSubmit: true });
      }
    }
  };

  refreshData = () => {
    axios
      .get(
        "https://api-testbed.giftbit.com/papi/v1/brands?&limit=100&region=2",
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: API_KEY
          }
        }
      )
      .then(res => {
        this.setState({ brands: res.data.brands });
      });
  };

  render() {
    console.log('gift bit modal', this.state.modalGiftVisible)
    console.log(this.state.modalVisible)
    const placeholder = {
      label: "Select the price",
      color: "rgba(102,102,102,0.7)"
    };

    return (
      <View style={{ flex: 1 }}>
        <Modal
          onModalHide={() => console.log("jide error")}
          isVisible={this.state.spinner}
          style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
        >
          <Spinner
            style={styles.spinner}
            size={50}
            type={"Circle"}
            color={"white"}
          />
        </Modal>

        <Modal
          onModalHide={() => {}}
          isVisible={this.state.modalVisible}
          style={{ flex: 1 }}
        >
          <View
            style={{
              height: "auto",
              width: 255,
              alignItems: "flex-end",
              alignSelf: "center",
              marginBottom: -15,
              zIndex: 1,
              marginLeft: 10
            }}
          >
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  modalVisible: !this.state.modalVisible,
                  inputPrice: "",
                  agreeToSubmit: false,
                  selectedPrice: ""
                })
              }
            >
              <Image
                source={Images.close}
                style={{ height: 27, width: 27 }}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
          <View style={styles.modalCard}>
            <Image
              source={{ uri: this.state.modalImage }}
              style={{
                height: 173,
                width: 158,
                alignSelf: "center",
                borderRadius: 10
              }}
              resizeMode="contain"
            />
            <Text
              style={{
                alignSelf: "center",
                fontFamily: "Comfortaa-Bold",
                color: "rgb(51,51,51)",
                marginVertical: 8
              }}
            >
              {this.state.modalName}
            </Text>
            {this.state.maxPrice && this.state.minPrice ? (
              <React.Fragment>
                {this.state.agreeToSubmit ? (
                  <Text
                    style={{
                      textAlign: "center",
                      color: "red",
                      width: 180
                    }}
                  >
                    Amount must be in the range of ${this.state.minPrice / 100}{" "}
                    - ${this.state.maxPrice / 100}
                  </Text>
                ) : (
                  <Text
                    style={{
                      textAlign: "center",
                      color: "rgba(51,51,51,0.7)",
                      width: 180,
                      height: "auto"
                    }}
                  >
                    {`Amount must be in the range of $${this.state.minPrice /
                      100}-$${this.state.maxPrice / 100}`}
                  </Text>
                )}
                <TextInput
                  value={this.state.inputPrice}
                  onChangeText={value =>
                    this.setState({ inputPrice: value.replace(/[^0-9]/g, "") })
                  }
                  style={{
                    borderWidth: 0.6,
                    height: 45,
                    width: 200,
                    borderRadius: 5,
                    borderColor: "rgba(51,51,51,0.5)",
                    marginVertical: 15,
                    padding: 5
                  }}
                  placeholder={"Enter amount in $"}
                  keyboardType="number-pad"
                />
              </React.Fragment>
            ) : (
              <RNPickerSelect
                onValueChange={value => this.setState({ selectedPrice: value })}
                placeholder={placeholder}
                placeholderTextColor="rgba(102,102,102,0.7)"
                value={this.state.selectedPrice}
                Icon={() => {
                  return (
                    <Image
                      source={Images.downArrow}
                      resizeMode="contain"
                      style={{
                        height: hp("1%"),
                        top: Platform.OS == "ios" ? 5 : 0
                      }}
                    />
                  );
                }}
                style={{
                  fontFamily: "Comfortaa-Bold",
                  inputIOS: {
                    color: "rgb(51,51,51)"
                  },
                  viewContainer: {
                    height: 47,
                    width: 206,
                    fontFamily: "Comfortaa-Bold",
                    borderWidth: 0.6,
                    marginLeft: "auto",
                    marginRight: "auto",
                    justifyContent: "center",
                    borderColor: "rgba(200,200,200,1)",
                    borderRadius: 5,
                    padding: 5,
                    marginVertical: 15
                  }
                }}
                items={this.state.allowedPrice}
              />
            )}
            <TouchableOpacity
              style={{
                height: 47,
                width: 206,
                backgroundColor: "rgb(41,34,108)",
                justifyContent: "center",
                alignSelf: "center",
                borderRadius: 10
              }}
              onPress={() => this.submitGift()}
            >
              <Text
                style={{
                  color: "white",
                  alignSelf: "center",
                  fontFamily: "Comfortaa-Bold",
                  marginBottom: Platform.OS == "ios" ? 0 : 5,
                  fontSize: 16
                }}
              >
                Buy
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
        <Modal
          onModalHide={() => {}}
          isVisible={this.state.modalGiftVisible}
          style={{ flex: 1 }}
        >
          <View
            style={[
              styles.modalCard,
              { height: "auto", width: 255, justifyContent: "center" }
            ]}
          >
            <Image
              source={
                (this.state.message && Images.giftImage) ||
                (this.state.errorMessage && Images.Alert)
              }
              style={{
                height: this.state.message ? 83 : 40,
                width: this.state.message ? 82 : 42,
                alignSelf: "center",
                borderRadius: 10
              }}
              resizeMode="contain"
            />
            <Text
              style={{
                textAlign: "center",
                fontFamily: "Comfortaa-Bold",
                color: "rgb(51,51,51)",
                margin: "5%",
                width: 212,
                alignSelf: "center"
              }}
            >
              {this.state.message
                ? this.state.message
                : this.state.errorMessage}
              
            </Text>
            <TouchableOpacity
              style={{
                height: 47,
                width: 206,
                backgroundColor: "rgb(41,34,108)",
                justifyContent: "center",
                alignSelf: "center",
                borderRadius: 10,
                marginVertical: 10
              }}
              onPress={() =>
                this.setState({
                  modalGiftVisible: !this.state.modalGiftVisible,
                  message: "",
                  errorMessage: ""
                })
              }
            >
              <Text
                style={{
                  color: "white",
                  alignSelf: "center",
                  fontFamily: "Comfortaa-Bold",
                  marginBottom: Platform.OS == "ios" ? 0 : 5,
                  fontSize: 16
                }}
              >
                Ok
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>

        <ImageBackground
          source={Images.loginLayer}
          style={{ flex: 1, alignItems: "center" }}
        >
          <View style={[MainStyles.cardStyle]}>
            {this.state.brands ? (
              <React.Fragment>
                <FlatList
                  data={this.state.brands}
                  extraData={this.state.brands}
                  contentContainerStyle={{ alignSelf: "center" }}
                  numColumns={2}
                  refreshing={this.state.refreshing}
                  onRefresh={() => this.refreshData()}
                  renderItem={({ item }) => (
                    <TouchableOpacity
                      style={styles.giftCard}
                      onPress={() =>
                        this.retrieveBrand(
                          item.image_url,
                          item.name,
                          item.brand_code
                        )
                      }
                    >
                      <Image
                        source={{ uri: item.image_url }}
                        style={{ height: 133, width: 118 }}
                        resizeMode="contain"
                      />
                      <Text
                        style={{
                          fontFamily: "Comfortaa-Bold",
                          color: "rgb(51,51,51)",
                          fontSize: 12
                        }}
                      >
                        {item.name}
                      </Text>
                    </TouchableOpacity>
                  )}
                  keyExtractor={(item, index) => index.toString()}
                  showsVerticalScrollIndicator={false}
                />
              </React.Fragment>
            ) : (
              <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <ActivityIndicator size="large" />
              </View>
            )}
          </View>
        </ImageBackground>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  giftCard: {
    height: 173,
    width: 138,
    elevation: 6,
    backgroundColor: "white",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.6,
    shadowRadius: 2,
    borderRadius: 10,
    shadowColor: "#000",
    alignItems: "center",
    marginHorizontal: 10,
    marginVertical: 10,
    justifyContent: "center"
  },
  modalCard: {
    height: "auto",
    width: 255,
    backgroundColor: "white",
    borderRadius: 10,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    zIndex: -1,
    paddingVertical: 15
  }
});

const mapStateToProps = ({ auth }) => {
  const { accessToken, userId } = auth;
  return { accessToken, userId };
};

export default connect(mapStateToProps, null)(Gift);