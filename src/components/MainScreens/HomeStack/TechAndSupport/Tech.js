import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity, ImageBackground, Image} from 'react-native';
import TechStackNavigator from './topNavigator';
import { Images } from '../../../../assets/images';
import { NavigationService } from '../../../../navigator';

export default class TechAndSupp extends React.Component{
    static navigationOptions = ({navigation}) => {
        console.log('navigation TechAndSupp ==>',navigation)
      };
    constructor(props) {
        super(props)
    }
    render(){
        return(
            <ImageBackground style={{flex:1, alignItems:'center'}} 
            source={require('../../../../assets/loginLayer.jpg')}>
                <View style={styles.cardStyle}>
                    <TechStackNavigator />
                </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    cardStyle:{
        height:'95%',
        width: "90%",
        padding:'4%',
        paddingBottom: 0,
        paddingTop: 0,
        elevation: 5,
        backgroundColor: "white",
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.5,
        shadowRadius: 2
    },
})