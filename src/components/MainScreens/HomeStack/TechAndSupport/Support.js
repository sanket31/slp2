import React from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Platform,
  TextInput,
  Image,
  Alert,
  Linking
} from "react-native";
import { Images } from "../../../../assets/images";
import Modal from "@kalwani/react-native-modal";
import {
  Freshchat,
  FreshchatConfig,
  FreshchatNotificationConfig,
  FreshchatUser,
  FreshchatMessage,
  ConversationOptions
} from "react-native-freshchat-sdk";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import axios from "axios";
import { TechSupportApi } from "../../../../config/api";
import { connect } from "react-redux";
import Spinner from "react-native-spinkit";
import {changeRouteTechSupport} from '../../../../Actions'
import i18n from "../../../../i18n";
import AsyncStorage from "@react-native-community/async-storage";

class Support extends React.Component {
  static navigationOptions = {};
  constructor(props) {
    super(props);
    this.state = {
      isVisibleSubmit: false,
      message: "",
      responseMessage: "",
      userData: "",
      modalVisible: false,
      spinner: false,
      sendmessage: false
    };
  }

  componentDidMount() {
    console.log('support mount called')
    // this.props.navigation.jumpTo('SprayParameters');
    console.log('support data....', this.props.navigation.getParam('data'))
    if(this.props.routeTechSupport === 'SprayParameters') {
    this.props.navigation.navigate('SprayParameters', {
      data: this.props.navigation.getParam('data')
    })
    }
    if(this.props.routeTechSupport == 'Library'){
      this.props.navigation.navigate('Library' , {
        fromscan : true
      })
    }

    var freshchatConfig = new FreshchatConfig(
      "696c365a-b370-4fcf-bd9b-64d23f7668f2",
      "bcb84238-d459-4f2e-b7a8-593c4bfaada4"
    );
    Freshchat.init(freshchatConfig);
    this.getMyValue();

    // var userData = this.props.user_profile_data.data;
    // var fullName = userData.full_name.split(' ');
    // var freshchatUser = {};
    // freshchatUser.firstName = fullName[0];
    // freshchatUser.lastName = fullName[1];
    // freshchatUser.email = userData.email;
    // console.log('user', freshchatUser)
    // Freshchat.setUser(freshchatUser, error => {
    //   console.log(error);
    // });
  }

  componentWillUnmount() {
    this.props.changeRouteTechSupport('Support')
  }

  getMyValue = async () => {
    try {
      const value = await AsyncStorage.getItem("userData");
      const newValue = JSON.parse(value);
      console.log("value", newValue);
      this.setState({
      userData: newValue
      });
    } catch (e) {
      console.log(e);
    }
  };

  submitRequest = () => {
    if (this.state.message) {
      if (this.props.accessToken) {
        this.setState({ spinner: !this.state.spinner });
        axios(TechSupportApi.submitRequest, {
          headers: {
            "Accept-Language": i18n.language.includes("es") ? "es" : null
          },
          method: "POST",
          data: {
            access_token: this.props.accessToken,
            message: this.state.message
          }
        }).then(res => {
          this.setState({
            responseMessage: res.data.message,
            spinner: !this.state.spinner
          });
          if (this.state.responseMessage) {
            this.setState({ modalVisible: !this.state.modalVisible });
          }
        });
      }
    } else {
      Alert.alert("Please enter your request in the message box!");
    }
  };

  submithandler = () => {
    this.setState({
      sendmessage: true
    });
  };

   changeText = (value) => {
    if(this.state.sendmessage){
    this.setState({sendmessage:false, message:value})
    }else {
      this.setState({message:value})
    }
  }

  render() {
    if (this.state.sendmessage) {
      console.log("support", this.state.userData);
      const {userData} = this.state;

      var conversationOptions = new ConversationOptions();
      conversationOptions.tags = ["premium"];
      conversationOptions.filteredViewTitle = "Premium Support";
      Freshchat.showConversations(conversationOptions);

      var freshchatUser = new FreshchatUser();
      freshchatUser.firstName = userData.full_name;
      freshchatUser.lastName = "";
      freshchatUser.email = userData.email;

      Freshchat.setUser(freshchatUser, error => {
        console.log(error);
      });
    }
    return (
      <View style={{ flex: 1 }}>
        <Modal
          onModalHide={() => console.log("jide error")}
          isVisible={this.state.spinner}
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <Spinner
            style={styles.spinner}
            size={50}
            type={"Circle"}
            color={"white"}
          />
        </Modal>

        <Modal
          onModalHide={() => {}}
          hideModalContentWhileAnimating
          isVisible={this.state.modalVisible}
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <View
            style={{
              height: 180,
              justifyContent: "space-around",
              width: 295,
              backgroundColor: "white",
              borderRadius: 10,
              padding: 8
            }}
          >
            <Image
              source={Images.success}
              resizeMode="contain"
              style={{
                height: 40,
                alignSelf: "center"
              }}
            />
            <Text
              style={{
                textAlign: "center",
                fontFamily: "Comfortaa-Bold",
                color: "rgb(51,51,51)",
                width: 200,
                alignSelf: "center"
              }}
            >
              {this.state.responseMessage}
            </Text>
            <TouchableOpacity
              style={[
                styles.buttonModal,
                { height: hp("5.5%"), alignSelf: "center" }
              ]}
              onPress={() => {
                this.setState({
                  modalVisible: !this.state.modalVisible,
                  message: null
                });
              }}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: 16,
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                Ok
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>

        <View style={{ flex: 1 }}>
          <View style={{ flex: 0.2 }} />
          <Text style={styles.titleStyle}>
            {i18n.t("techSupport:submitReqLine")}
          </Text>
          <View style={{ flex: 0.2 }} />
          <Text style={styles.headerStyle}>
            {i18n.t("techSupport:message")}
          </Text>
          <TextInput
            numberOfLines={8}
            style={{
              flex: 2.5,
              borderWidth: 0.6,
              borderColor: "rgba(200,200,200,1)",
              fontFamily: "Comfortaa-Bold",
              fontWeight: "normal",
              textAlignVertical: "top",
              marginBottom: "10%",
              height: 120,
              borderRadius: 10,
              padding: 5,
              paddingTop: 10
            }}
            multiline={true}
            placeholder={i18n.t("techSupport:typeHere")}
            onChangeText={value => this.changeText(value)}
            value={this.state.message}
          />
          <View style={{ flex: 4 }}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => this.submitRequest()}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: 17,
                  fontFamily: "Comfortaa-Bold",
                  marginBottom: Platform.OS == "android" ? "2%" : 0
                }}
              >
                {i18n.t("common:submit")}
              </Text>
            </TouchableOpacity>
            <View style={{ margin: "5%" }}>
              <Text
                style={{
                  color: "rgb(41,34,108)",
                  textAlign: "center",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {i18n.t("techSupport:or")}
              </Text>
            </View>
            <TouchableOpacity
              style={styles.button2}
              onPress={() => Linking.openURL(`tel:${"+1203-760-0025"}`)}
            >
              <Text
                style={{
                  color: "rgb(41,34,108)",
                  textAlign: "center",
                  fontSize: 17,
                  fontFamily: "Comfortaa-Bold",
                  marginBottom: Platform.OS == "android" ? "2%" : 0
                }}
              >
                {i18n.t("techSupport:callNow")}
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={{
              flex: 1,
              flexDirection: "row",
              alignSelf: "flex-end",
              alignItems: "center"
            }}
            onPress={this.submithandler}
          >
            <View style={styles.chatContainer}>
              <Text
                style={{
                  width: "85%",
                  color: "white",
                  fontFamily: "Comfortaa-Bold",
                  textAlign: "center",
                  fontSize: 10,
                  transform: [{ translateY: Platform.OS == "ios" ? 0 : -1 }],
                  flexShrink: 1,
                  flexWrap: "wrap",
                  paddingHorizontal:5
                }}
                ellipsizeMode="tail"
                numberOfLines={1}
              >
                {i18n.t("techSupport:chatNow")}
              </Text>
            </View>
            <Image
              source={Images.chat}
              style={styles.chatStyle}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    height: "97%",
    width: "90%",
    padding: "4%",
    paddingBottom: 0,
    paddingTop: 0,
    elevation: 5,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2
  },
  titleStyle: {
    flex: 0.5,
    color: "rgb(51,51,51)",
    fontFamily: "Comfortaa-Bold",
    fontSize: 16
  },
  headerStyle: {
    flex: 0.6,
    fontSize: 14,
    color: "rgb(51,51,51)",
    fontFamily: "Comfortaa-Bold"
  },
  button: {
    height: 45,
    width: "100%",
    borderRadius: 8,
    justifyContent: "center",
    backgroundColor: "rgb(41,34,108)"
  },
  button2: {
    height: 45,
    width: "100%",
    borderRadius: 8,
    borderWidth: 1,
    borderColor: "rgb(41,34,108)",
    justifyContent: "center",
    backgroundColor: "white"
  },
  chatStyle: {
    height: "60%",
    width: "10%"
  },
  chatContainer: {
    backgroundColor: "rgb(41,34,108)",
    height: "42%",
    width: "28%",
    justifyContent: "center",
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    transform: [{ translateX: 10 }]
  },
  cardStyle: {
    height: "95%",
    width: "90%",
    elevation: 6,
    backgroundColor: "white",
    borderRadius: 10
  },
  modalStyle: {
    justifyContent: "center",
    alignItems: "center"
  },
  buttonModal: {
    width: "40%",
    borderRadius: 8,
    justifyContent: "center",
    backgroundColor: "rgb(41,34,108)"
  },
  modalStyle: {
    // flex: 1,
    height: 221,
    justifyContent: "space-around",
    width: 295,
    backgroundColor: "white",
    borderRadius: 10
  }
});

const mapStateToProps = ({ auth, userprofile, home }) => {
  const { accessToken } = auth;
  const { user_profile_data } = userprofile;
  const {routeTechSupport} = home;
  return { accessToken, user_profile_data, routeTechSupport };
};

export default connect(mapStateToProps, {changeRouteTechSupport})(Support);
