/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  Dimensions,
  View,
  Text,
  TouchableOpacity
} from "react-native";
import Helpshift from "helpshift-react-native";

const user = {
  identifier: "YOUR_UNIQUE_ID", // required if no email
  email: "jane@doe.com", // required if no identifier
  name: "Jane Doe", // optional
  authToken: "XXXXXXXX=" // required if User Identity Verification is enabled
};

const cifs = {
  // 'key': ['type', 'value']
  number_of_rides: ["n", "12"],
  street: ["sl", "343 sansome"],
  new_customer: ["b", "true"]
};

// Where data types are mapped like so:
// singleline => sl
// multiline => ml
// number => n
// date => dt
// dropdown => dd
// checkbox => b

const apiKey = "60035ecfa90ac35f3d989fd4aa38cf3e";
const domain = "freetrialsolulab.helpshift.com";
const appId = Platform.select({
  ios: "freetrialsolulab_platform_20200106132326663-28e50c47a689e0e",
  android: "freetrialsolulab_platform_20200106135514958-8e38f4cd21f9f1d"
});

class Faqs extends Component {
  componentDidMount(){
    Helpshift.init(apiKey, domain, appId);

  }

  faq = () => {
    // Helpshift.showConversation()
    Helpshift.showFAQs()

  }

  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text style={{ marginBottom: 10 }}>
          Click Help button for FAQ and Chat
        </Text>
        <TouchableOpacity
          style={{
            backgroundColor: "rgb(41,34,108)",
            paddingVertical: 10,
            paddingHorizontal: 20,
            borderRadius: 5
          }}
          onPress={() => this.faq()}
        >
          <Text style={{ color: "#fff" }}>Help</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
export default Faqs;
