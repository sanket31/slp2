import React, { Component } from "react";
import {
  Text,
  View,
  Dimensions,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import Modal from "@kalwani/react-native-modal";
import { Rating, AirbnbRating } from "react-native-ratings";
import { Images } from "../../../../assets/images";
import axios from "axios";
import { TechSupportApi } from "../../../../config/api";
import { connect } from "react-redux";
import i18n from "../../../../i18n";
class RatingModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      rating: 1,
    };
  }
  showButtonView = () => {
    if (this.state.loader) {
      return <ActivityIndicator size="large" color="#fff" />;
    } else {
      return (
        <Text
          style={{
            fontSize: 16,
            color: "#fff",
            fontFamily: "Comfortaa-Bold",
          }}
        >
          Submit
        </Text>
      );
    }
  };
  handleSubmit = () => {
    this.setState(
      {
        loader: true,
      },
      () => {
        const formdata1 = new FormData();
        formdata1.append("access_token", this.props.accessToken);
        formdata1.append("rating", this.state.rating);

        fetch(TechSupportApi.rating, {
          method: "POST",
          headers: {
            "Accept-Language": i18n.language.includes("es") ? "es" : null,
          },
          body: formdata1,
        })
          .then((res) => res.json())
          .then((res) => {
            if (res.status_code == 200) {
              this.setState(
                {
                  loader: false,
                },
                () => {
                  this.props.closeModal(false);
                }
              );
            } else {
              this.setState({
                loader: false,
              });
            }
          })
          .catch((err) => {
            this.setState({
              loader: false,
            });
          });
      }
    );
  };
  render() {
    return (
      <Modal
        animationIn={"slideInUp"}
        isVisible={this.props.visible}
        onModalHide={() => this.props.closeModal(false)}
        style={{ justifyContent: "center", alignItems: "center" }}
        
      >
        <View
          style={{
            height: Dimensions.get("screen").height / 2.8,
            backgroundColor: "#fff",
            borderRadius: 10,
            width: "90%",
            justifyContent: "space-around",
            alignItems: "center",
          }}
        >
          <TouchableOpacity
            style={{
              height: 35,
              width: 35,
              position: "absolute",
              right: 0,
              top: -15,
            }}
            onPress={() => this.props.closeModal(false)}
          >
            <Image
              style={{ height: "100%", width: "100%" }}
              source={Images.close}
            />
          </TouchableOpacity>
          <Text
            style={{
              fontSize: 15,
              color: "rgb(51,51,51)",
              fontFamily: "Comfortaa-Bold",
            }}
          >
            Let us know your experience
          </Text>
          <AirbnbRating
            defaultRating={1}
            onFinishRating={(rating) => {
              this.setState({ rating });
            }}
            showRating={false}
            size={32}
          />
          <TouchableOpacity
            onPress={() => this.handleSubmit()}
            style={{
              width: "80%",
              backgroundColor: "rgb(41,34,108)",
              height: 45,
              justifyContent: "center",
              alignItems: "center",
              borderRadius: 10,
            }}
          >
            {this.showButtonView()}
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}
const mapStateToProps = ({ auth }) => {
  const { accessToken } = auth;
  return { accessToken };
};

export default connect(mapStateToProps, null)(RatingModal);
