import React, { Fragment } from "react";
import { View, Text, StyleSheet, Image, Platform } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import RNPickerSelect from "react-native-picker-select";
import Icon from "react-native-vector-icons/Ionicons";
import { Images } from "../../../../assets/images";
import { connect } from "react-redux";
class QuestionPicker extends React.Component {
  constructor(props){
    super(props)
    this.state= {
      // basic_selected_value :  "",
      // advance_selected_value : "",
      selected_value : '',
    }
  }
  render() {
    const { page, advancequestionindex } = this.props;
    const { question, options, qid } = this.props.data;
    // console.log("questuib data", this.props.data);
    if(this.props.page == 'basic' && qid == 1) {
      return null
    } else {
      return (
        <Fragment key={qid.toString()}>
          <Text style={styles.question}>{`Q${qid}. ${question}`}</Text>
          <RNPickerSelect
            // onValueChange={value => this.props.onValueChange(value)}
            placeholder={{
              label: "Select",
              value: null,
              color: "rgb(102,102,102)"
            }}
            // value={
            //   page == "basic"
            //     ? this.props.basicQuestions.length > 0
            //       ? this.state.basic_selected_value
            //       : null
            //     : this.props.advanceQuestions.length > 0
            //     ? this.state.advance_selected_value
            //     : null
            // }
            value={
              page == "basic"
                ? this.props.basicQuestions.length > 0
                  ? this.state.selected_value
                  : null
                : this.props.advanceQuestions.length > 0
                ? this.state.selected_value
                : null
            }
            onValueChange={value => {
              const selecteddata = {
                qid: qid,
                question: question,
                selected_option: value
              };
              this.props.onValueChange(selecteddata);
              this.setState({
                selected_value : value
                // basic_selected_value : page == "basic" ? value : null,
                // advance_selected_value : page != "basic" ? value : null
              })
            }}
            items={options.map(item => {
              return {
                label: item.toString(),
                value: item,
                color: "rgb(51,51,51)"
              };
            })}
            Icon={() => {
              return (
                // <View style={{ marginLeft: 8 }}>
                  <Icon
                    name="ios-arrow-down"
                    type={"antdesign"}
                    style={{ marginRight: 10 }}
                    size={18}
                  />
                // </View>
              );
            }}
            style={{
              fontFamily: "Comfortaa-Bold",
              inputIOS: {
                color: "rgb(51,51,51)",
                paddingLeft: 10
              },
              viewContainer: styles.viewContainer
            }}
          />
        </Fragment>
      );
    }
 
  }
}
const mapStateToProps = ({ auth, TechSupport }) => {
  const { accessToken } = auth;
  const { loader, advanceQuestions, basicQuestions } = TechSupport;
  return { accessToken, advanceQuestions, basicQuestions };
};
export default connect(mapStateToProps)(QuestionPicker);
const styles = StyleSheet.create({
  titleStyle: {
    fontSize: 15,
    color: "rgb(51,51,51)",
    fontFamily: "Comfortaa-Bold",
    marginVertical: 10
  },
  question: {
    fontSize: 13,
    color: "rgb(51,51,51)",
    fontFamily: "Comfortaa-Bold",
    marginVertical: 10
  },
  viewContainer: {
    height: hp("5.5%"),
    width: wp("82%"),
    fontFamily: "Comfortaa-Bold",
    borderWidth: 0.6,
    marginLeft: "auto",
    marginRight: "auto",
    justifyContent: "center",
    borderColor: "rgba(200,200,200,1)",
    borderRadius: 5
  },
  button: {
    height: 47,
    width: "100%",
    borderRadius: 8,
    justifyContent: "center",
    alignSelf: "center",
    // alignItems: 'center',
    backgroundColor: "rgb(41,34,108)",
    marginVertical: 10
  }
});
