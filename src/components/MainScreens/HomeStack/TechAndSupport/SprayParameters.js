import React, { Fragment } from "react";
import {
  View,
  Text,
  ScrollView,
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import SnackBar from "react-native-snackbar-component";
import RNPickerSelect from "react-native-picker-select";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import QuestionPicker from "./QuestionPicker";
import { connect } from "react-redux";
import { TechSupportApi } from "../../../../config/api";
import axios from "axios";
import {
  setBasicSettings,
  getRecommendation,
  setLoader,
} from "../../../../Actions/sprayparameters";
import { ModalSpinner } from "../../../Spinner";
import Icon from "react-native-vector-icons/Ionicons";
import { NavigationEvents } from "react-navigation";

class SprayParameters extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      general_question: [],
      advance_question: [],
      showSnackBar: false,
      selected_value: null,
      question1: {},
    };
  }

  onPickerValueChange = (value) => {
    this.props.setBasicSettings(value);
  };

  renderSnackMessage = () => {
    return (
      <SnackBar
        visible={this.state.showSnackBar}
        accentColor="white"
        backgroundColor="rgb(41,34,108)"
        position="bottom"
        textMessage={"All Field Are Required"}
        autoHidingTime={3000}
        actionText="OK"
        actionHandler={() => {
          this.setState({
            showSnackBar: false,
          });
        }}
      />
    );
  };
  hanldeRecommendation = () => {
    if (this.props.basicQuestions.length < 5) {
      this.setState(
        {
          showSnackBar: true,
        },
        () => {
          setTimeout(() => {
            this.setState({
              showSnackBar: false,
            });
          }, 3000);
        }
      );
    } else {
      this.setState(
        {
          selected_value: "",
        },
        () => {
          this.props.setLoader(true);
          console.log('data...', this.props.basicQuestions);
          this.props.getRecommendation(
            this.props.accessToken,
            this.props.basicQuestions,
            this.props.navigation
          );
        }
      );
    }
  };
  componentDidMount() {
    axios(TechSupportApi.getSprayParametersQuestion(this.props.accessToken))
      .then((res) => {
        // console.log('question response......',res);
        if (res.status == 200) {
          const chuck = 5;
          this.setState(
            {
              selected_value: this.props.save_question_one,
              general_question: res.data.data.slice(0, chuck),
              advance_question: res.data.data.slice(
                chuck,
                res.data.data.length
              ),
            },
            () => {
              const selecteddata = {
                qid: this.state.general_question[0].qid,
                question: this.state.general_question[0].question,
                selected_option: this.props.save_question_one,
              };
              this.onPickerValueChange(selecteddata);
              this.props.setLoader(false);
            }
          );
        } else {
          alert("Server Error");
        }
      })
      .catch((err) => {
        console.log("question response err", err);
      });
  }
  showLoader = () => {
    return (
      <View>
        <ActivityIndicator
          size={"large"}
          color={"rgb(41,34,108)"}
          style={{ marginTop: "auto", marginBottom: "auto" }}
        />
      </View>
    );
  };
  render() {
    return (
      <View>
        {this.renderSnackMessage()}
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            paddingBottom: 20,
          }}
        >
          {/* <NavigationEvents
            onDidFocus={() => {
              console.log("did focus called");
              const selecteddata = {
                qid: this.state.general_question[0].qid,
                question: this.state.general_question[0].question,
                selected_option: this.props.save_question_one,
              };
              this.onPickerValueChange(selecteddata);
              this.props.setLoader(false);
            }}
          /> */}
          <ModalSpinner visible={this.props.loader} spinnerText={""} />
          <Text style={styles.titleStyle}>Basic Settings</Text>
          {this.state.general_question.length > 0 ? (
            <Fragment key={this.state.general_question[0].qid.toString()}>
              <Text
                style={styles.question}
              >{`Q${this.state.general_question[0].qid}. ${this.state.general_question[0].question}`}</Text>
              <RNPickerSelect
                placeholder={{
                  label: "Select",
                  value: null,
                  color: "rgb(102,102,102)",
                }}
                value={this.state.selected_value}
                onValueChange={(value) => {
                  const selecteddata = {
                    qid: this.state.general_question[0].qid,
                    question: this.state.general_question[0].question,
                    selected_option: value,
                  };
                  this.onPickerValueChange(selecteddata);
                  this.setState({
                    selected_value: value,
                  });
                }}
                items={this.state.general_question[0].options.map((item) => {
                  return {
                    label: item.toString(),
                    value: item,
                    color: "rgb(51,51,51)",
                  };
                })}
                Icon={() => {
                  return (
                    <Icon
                      name="ios-arrow-down"
                      type={"antdesign"}
                      style={{ marginRight: 10 }}
                      size={18}
                    />
                  );
                }}
                style={{
                  fontFamily: "Comfortaa-Bold",
                  inputIOS: {
                    color: "rgb(51,51,51)",
                    paddingLeft: 10,
                  },
                  viewContainer: styles.viewContainer,
                }}
              />
            </Fragment>
          ) : null}
          {this.state.general_question.length > 0 ? (
            this.state.general_question.map((item) => (
              <QuestionPicker
                page="basic"
                onValueChange={(data) => {
                  this.onPickerValueChange(data);
                }}
                data={item}
                spray_value={
                  this.props.navigation.getParam("data")
                    ? this.props.navigation.getParam("data")
                    : ""
                }
              />
            ))
          ) : (
            <View>{this.showLoader()}</View>
          )}
          {this.state.general_question.length > 0 ? (
            <Fragment>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("AdvanceQuestion", {
                    data: this.state.advance_question,
                  })
                }
                style={styles.button}
              >
                <Text
                  style={{
                    color: "white",
                    textAlign: "center",
                    fontSize: 15,
                    fontFamily: "Comfortaa-Bold",
                    //   marginVertical : 10
                  }}
                >
                  Advanced Settings
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.hanldeRecommendation();
                  // console.log("basic answer data....", this.props.basicQuestions);
                }}
                style={[
                  styles.button,
                  {
                    backgroundColor: "white",
                    borderWidth: 1,
                    borderColor: "rgb(41,34,108)",
                    marginVertical: 0,
                    marginBottom: 10,
                  },
                ]}
              >
                <Text
                  style={{
                    textAlign: "center",
                    fontSize: 15,
                    fontFamily: "Comfortaa-Bold",
                    color: "rgb(41,34,108)",
                  }}
                >
                  Get Recommendation
                </Text>
              </TouchableOpacity>
            </Fragment>
          ) : null}
        </ScrollView>
      </View>
    );
  }
}
const mapStateToProps = ({ auth, TechSupport }) => {
  const { accessToken } = auth;
  const { basicQuestions, loader, save_question_one } = TechSupport;
  return { accessToken, basicQuestions, loader, save_question_one };
};
const actionCreater = {
  setBasicSettings,
  getRecommendation,
  setLoader,
};

export default connect(mapStateToProps, actionCreater)(SprayParameters);
const styles = StyleSheet.create({
  titleStyle: {
    fontSize: 15,
    color: "rgb(51,51,51)",
    fontFamily: "Comfortaa-Bold",
    marginVertical: 10,
  },
  question: {
    fontSize: 13,
    color: "rgb(51,51,51)",
    fontFamily: "Comfortaa-Bold",
    marginVertical: 10,
  },
  viewContainer: {
    height: hp("5.5%"),
    width: wp("82%"),
    fontFamily: "Comfortaa-Bold",
    borderWidth: 0.6,
    marginLeft: "auto",
    marginRight: "auto",
    justifyContent: "center",
    borderColor: "rgba(200,200,200,1)",
    borderRadius: 5,
  },
  button: {
    height: 47,
    width: "100%",
    borderRadius: 8,
    justifyContent: "center",
    alignSelf: "center",
    // alignItems: 'center',
    backgroundColor: "rgb(41,34,108)",
    marginVertical: 10,
  },
});
