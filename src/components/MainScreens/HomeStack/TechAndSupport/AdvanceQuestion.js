import React, { Fragment, Component } from "react";
import {
  View,
  Text,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
  StyleSheet
} from "react-native";
import SnackBar from "react-native-snackbar-component";
import { connect } from "react-redux";
import {
  setBasicSettings,
  setAdvanceSettings,
  getRecommendation,
  setLoader
} from "../../../../Actions/sprayparameters";
import QuestionPicker from "./QuestionPicker";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
class AdvanceQuestion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showSnackBar: false,
      snackbar_msg: ""
    };
  }
  onPickerValueChange = value => {
    this.props.setAdvanceSettings(value);
  };
  renderSnackMessage = () => {
    return (
      <SnackBar
        visible={this.state.showSnackBar}
        accentColor="white"
        backgroundColor="rgb(41,34,108)"
        position="bottom"
        textMessage={this.state.snackbar_msg}
        actionText="OK"
        autoHidingTime={2000}
        actionHandler={() => {
          this.setState({
            showSnackBar: false
          });
        }}
      />
    );
  };
  hanldeRecommendation = () => {
    if (
      this.props.basicQuestions.length < 5 ||
      this.props.advanceQuestions.length < 8
    ) {
      this.setState(
        {
          showSnackBar: true,
          snackbar_msg:
            this.props.basicQuestions.length < 5
              ? "Basic Setting fields Required"
              : "Advance fields are required"
        },
        () => {
          setTimeout(() => {
            this.setState({
              showSnackBar: false
            });
          }, 2000);
        }
      );
    } else {
      this.props.setLoader(true);
      this.props.getRecommendation(
        this.props.accessToken,
        this.props.basicQuestions.concat(this.props.advanceQuestions),
        this.props.navigation
      );
    }
  };
  showLoader = () => {
    return (
      <View>
        <ActivityIndicator
          size={"large"}
          color={"rgb(41,34,108)"}
          style={{ marginTop: "auto", marginBottom: "auto" }}
        />
      </View>
    );
  };
  render() {
    const question = this.props.navigation.getParam("data");
    return (
      <Fragment>
        {this.renderSnackMessage()}
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            paddingBottom: 20
          }}
        >
          <Text style={styles.titleStyle}>Advanced Settings</Text>

          {question.length > 0
            ? question.map((item, index) => (
                <QuestionPicker
                  advancequestionindex={index}
                  page="advance"
                  onValueChange={data => {
                    this.onPickerValueChange(data);
                  }}
                  data={item}
                />
              ))
            : this.showLoader()}
          {question.length > 0 ? (
            <Fragment>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("SprayParameters")
                }
                style={styles.button}
              >
                <Text
                  style={{
                    color: "white",
                    textAlign: "center",
                    fontSize: 15,
                    fontFamily: "Comfortaa-Bold"
                    //   marginVertical : 10
                  }}
                >
                  Go Back
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.hanldeRecommendation()}
                style={[
                  styles.button,
                  {
                    backgroundColor: "white",
                    borderWidth: 1,
                    borderColor: "rgb(41,34,108)",
                    marginVertical: 0,
                    marginBottom: 10
                  }
                ]}
              >
                <Text
                  style={{
                    textAlign: "center",
                    fontSize: 15,
                    fontFamily: "Comfortaa-Bold",
                    color: "rgb(41,34,108)"
                  }}
                >
                  Get Recomendation
                </Text>
              </TouchableOpacity>
            </Fragment>
          ) : null}
        </ScrollView>
      </Fragment>
    );
  }
}

const mapStateToProps = ({ auth, TechSupport }) => {
  const { accessToken } = auth;
  const { loader, advanceQuestions, basicQuestions } = TechSupport;
  return { accessToken, advanceQuestions, loader, basicQuestions };
};
const actionCreater = {
  setBasicSettings,
  setAdvanceSettings,
  getRecommendation,
  setLoader
};

export default connect(mapStateToProps, actionCreater)(AdvanceQuestion);
const styles = StyleSheet.create({
  titleStyle: {
    fontSize: 15,
    color: "rgb(51,51,51)",
    fontFamily: "Comfortaa-Bold",
    marginVertical: 10
  },
  question: {
    fontSize: 13,
    color: "rgb(51,51,51)",
    fontFamily: "Comfortaa-Bold",
    marginVertical: 10
  },
  viewContainer: {
    height: hp("5.5%"),
    width: wp("82%"),
    fontFamily: "Comfortaa-Bold",
    borderWidth: 0.6,
    marginLeft: "auto",
    marginRight: "auto",
    justifyContent: "center",
    borderColor: "rgba(200,200,200,1)",
    borderRadius: 5
  },
  button: {
    height: 47,
    width: "100%",
    borderRadius: 8,
    justifyContent: "center",
    alignSelf: "center",
    // alignItems: 'center',
    backgroundColor: "rgb(41,34,108)",
    marginVertical: 10
  }
});
