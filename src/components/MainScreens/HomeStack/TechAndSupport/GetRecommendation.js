import React, { Fragment, Component } from "react";
import {
  View,
  Text,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
  StyleSheet,
  Platform
} from "react-native";
import { connect } from "react-redux";
import RatingModal from "./RatingModal";
import Helpshift from "helpshift-react-native";
const apiKey = "60035ecfa90ac35f3d989fd4aa38cf3e";
const domain = "freetrialsolulab.helpshift.com";
const appId = Platform.select({
  ios: "freetrialsolulab_platform_20200106132326663-28e50c47a689e0e",
  android: "freetrialsolulab_platform_20200106135514958-8e38f4cd21f9f1d",
});
class GetRecommendation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showmodal: false,
    };
  }
  toggleModal = (value) => {
    this.setState({
      showmodal: value,
    });
  };
  componentDidMount() {
    Helpshift.init(apiKey, domain, appId);
  }
  faq = () => {
    // Helpshift.showConversation()
    Helpshift.showFAQs();
  };
  render() {
    const {
      spray_foam_type,
      pressure_temp,
      a_side,
      b_side,
      hose_side,
      board_feet,
      density,
      notes,
    } = this.props.Recommendationdata;
    return (
      <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
        <RatingModal
          visible={this.state.showmodal}
          closeModal={(value) => this.toggleModal(value)}
        />
        <View style={[styles.outersection, { marginTop: 10 }]}>
          <Text style={styles.titletext}>Recommendation Settings For </Text>
          <Text style={styles.subtext}>{spray_foam_type}</Text>
        </View>
        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
          <View style={[styles.outersection, { width: "30%" }]}>
            <Text style={styles.titletext}>A-Heater</Text>
            <Text style={[styles.subtext, { color: "red" }]}>{a_side}</Text>
          </View>
          <View style={[styles.outersection, { width: "30%" }]}>
            <Text style={styles.titletext}>B-Heater </Text>
            <Text style={[styles.subtext, { color: "blue" }]}>{b_side}</Text>
          </View>
          <View style={[styles.outersection, { width: "30%" }]}>
            <Text style={styles.titletext}>Hose Heat </Text>
            <Text style={[styles.subtext, { color: "green" }]}>
              {hose_side}
            </Text>
          </View>
        </View>
        <View style={styles.outersection}>
          <Text style={styles.titletext}>Pressure Set</Text>
          <Text style={styles.subtext}>{`${pressure_temp} psi`}</Text>
        </View>
        <View style={styles.outersection}>
          <Text style={styles.titletext}>Notes </Text>
          <Text style={styles.subtext}>{notes != "" ? notes : "N/A"}</Text>
        </View>
        <View style={styles.outersection}>
          <Text style={styles.titletext}>Expected Approx Yield Per Set</Text>
          <Text style={styles.subtext}>{`${board_feet} +/- Board Feet`}</Text>
        </View>
        <View style={styles.outersection}>
          <Text style={styles.titletext}>Expected Approx Core Density</Text>
          <Text style={styles.subtext}>{`${density} lbs/cu.ft.+/-`}</Text>
        </View>
        <View style={styles.outersection}>
          <Text style={styles.titletext}>
            Chemical in Drum Must No Less Than
          </Text>
          <Text style={styles.subtext}>UPC 500 Max</Text>
        </View>
        <TouchableOpacity onPress={() => this.faq()} style={styles.button}>
          <Text
            style={{
              color: "white",
              textAlign: "center",
              fontSize: 15,
              fontFamily: "Comfortaa-Bold",
              //   marginVertical : 10
            }}
          >
            Ask in Community
          </Text>
        </TouchableOpacity>
      </ScrollView>
    );
  }
}
const mapStateToProps = ({ auth, TechSupport }) => {
  const { accessToken } = auth;
  const { Recommendationdata } = TechSupport;
  return { accessToken, Recommendationdata };
};
export default connect(mapStateToProps)(GetRecommendation);
const styles = StyleSheet.create({
  outersection: {
    width: "100%",
    marginBottom: 10,
    backgroundColor: "rgb(233 ,232 ,240)",
    // height : 60,
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 10,
    paddingVertical: 10,
    borderRadius: 6,
  },
  titletext: {
    fontSize: 12,
    color: "rgb(102,102,102)",
    fontFamily: "Comfortaa-Bold",
  },
  subtext: {
    fontSize: 14,
    color: "rgb(51 ,51 ,51)",
    fontFamily: "Comfortaa-Bold",
  },
  button: {
    height: 47,
    width: "100%",
    borderRadius: 8,
    justifyContent: "center",
    alignSelf: "center",
    // alignItems: 'center',
    backgroundColor: "rgb(41,34,108)",
    marginBottom: 10,
  },
});
