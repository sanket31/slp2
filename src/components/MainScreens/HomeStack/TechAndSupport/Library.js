import React from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ImageBackground,
  FlatList,
  ScrollView,
  Image,
  Platform,
  ActivityIndicator,
  Alert,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

import RNPickerSelect from "react-native-picker-select";
import { Images } from "../../../../assets/images";
import axios from "axios";
import { TechSupportApi } from "../../../../config/api";
import { connect } from "react-redux";
import Modal from "@kalwani/react-native-modal";
import Spinner from "react-native-spinkit";
import { NavigationService } from "../../../../navigator";
import i18n from "../../../../i18n";

class Library extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      merchantData: [],
      products: [],
      dataSheets: "",
      spinner: false,
      manufacturerName: "",
      imageModal: false,
      imageUrl: "",
      merchantname: '',
      merchant_name : ''
    };
  }

  componentDidMount() {
    axios(TechSupportApi.getMerchant + this.props.accessToken).then((res) => {
      this.setState({ merchantData: res.data.data }, () => {
        if (this.props.navigation.getParam("fromscan")) {
          this.fetchProducts(this.props.barrelData.merchant_name);
          console.log(this.props.barrelData.merchant_name);
        }
      });
    });
  }

  fetchProducts = (name) => {
    console.log('selected data', name)
    this.setState({
      merchant_name: name,
      manufacturerName: name,
      spinner: true,
      products: [],
      dataSheets: [],
      manufacturerName: "",
      dataSheets: "",
    });
    let data = this.state.merchantData.filter(
      (item) => item.full_name === name
    );
    axios(TechSupportApi.getProducts(this.props.accessToken, data[0].id)).then(
      (res) => {
        console.log("FETCHPRODUCTS", res.data);
        this.setState({
          products: res.data.data,
          manufacturerName: [],
          spinner: false,
        });
      }
    );
  };

  fetchDataSheet = (id) => {
    this.setState({ spinner: true });
    axios(TechSupportApi.getDataSheets(this.props.accessToken, id))
      .then((res) => {
        console.log("DATASHEET", res.data);
        this.setState({ dataSheets: res.data.data, spinner: false });
      })
      .catch((res) => {
        this.setState({ spinner: false });
      });
  };

  getPdfName = (fileUri, type) => {
    switch (type) {
      case "certificate":
        return fileUri.replace(
          "https://slp-static.s3.amazonaws.com/media/product/certificate/",
          ""
        );
      case "technical":
        return fileUri.replace(
          "https://slp-static.s3.amazonaws.com/media/product/tech_data/",
          ""
        );
      case "application":
        return fileUri.replace(
          "https://slp-static.s3.amazonaws.com/media/product/app_data/",
          ""
        );
      case "safety":
        return fileUri.replace(
          "https://slp-static.s3.amazonaws.com/media/product/safety_data/",
          ""
        );
      case "video":
        return fileUri.replace(
          "https://slp-static.s3.amazonaws.com/media/product/videos/",
          ""
        );
      case "equipment":
        return fileUri.replace(
          "https://slp-static.s3.amazonaws.com/media/product/equipment/",
          ""
        );
      case "industry":
        return fileUri.replace(
          "https://slp-static.s3.amazonaws.com/media/product/industry/",
          ""
        );
      case "building":
        return fileUri.replace(
          "https://slp-static.s3.amazonaws.com/media/product/building/",
          ""
        );
      case "quality":
        return fileUri.replace(
          "https://slp-static.s3.amazonaws.com/media/product/quality/",
          ""
        );
      default:
        return type;
    }
  };

  showImageModal = (image) => {
    this.setState({ imageUrl: image, imageModal: true });
  };

  render() {
    const placeholder = {
      label: i18n.t("techSupport:selectManufacturer"),
      value: null,
      color: "rgba(102,102,102,0.7)",
    };
    console.log("Satte", this.state.dataSheets);

    return (
      <View style={{ flex: 1 }}>
        <Modal onModalHide={() => {}} isVisible={this.state.spinner}>
          <Spinner
            style={{ alignSelf: "center" }}
            size={50}
            type={"Circle"}
            color={"white"}
          />
        </Modal>
        <Modal
          onModalHide={() => {}}
          isVisible={this.state.imageModal}
          style={{ flex: 1 }}
        >
          <View style={{ flex: 1, justifyContent: "center", zIndex: 1 }}>
            <TouchableOpacity
              onPress={() => this.setState({ imageModal: false })}
              style={{ zIndex: 1, width: "98%", marginBottom: -15 }}
            >
              <Image
                source={Images.close}
                style={{ height: 30, width: 30, alignSelf: "flex-end" }}
              />
            </TouchableOpacity>
            <Image
              source={{ uri: this.state.imageUrl }}
              style={{ zIndex: 0, width: "98%", height: 300 }}
              resizeMode="contain"
            />
          </View>
        </Modal>
        <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
          {this.state.merchantData.length > 0 ? (
            <React.Fragment>
              <View style={{ flex: 1 }}>
                <View style={{ margin: "2.5%" }} />
                <View style={{ justifyContent: "center" }}>
                  <RNPickerSelect
                    onValueChange={(value) => {
                      value
                        ? this.fetchProducts(value)
                        : this.setState({
                            products: [],
                            dataSheets: [],
                            manufacturerName: "",
                            dataSheets: "",
                          });
                    }}
                    placeholder={placeholder}
                    placeholderTextColor="rgba(102,102,102,0.7)"
                    Icon={() => {
                      return (
                        <Image
                          source={Images.downArrow}
                          resizeMode="contain"
                          style={{
                            height: hp("1%"),
                            top: Platform.OS == "ios" ? 5 : 0,
                          }}
                        />
                      );
                    }}
                    value={this.state.merchant_name}
                    style={{
                      fontFamily: "Comfortaa-Bold",
                      inputIOS: {
                        color: "rgb(51,51,51)",
                        paddingLeft: 10,
                      },
                      viewContainer: {
                        height: hp("5.5%"),
                        width: wp("82%"),
                        fontFamily: "Comfortaa-Bold",
                        borderWidth: 0.6,
                        marginLeft: "auto",
                        marginRight: "auto",
                        justifyContent: "center",
                        borderColor: "rgba(200,200,200,1)",
                        borderRadius: 5,
                      },
                    }}
                    items={this.state.merchantData.map((item) => {
                      return {
                        label: item.full_name,
                        value: item.full_name,
                        color: "rgb(51,51,51)",
                      };
                    })}
                  />
                </View>
              </View>
              <View style={{ margin: "1%" }} />
              {this.state.products.length > 0 ? (
                <View style={{ flex: 2 }}>
                  <Text style={styles.titleStyle}>
                    {i18n.t("techSupport:products")}
                  </Text>
                  <FlatList
                    data={this.state.products}
                    extraData={this.state.products}
                    renderItem={({ item }) => (
                      <TouchableOpacity
                        style={{
                          margin: hp("1.5%"),
                          marginLeft: 0,
                          marginTop: 0,
                        }}
                        onPress={() => this.fetchDataSheet(item.id)}
                      >
                        <Image
                          source={{ uri: item.image }}
                          style={[
                            styles.productImage,
                            {
                              height: hp("15%"),
                              width: hp("15%"),
                              borderRadius: 10,
                            },
                          ]}
                          resizeMode="cover"
                        />
                      </TouchableOpacity>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                  />
                </View>
              ) : (
                <Text style={{ alignSelf: "center", marginVertical: 10 }}>
                  {this.state.manufacturerName
                    ? i18n.t("techSupport:noManufactProduct")
                    : i18n.t("techSupport:selectManuLine")}
                </Text>
              )}
              <View style={{ margin: "1%" }} />
              {this.state.dataSheets ? (
                <React.Fragment>
                  <View style={{ flex: 4.5 }}>
                    <Text style={styles.titleStyle}>
                      {i18n.t("techSupport:certificates")}
                    </Text>
                    {this.state.dataSheets.certificate.length > 0 ? (
                      <FlatList
                        data={this.state.dataSheets.certificate}
                        renderItem={({ item }) => {
                          console.log("type", item, item.type);
                          if (item.type == "pdf") {
                            return (
                              <TouchableOpacity
                                style={{
                                  margin: hp("1.5%"),
                                  marginLeft: 0,
                                  marginTop: 0,
                                }}
                                onPress={() =>
                                  NavigationService.navigate("PDFLibrary", {
                                    pdfUrl: item.url,
                                  })
                                }
                              >
                                <Image
                                  source={Images.pdf}
                                  style={styles.productImage}
                                  resizeMode="contain"
                                />
                                <Text
                                  style={{
                                    textAlign: "center",
                                    width: wp("20%"),
                                  }}
                                >
                                  {this.getPdfName(item.url, "certificate")}
                                </Text>
                              </TouchableOpacity>
                            );
                          }
                          if (item.type == "image") {
                            return (
                              <TouchableOpacity
                                style={{
                                  margin: hp("1.5%"),
                                  marginLeft: 0,
                                  marginTop: 0,
                                }}
                                onPress={() => this.showImageModal(item.url)}
                              >
                                <Image
                                  source={{ uri: item.url }}
                                  style={styles.productImage}
                                  resizeMode="contain"
                                />
                              </TouchableOpacity>
                            );
                          }
                        }}
                        keyExtractor={(item, index) => index.toString()}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                      />
                    ) : (
                      <Text style={{}}>{i18n.t("techSupport:noCerti")}</Text>
                    )}
                  </View>
                  <View style={{ margin: "1%" }} />
                  <View style={{ flex: 4.5 }}>
                    <Text style={styles.titleStyle}>
                      {i18n.t("techSupport:techDataSheet")}
                    </Text>
                    {this.state.dataSheets.technical_datasheet.length > 0 ? (
                      <FlatList
                        data={this.state.dataSheets.technical_datasheet}
                        renderItem={({ item }) => (
                          <TouchableOpacity
                            style={{
                              margin: hp("1.5%"),
                              marginLeft: 0,
                              marginTop: 0,
                            }}
                            onPress={() =>
                              NavigationService.navigate("PDFLibrary", {
                                pdfUrl: item,
                              })
                            }
                          >
                            <Image
                              source={Images.pdf}
                              style={styles.productImage}
                              resizeMode="contain"
                            />
                            <Text
                              style={{
                                textAlign: "center",
                                width: wp("20%"),
                              }}
                            >
                              {this.getPdfName(item, "technical")}
                            </Text>
                          </TouchableOpacity>
                        )}
                        keyExtractor={(item, index) => index.toString()}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                      />
                    ) : (
                      <Text style={{}}>{i18n.t("techSupport:noTech")}</Text>
                    )}
                  </View>
                  <View style={{ margin: "1%" }} />
                  <View style={{ flex: 4.5 }}>
                    <Text style={styles.titleStyle}>
                      {i18n.t("techSupport:appGuideline")}
                    </Text>
                    {this.state.dataSheets.application_guideline.length > 0 ? (
                      <FlatList
                        data={this.state.dataSheets.application_guideline}
                        renderItem={({ item, index }) => {
                          if (item.type == "pdf") {
                            return (
                              <TouchableOpacity
                                style={styles.imageContainer}
                                onPress={() =>
                                  NavigationService.navigate("PDFLibrary", {
                                    pdfUrl: item.url,
                                  })
                                }
                              >
                                <Image
                                  source={Images.pdf}
                                  style={styles.productImage}
                                  resizeMode="contain"
                                />
                                <Text
                                  style={{
                                    textAlign: "center",
                                    width: wp("20%"),
                                  }}
                                >
                                  {this.getPdfName(item.url, "application")}
                                </Text>
                              </TouchableOpacity>
                            );
                          }
                          if (item.type == "video") {
                            return (
                              <TouchableOpacity
                                style={{
                                  margin: hp("1.5%"),
                                  marginLeft: 0,
                                  marginTop: 0,
                                }}
                                onPress={() =>
                                  NavigationService.navigate(
                                    "VideoPlayerLibrary",
                                    {
                                      url: item.url,
                                      id: index,
                                      libraryVideo: true,
                                    }
                                  )
                                }
                              >
                                <ImageBackground
                                  source={Images.Product2}
                                  style={styles.productImage}
                                >
                                  <Image
                                    source={Images.play}
                                    style={[
                                      styles.productImage,
                                      { height: hp("6%") },
                                    ]}
                                    resizeMode="contain"
                                  />
                                </ImageBackground>
                                <Text
                                  style={{
                                    textAlign: "center",
                                    width: wp("20%"),
                                  }}
                                >
                                  {this.getPdfName(item.url, "video")}
                                </Text>
                              </TouchableOpacity>
                            );
                          }
                        }}
                        keyExtractor={(item, index) => index.toString()}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                      />
                    ) : (
                      <Text style={{}}>{i18n.t("techSupport:noGuide")}</Text>
                    )}
                  </View>
                  <View style={{ margin: "1%" }} />
                  <View style={{ flex: 4.5 }}>
                    <Text style={styles.titleStyle}>
                      {i18n.t("techSupport:safetyDataSheet")}
                    </Text>
                    {this.state.dataSheets.safety_datasheet.length > 0 ? (
                      <FlatList
                        data={this.state.dataSheets.safety_datasheet}
                        renderItem={({ item }) => (
                          <TouchableOpacity
                            style={{
                              margin: hp("1.5%"),
                              marginLeft: 0,
                              marginTop: 0,
                            }}
                            onPress={() =>
                              NavigationService.navigate("PDFLibrary", {
                                pdfUrl: item,
                              })
                            }
                          >
                            <Image
                              source={Images.pdf}
                              style={styles.productImage}
                              resizeMode="contain"
                            />
                            <Text
                              style={{
                                textAlign: "center",
                                width: wp("20%"),
                              }}
                            >
                              {this.getPdfName(item, "safety")}
                            </Text>
                          </TouchableOpacity>
                        )}
                        keyExtractor={(item, index) => index.toString()}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                      />
                    ) : (
                      <Text style={{}}>{i18n.t("techSupport:noSafety")}</Text>
                    )}
                  </View>
                  {/* ADDINg News fields */}
                  <View style={{ margin: "1%" }} />
                  <View style={{ flex: 4.5 }}>
                    <Text style={styles.titleStyle}>
                      {/* {i18n.t("techSupport:certificates")} */}
                      Equipment
                    </Text>
                    {this.state.dataSheets.equipment.length > 0 ? (
                      <FlatList
                        data={this.state.dataSheets.equipment}
                        renderItem={({ item }) => {
                          console.log("type", item, item.type);
                          if (item.type == "pdf") {
                            return (
                              <TouchableOpacity
                                style={{
                                  margin: hp("1.5%"),
                                  marginLeft: 0,
                                  marginTop: 0,
                                }}
                                onPress={() =>
                                  NavigationService.navigate("PDFLibrary", {
                                    pdfUrl: item.url,
                                  })
                                }
                              >
                                <Image
                                  source={Images.pdf}
                                  style={styles.productImage}
                                  resizeMode="contain"
                                />
                                <Text
                                  style={{
                                    textAlign: "center",
                                    width: wp("20%"),
                                  }}
                                >
                                  {this.getPdfName(item.url, "equipment")}
                                </Text>
                              </TouchableOpacity>
                            );
                          }
                          if (item.type == "image") {
                            return (
                              <TouchableOpacity
                                style={{
                                  margin: hp("1.5%"),
                                  marginLeft: 0,
                                  marginTop: 0,
                                }}
                                onPress={() => this.showImageModal(item.url)}
                              >
                                <Image
                                  source={{ uri: item.url }}
                                  style={styles.productImage}
                                  resizeMode="contain"
                                />
                              </TouchableOpacity>
                            );
                          }
                        }}
                        keyExtractor={(item, index) => index.toString()}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                      />
                    ) : (
                      <Text style={{}}>{i18n.t("techSupport:noCerti")}</Text>
                    )}
                  </View>
                  {/* adding field2 */}
                  <View style={{ margin: "1%" }} />
                  <View style={{ flex: 4.5 }}>
                    <Text style={styles.titleStyle}>
                      {/* {i18n.t("techSupport:certificates")} */}
                      Industry
                    </Text>
                    {this.state.dataSheets.industry.length > 0 ? (
                      <FlatList
                        data={this.state.dataSheets.industry}
                        renderItem={({ item }) => {
                          console.log("type", item, item.type);
                          if (item.type == "pdf") {
                            return (
                              <TouchableOpacity
                                style={{
                                  margin: hp("1.5%"),
                                  marginLeft: 0,
                                  marginTop: 0,
                                }}
                                onPress={() =>
                                  NavigationService.navigate("PDFLibrary", {
                                    pdfUrl: item.url,
                                  })
                                }
                              >
                                <Image
                                  source={Images.pdf}
                                  style={styles.productImage}
                                  resizeMode="contain"
                                />
                                <Text
                                  style={{
                                    textAlign: "center",
                                    width: wp("20%"),
                                  }}
                                >
                                  {this.getPdfName(item.url, "industry")}
                                </Text>
                              </TouchableOpacity>
                            );
                          }
                          if (item.type == "image") {
                            return (
                              <TouchableOpacity
                                style={{
                                  margin: hp("1.5%"),
                                  marginLeft: 0,
                                  marginTop: 0,
                                }}
                                onPress={() => this.showImageModal(item.url)}
                              >
                                <Image
                                  source={{ uri: item.url }}
                                  style={styles.productImage}
                                  resizeMode="contain"
                                />
                              </TouchableOpacity>
                            );
                          }
                        }}
                        keyExtractor={(item, index) => index.toString()}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                      />
                    ) : (
                      <Text style={{}}>{i18n.t("techSupport:noCerti")}</Text>
                    )}
                  </View>
                  {/* adding filed3 */}
                  <View style={{ margin: "1%" }} />
                  <View style={{ flex: 4.5 }}>
                    <Text style={styles.titleStyle}>
                      {/* {i18n.t("techSupport:certificates")} */}
                      Building
                    </Text>
                    {this.state.dataSheets.building.length > 0 ? (
                      <FlatList
                        data={this.state.dataSheets.building}
                        renderItem={({ item }) => {
                          console.log("type", item, item.type);
                          if (item.type == "pdf") {
                            return (
                              <TouchableOpacity
                                style={{
                                  margin: hp("1.5%"),
                                  marginLeft: 0,
                                  marginTop: 0,
                                }}
                                onPress={() =>
                                  NavigationService.navigate("PDFLibrary", {
                                    pdfUrl: item.url,
                                  })
                                }
                              >
                                <Image
                                  source={Images.pdf}
                                  style={styles.productImage}
                                  resizeMode="contain"
                                />
                                <Text
                                  style={{
                                    textAlign: "center",
                                    width: wp("20%"),
                                  }}
                                >
                                  {this.getPdfName(item.url, "building")}
                                </Text>
                              </TouchableOpacity>
                            );
                          }
                          if (item.type == "image") {
                            return (
                              <TouchableOpacity
                                style={{
                                  margin: hp("1.5%"),
                                  marginLeft: 0,
                                  marginTop: 0,
                                }}
                                onPress={() => this.showImageModal(item.url)}
                              >
                                <Image
                                  source={{ uri: item.url }}
                                  style={styles.productImage}
                                  resizeMode="contain"
                                />
                              </TouchableOpacity>
                            );
                          }
                        }}
                        keyExtractor={(item, index) => index.toString()}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                      />
                    ) : (
                      <Text style={{}}>{i18n.t("techSupport:noCerti")}</Text>
                    )}
                  </View>
                  {/* adding field 4 */}
                  <View style={{ margin: "1%" }} />
                  <View style={{ flex: 4.5 }}>
                    <Text style={styles.titleStyle}>
                      {/* {i18n.t("techSupport:certificates")} */}
                      Quality
                    </Text>
                    {this.state.dataSheets.quality.length > 0 ? (
                      <FlatList
                        data={this.state.dataSheets.quality}
                        renderItem={({ item }) => {
                          console.log("type", item, item.type);
                          if (item.type == "pdf") {
                            return (
                              <TouchableOpacity
                                style={{
                                  margin: hp("1.5%"),
                                  marginLeft: 0,
                                  marginTop: 0,
                                }}
                                onPress={() =>
                                  NavigationService.navigate("PDFLibrary", {
                                    pdfUrl: item.url,
                                  })
                                }
                              >
                                <Image
                                  source={Images.pdf}
                                  style={styles.productImage}
                                  resizeMode="contain"
                                />
                                <Text
                                  style={{
                                    textAlign: "center",
                                    width: wp("20%"),
                                  }}
                                >
                                  {this.getPdfName(item.url, "quality")}
                                </Text>
                              </TouchableOpacity>
                            );
                          }
                          if (item.type == "image") {
                            return (
                              <TouchableOpacity
                                style={{
                                  margin: hp("1.5%"),
                                  marginLeft: 0,
                                  marginTop: 0,
                                }}
                                onPress={() => this.showImageModal(item.url)}
                              >
                                <Image
                                  source={{ uri: item.url }}
                                  style={styles.productImage}
                                  resizeMode="contain"
                                />
                              </TouchableOpacity>
                            );
                          }
                        }}
                        keyExtractor={(item, index) => index.toString()}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                      />
                    ) : (
                      <Text style={{}}>{i18n.t("techSupport:noCerti")}</Text>
                    )}
                  </View>
                </React.Fragment>
              ) : (
                <Text style={{ alignSelf: "center" }}></Text>
              )}
            </React.Fragment>
          ) : (
            <View style={{ alignItems: "center", justifyContent: "center" }}>
              <Text>{i18n.t("techSupport:pleaseSelect")}</Text>
            </View>
          )}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  titleStyle: {
    fontFamily: "Comfortaa-Bold",
    color: "rgb(51,51,51)",
    fontSize: 15,
    marginBottom: hp("1%"),
  },
  imageContainer: {
    margin: hp("1.5%"),
    marginLeft: 0,
    marginTop: 0,
  },
  productImage: {
    height: hp("11%"),
    width: hp("11%"),
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 8,
  },
});

const mapStateToProps = ({ auth, barrel }) => {
  const { accessToken } = auth;
  const { barrelData } = barrel;
  return { accessToken, barrelData };
};

export default connect(mapStateToProps, null)(Library);
