import React from "react";
import {
  Text,
  View,
  ImageBackground,
  StyleSheet,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions,
  Linking,
  Platform
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import { Avatar, Badge, Icon, withBadge } from "react-native-elements";
import { Images } from "../../../assets/images";
import { fetchDashBoard } from "../../../Actions";
import { getProfile } from "../../../Actions/profileActions";
import { connect } from "react-redux";
import { MainStyles } from "../../styles";
import firebase from "react-native-firebase";
import { NavigationService } from "../../../navigator";
import { NavigationEvents } from "react-navigation";
import i18n from "i18next";
import Carousel, { Pagination } from "react-native-snap-carousel";
import NoConnectionModal from "../../../utility/NoConnectionModal";
import { BASE_URL, authApi } from "../../../config/api";
import axios from 'axios';
import AsyncStorage from "@react-native-community/async-storage";
import {Freshchat} from 'react-native-freshchat-sdk';

const { width: viewportWidth, height: viewportHeight } = Dimensions.get(
  "window"
);

const notifications = firebase.notifications();

function wtp(percentage) {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
}

const slideWidth = wtp(90);
const itemWidth = slideWidth;

class Home extends React.Component { 

  constructor(props) {
    super(props);
    this.state = {
      checkedSpanish: false,
      checkedEnglish: true
    };
  }

  async componentDidMount() {
    // await this.props.fetchDashBoard(this.props.accessToken);
    await this.props.getProfile(this.props.accessToken);

    firebase
      .messaging()
      .hasPermission()
      .then(async hasPermission => {
        if (hasPermission) {
          this.subscribeToNotificationListeners();
          const badgeCount = await notifications.getBadge();
          this.props.navigation.setParams({
            badgeValue: badgeCount ? badgeCount : 0
          });
        } else {
          firebase
            .messaging()
            .requestPermission()
            .then(async () => {
              this.subscribeToNotificationListeners();
              const badgeCount = await notifications.getBadge();
              this.props.navigation.setParams({
                badgeValue: badgeCount ? badgeCount : 0
              });
            })
            .catch(error => {
              console.error(error);
            });
        }
      });

    // setTimeout(() => {

    // }, 4000);
  }

  subscribeToNotificationListeners() {
    const channel = new firebase.notifications.Android.Channel(
      "notification_channel_SLP", // To be Replaced as per use
      "Notifications", // To be Replaced as per use
      firebase.notifications.Android.Importance.Max
    ).setDescription(
      "A Channel To manage the notifications related to Application"
    );
    firebase.notifications().android.createChannel(channel);

    this.notificationListener = firebase
      .notifications()
      .onNotification(notification => {
        console.log("onNotification notification-->", notification);
        // Process your notification as required
        this.displayNotification(notification);
      });
  }

  displayNotification = async notification => {
    const badgeCount = await notifications.getBadge();
    if (Platform.OS === "android") {
      const localNotification = new firebase.notifications.Notification({
        sound: "default",
        show_in_foreground: true
      })
        .setNotificationId(notification._notificationId)
        .setTitle(notification._title)
        .setSubtitle(notification._subtitle)
        .setBody(notification._body)
        .setData(notification._data)
        .android.setChannelId("notification_channel_SLP") // e.g. the id you chose above
        // .android.setSmallIcon("ic_notification_icon") // create this icon in Android Studio
        // .android.setColor(colors.colorAccent) // you can set a color here
        .android.setPriority(firebase.notifications.Android.Priority.High);

      firebase
        .notifications()
        .displayNotification(localNotification)
        .then(() => {
          notifications.setBadge(badgeCount + 1);
          this.props.navigation.setParams({ badgeValue: badgeCount + 1 });
        })
        .catch(err => console.error(err));
    } else {
      const localNotification = new firebase.notifications.Notification({
        sound: "default",
        show_in_foreground: true
      })
        .setNotificationId(notification._notificationId)
        .setTitle(notification._title)
        .setSubtitle(notification._subtitle)
        .setBody(notification._body)
        .setData(notification._data);

      firebase
        .notifications()
        .displayNotification(localNotification)
        .then(() => {
          notifications.setBadge(badgeCount + 1);
          this.props.navigation.setParams({ badgeValue: badgeCount + 1 });
        })
        .catch(err => console.error(err));
    }
  };

  openLink = bannerURL => {
    Linking.canOpenURL(bannerURL).then(supported => {
      if (supported) {
        Linking.openURL(bannerURL);
      } else {
        console.log("Don't know how to open URI: " + bannerURL);
      }
    });
  };

  recallHomeData = async () => {
    await this.props.fetchDashBoard(this.props.accessToken);
    await this.props.getProfile(this.props.accessToken);
  };

  renderCheckBox = language => {
    if (language == "Spanish") {
      this.setState({
        checkedSpanish: true,
        checkedEnglish: false
      });
    }
    if (language == "English") {
      this.setState({
        checkedEnglish: true,
        checkedSpanish: false
      });
    }
  };


  logOut = async userToken => {
    let userKeys = ["userAccessToken", "userIdToken"];
    const token = {
      access_token: userToken
    };
    if (userToken !== null) {
      await axios
        .post(BASE_URL + authApi.logout, token)
        .then(res => {
          console.log("logout", res);
          AsyncStorage.multiRemove(userKeys);
          Freshchat.resetUser();
          NavigationService.navigate("Login");
        })
        .catch(err => {
          console.log("logoutError", err.response.data);
        });
    } else {
      return () => null;
    }
  };

  checkBlockUser = async () => {
    await this.props.fetchDashBoard(this.props.accessToken);
    console.log("DashN", this.props.dashboardData);
    if(this.props.dashboardData.is_blocked){
      this.logOut(this.props.accessToken)
    }else {
      console.log("USER IS NOT BLOCKED")
    }
  };

  renderItem = image => {
    // console.log("IMAGE", image.item);
    return (
      <View
        style={{
          flex: 1,
          overflow: "hidden",
          borderBottomLeftRadius: 10,
          borderBottomRightRadius: 10,
          borderTopLeftRadius: 0,
          borderTopRightRadius: 0
        }}
      >
        <TouchableOpacity
          style={{ flex: 1 }}
          onPress={() => this.openLink(image.item.banner_ad_url)}
        >
          <Image
            source={{ uri: image.item.banner_image }}
            style={{
              flex: 1,
              overflow: "hidden"
            }}
          />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          <ImageBackground
            source={require("../../../assets/loginLayer.jpg")}
            style={{ flex: 1 }}
          >
            <NavigationEvents
              onWillFocus={async () => {
                const badgeCount = await notifications.getBadge();
                this.props.navigation.setParams({
                  badgeValue: badgeCount ? badgeCount : 0
                });
                //Call whatever logic or dispatch redux actions and update the screen!
              }}
              onDidFocus={() => this.checkBlockUser()}
            />

            <NoConnectionModal
              onPress={this.recallHomeData}
              errorMessage={this.props.dashboardDataError}
              isVisible={this.props.dashboardFail}
            />

            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <View style={styles.cardStyle}>
                <View style={styles.headerStyle}>
                  <Text
                    style={{
                      fontSize: 16,
                      fontFamily: "Comfortaa-Bold",
                      flexWrap: "wrap",
                      flexShrink: 1
                    }}
                  >
                    {i18n.t("common:lifetimeEarnedPoints")}
                  </Text>
                  {this.props.dashboardData ? (
                    <Text
                      style={{
                        fontFamily: "Comfortaa-Bold",
                        fontSize: 16,
                        color: "black"
                      }}
                    >
                      {this.props.dashboardData.lifetime_earned_points}
                    </Text>
                  ) : (
                    <ActivityIndicator
                      size={"small"}
                      color={"rgb(41,34,108)"}
                    />
                  )}
                </View>
                <View style={{ flex: 1, marginTop: 10 }}>
                  <View style={styles.cardContainer}>
                    <TouchableOpacity
                      style={styles.innerCardStyle}
                      onPress={() =>
                        this.props.navigation.navigate("ScanScreen")
                      }
                    >
                      <Image
                        source={Images.scanCode}
                        resizeMode="contain"
                        style={{ height: "45%", alignSelf: "center" }}
                      />
                      <Text style={styles.textStyle}>
                        {i18n.t("home:sprayPoints")}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.innerCardStyle}
                      onPress={() =>
                        this.props.navigation.navigate("TechScreen")
                      }
                    >
                      <Image
                        source={Images.supportHome}
                        resizeMode="contain"
                        style={{ height: "45%", alignSelf: "center" }}
                      />
                      <Text style={styles.textStyle}>
                        {i18n.t("home:techSupport")}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{ marginTop: 10 }} />
                  <View style={styles.cardContainer}>
                    <TouchableOpacity
                      style={styles.innerCardStyle}
                      onPress={() => NavigationService.navigate("VideoScreen")}
                    >
                      <Image
                        source={Images.earn}
                        resizeMode="contain"
                        style={{ height: "45%", alignSelf: "center" }}
                      />
                      <Text style={styles.textStyle}>
                        {i18n.t("home:learnToEarnPoints")}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.innerCardStyle}
                      onPress={() =>
                        this.props.navigation.navigate("ContestList")
                      }
                    >
                      <Image
                        source={Images.referral}
                        resizeMode="contain"
                        style={{ height: "45%", alignSelf: "center" }}
                      />
                      <Text style={styles.textStyle}>
                        {/* {i18n.t("home:referralPoints")}
                         */}
                         Contest
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{ marginTop: "5%" }} />
                  {this.props.dashboardData && (
                    <Carousel
                      data={this.props.dashboardData.banners}
                      renderItem={this.renderItem}
                      // onSnapToItem={index => this.setState({ activeSlide: index })}
                      sliderWidth={slideWidth}
                      itemWidth={itemWidth}
                      autoplay
                      loop
                    />
                  )}
                </View>
              </View>
              <View style={{ margin: "2%" }} />
            </View>
          </ImageBackground>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    height: "97%",
    width: wp("90%"),
    // padding: "2%",
    paddingBottom: 0,
    paddingTop: 18,
    elevation: 5,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2
  },
  headerStyle: {
    height: 60,
    width: "90%",
    alignSelf: "center",
    backgroundColor: "rgb(229,227,237)",
    justifyContent: "space-around",
    flexDirection: "row",
    borderRadius: 8,
    alignItems: "center",
    padding: 10
  },
  innerCardStyle: {
    height: "100%",
    width: "42%",
    elevation: 5,
    justifyContent: "center",
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2
  },
  cardContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    height: "35%",
    margin: "2%",
    marginBottom: 0
  },
  textStyle: {
    marginTop: "5%",
    fontFamily: "Comfortaa-Bold",
    textAlign: "center",
    fontSize: 15,
    width: 120,
    color: "rgb(51,51,51)",
    alignSelf: "center"
  },
  imageStyle: {
    height: "100%",
    width: "7%"
  },
  imageModalStyle: {
    height: "100%",
    width: "15%"
  },
  textStyle2: {
    fontSize: 17,
    color: "black",
    marginLeft: "3%",
    fontFamily: "Comfortaa-Bold"
  },
  borderStyle: {
    borderWidth: 0.6,
    opacity: 0.05,
    marginTop: "2%"
  },
  button: {
    height: 47,
    width: "90%",
    borderRadius: 8,
    justifyContent: "center",
    alignSelf: "center",
    backgroundColor: "rgb(41,34,108)"
  }
});

const mapStateToProps = ({ auth, home }) => {
  const { userData, accessToken, userId } = auth;
  const { dashboardData, dashboardDataError, dashboardFail } = home;
  return {
    userData,
    dashboardData,
    accessToken,
    userId,
    dashboardDataError,
    dashboardFail
  };
};

const actionCreater = {
  fetchDashBoard,
  getProfile
};

export default connect(mapStateToProps, actionCreater)(Home);
