import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  Platform
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import axios from "axios";
import moment from "moment";
import { connect } from "react-redux";
import { homeApi } from "../../../config/api";
import { Images } from "../../../assets/images";
import { MainStyles } from "../../styles";
import firebase from "react-native-firebase";
import AsyncStorage from "@react-native-community/async-storage";

const notifications = firebase.notifications();

class Notifications extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: (
        <View
          style={{
            flexDirection: "row",
            flex: 1,
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <TouchableOpacity
            style={{ flex: 0.1 }}
            onPress={() => navigation.goBack()}
          >
            <Image
              source={require("../../../assets/settingsIcon/back.png")}
              resizeMode="contain"
              style={{
                height: hp("3%"),
                marginLeft: hp("2%"),
                alignSelf: "center",
                marginTop: "7%"
              }}
            />
          </TouchableOpacity>
          <Text
            style={{
              flex: 0.8,
              color: "white",
              fontSize: 20,
              alignSelf: "center",
              textAlign: "center",
              marginLeft: "auto",
              marginRight: "auto",
              fontFamily: "Comfortaa-Bold"
            }}
          >
            {"Notifications"}
          </Text>
          <View style={{ flex: 0.1 }} />
        </View>
      ),
      headerLeft: null,
      headerStyle: {
        backgroundColor: "rgb(41,34,108)",
        borderBottomWidth: 0,
        height: hp("7%"),
        elevation: 0
      }
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      data: "",
      loadingNotif: false
    };
  }

  componentDidMount = async () => {
    const badgeCount = await notifications.getBadge();
    notifications.setBadge(badgeCount - badgeCount);
    // this.props.navigation.setParams({ badgeValue: badgeCount - badgeCount });

    this.setState({ loadingNotif: true });
    await AsyncStorage.getItem(
      "userAccessToken",
      async (err, resAccessToken) => {
        if (resAccessToken !== null) {
          await AsyncStorage.getItem(
            "userIdToken",
            async (errId, resIdToken) => {
              console.log(resAccessToken, resIdToken);
              await axios
                .get(homeApi.notification(resIdToken, resAccessToken))
                .then(resNotif => {
                  console.log("notifications", resNotif);
                  const reverseDataNotif = resNotif.data.data;
                  this.setState({
                    data: reverseDataNotif.reverse(),
                    loadingNotif: false
                  });
                })
                .catch(notErr => {
                  console.log("errr", notErr.response.data);
                  this.setState({ loadingNotif: false });
                });
            }
          );
        }
      }
    );
  };

  render() {
    return (
      <ImageBackground source={Images.loginLayer} style={{ flex: 1 }}>
        <View style={[MainStyles.cardStyle, { padding: 0, paddingTop: 0 }]}>
          {this.state.data.length > 0 ? (
            <FlatList
              data={this.state.data}
              contentContainerStyle={{ paddingBottom: 10 }}
              renderItem={({ item }) => {
                return (
                  <TouchableOpacity style={styles.cardView} activeOpacity={0.6}>
                    <Text style={styles.content}>{item.notification_msg}</Text>
                    <Text style={styles.time}>
                      {" "}
                      {moment
                        .utc(item.created_at)
                        .local()
                        .format("YYYY-MM-DD hh:mm A")}
                    </Text>
                  </TouchableOpacity>
                );
              }}
            />
          ) : !this.state.data ? (
            <ActivityIndicator
              size={"large"}
              color={"rgb(41,34,108)"}
              style={{ marginTop: "auto", marginBottom: "auto" }}
            />
          ) : (
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text>You haven't received any notifications yet.</Text>
            </View>
          )}
        </View>
      </ImageBackground>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  const { accessToken, userId } = auth;
  return { accessToken, userId };
};

export default connect(mapStateToProps, null)(Notifications);

const styles = StyleSheet.create({
  cardView: {
    flex: 1,
    width: "92%",
    alignSelf: "center",
    marginTop: 15,
    borderBottomWidth: 0.6,
    borderBottomColor: "rgba(41,34,108,0.3)"
  },
  time: {
    color: "rgb(102,102,102)",
    fontFamily: "Comfortaa-Bold",
    lineHeight: 16,
    marginVertical: 10,
    fontSize: 12
  },
  content: {
    color: "rgb(51,51,51)",
    fontFamily: "Comfortaa-Bold",
    lineHeight: 16,
    fontSize: 14
  }
});
