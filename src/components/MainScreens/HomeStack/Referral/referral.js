import React from "react";
import {
  Text,
  View,
  ImageBackground,
  Image,
  StyleSheet,
  TouchableOpacity,
  Platform,
  ActivityIndicator,
  Share
} from "react-native";
import { connect } from "react-redux";
import AsyncStorage from "@react-native-community/async-storage";
import { homeApi } from "../../../../config/api";
import axios from "axios";
import { Images } from "../../../../assets/images";
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from "react-native-responsive-screen";
import i18n from "../../../../i18n";

class Referral extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      referralCode: null,
      referralMessage: null,
    };
  }

  async componentDidMount() {
    AsyncStorage.getItem("referralId", async (err, resRefToken) => {
      console.log("res", resRefToken);
      if (resRefToken === null) {
        await axios
          .get(homeApi.referral(this.props.accessToken))
          .then(resRef => {
            console.log("resreferral", resRef.data.data);
            AsyncStorage.setItem("referralId", resRef.data.data.referral_code);
            AsyncStorage.setItem("referralMsg", resRef.data.data.referral_msg);
            this.setState({
              referralCode: resRef.data.data.referral_code,
              referralMessage: resRef.data.data.referral_msg,
            });
          })
          .catch(errRef => {
            console.log("errReferral", errRef);
          });
      } else {
        AsyncStorage.getItem("referralMsg", (err, resMsgToken) => {
          console.log("resMsg", resMsgToken);
          this.setState({
            referralCode: resRefToken,
            referralMessage: resMsgToken,
          });
        });
      }
    });
  }

  shareRef = (msg) => {
    Share.share({
        message: msg,
      }, {
        // Android only:
        dialogTitle: 'Share Referral Code'
      });
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={require("../../../../assets/loginLayer.jpg")}
          style={{ flex: 1, alignItems: "center" }}
        >
          <View style={styles.cardStyle}>
            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                flex: 1,
              }}
            >
              <Image
                source={Images.referralBg}
                resizeMode="contain"
                style={styles.imageStyle}
              />
              <Text style={styles.textStyle}>
                {i18n.t("referralPage:earnPointGuide")}
              </Text>
              <View style={{ margin: "3%" }} />
              <View style={styles.referralContainer}> 
              {this.state.referralCode !== null ? (
                <Text style={styles.referralTextStyle}>{this.state.referralCode}</Text>) : (<ActivityIndicator size={'small'} color={'rgb(41,34,108)'} />)}
              </View>
              <View style={{ margin: "3%" }} />
              <TouchableOpacity style={styles.button} onPress={() => this.shareRef(this.state.referralMessage)}>
                <Text
                  style={{
                    color: "white",
                    textAlign: "center",
                    fontSize: 15,
                    fontFamily: "Comfortaa-Bold",
                    marginBottom: Platform.OS == "ios" ? 0 : 5,
                  }}
                >
                   {i18n.t("referralPage:share")}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    height: "95%",
    width: "90%",
    padding: "4%",
    paddingBottom: 0,
    paddingTop: 0,
    elevation: 5,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
  },
  imageStyle: {
    height: heightPercentageToDP("40%"),
    width: widthPercentageToDP("60%"),
  },
  textStyle: {
    textAlign: "center",
    fontFamily: "Comfortaa-Bold",
    fontSize: 13,
  },
  referralTextStyle: {
    color: "rgb(41,34,108)",
    fontFamily: "Comfortaa-Bold",
    fontSize: 11,
  },
  referralContainer: {
    borderRadius: 1,
    borderWidth: 2,
    borderStyle: "dotted",
    width: widthPercentageToDP("18%"),
    padding: heightPercentageToDP("0.1%"),
    justifyContent: "center",
    alignItems: "center",
    borderColor: "rgb(41,34,108)",
    backgroundColor: "rgba(41,34,108,0.1)",
  },
  button: {
    height: 47,
    width: "90%",
    borderRadius: 8,
    justifyContent: "center",
    backgroundColor: "rgb(41,34,108)",
  },
});

const mapStateToProps = ({ auth }) => {
  const { accessToken } = auth;
  return { accessToken };
};

export default connect(mapStateToProps)(Referral);
