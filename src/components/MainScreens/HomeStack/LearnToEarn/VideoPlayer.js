import React, { Component } from "react";
import { Button as ElementButton, Icon } from "react-native-elements";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import {
  View,
  Text,
  StyleSheet,
  Button,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  Platform,
  BackHandler,
} from "react-native";
// `import JWPlayer from "react-native-jw-media-player";`
import { MainRouter, NavigationService } from "../../../../navigator";
import Orientation from "react-native-orientation-locker";
import { SafeAreaView } from "react-navigation";
// import JWPlayer, { JWPlayerState } from "react-native-jw-media-player";
const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;
import VideoPlayer from 'react-native-video-controls';
const styles = StyleSheet.create({
  container: {
    backgroundColor: "rgba(0, 0, 0, 0.9)",
    flex: 1,
  },
  player: {
    flex: 1,
  },
});

class VideoPlayers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      jwplayer: {
        styles: {
          flex: 0.5,
        },
      },
      btnView: true,
      statusbar: true,
      fullScreen: false,
      portraitHeight: null,
      backgroundColor: "rgb(41,34,108)",
    };
    this.player = ''
  }

  componentDidMount() {
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      this.props.navigation.goBack(); // works best when the goBack is async
      return true;
    });
    Orientation.addOrientationListener(this._onOrientationDidChange);
  }

  onFullScreen = () => {
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      Orientation.lockToPortrait(); // works best when the goBack is async
      return true;
    });
    this.setState(
      {
        jwplayer: {
          styles: {
            flex: 1,
          },
        },
        btnView: false,
        fullScreen: true,
        statusbar: false,
        backgroundColor: "transparent",
      },
      () => console.log(this.state.fullScreen)
    );
    if (Platform.OS == "ios") {
      Orientation.unlockAllOrientations();
    }
  };
  onFullScreenExit = () => {
    this.backHandler.remove();
    this.setState(
      {
        jwplayer: {
          styles: {
            flex: 0.5,
          },
        },
        btnView: true,
        fullScreen: false,
        statusbar: true,
        backgroundColor: "rgb(41,34,108)",
      },
      () => console.log(this.state.fullScreen)
    );
    if (Platform.OS == "ios") {
      Orientation.lockToPortrait();
    }
  };

  _onOrientationDidChange = (orientation) => {
    if (orientation == "LANDSCAPE-LEFT") {
      if (Platform.OS == "ios") {
        Orientation.lockToLandscapeLeft();
      }
      //do something with landscape left layout
    } else if (orientation == "LANDSCAPE-RIGHT") {
      if (Platform.OS == "ios") {
        Orientation.lockToLandscapeRight();
      }
      //do something with portrait layout
    }
  };

  componentWillUnmount() {
    // this.player.stop();s
  }

  render() {
    console.log('params....', this.props.navigation.state.params)
    const playlistItem = {
      title: this.props.navigation.state.params.videoName,
      mediaId: this.props.navigation.state.params.id.toString(),
      image: this.props.navigation.state.params.image,
      desc: this.props.navigation.state.params.description,
      time: 0,
      file: this.props.navigation.state.params.url,
      autostart: true,
      controls: true,
      repeat: false,
      displayDescription: true,
      displayTitle: true,
    };

    const libraryPlaylistItem = {
      // title: this.props.navigation.state.params.videoName,
      mediaId: this.props.navigation.state.params.id.toString(),
      // image: this.props.navigation.state.params.image,
      // desc: this.props.navigation.state.params.description,
      time: 0,
      file: this.props.navigation.state.params.url,
      autostart: true,
      controls: true,
      repeat: false,
      displayDescription: true,
      displayTitle: true,
    };

    return (
      <View style={styles.container}>
        {this.state.btnView ? (
          <SafeAreaView forceInset={{ bottom: "never" }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.goBack();
              }}
              activeOpacity={0.8}
              style={{
                // marginBottom: 20,
                height: 50,
                justifyContent: "center",
                marginTop: 20,
                width: 50,
                marginRight: "auto",
                marginLeft: hp("1.3%"),
              }}
            >
              <Icon name="left" type="antdesign" color="white" size={25} />
            </TouchableOpacity>
          </SafeAreaView>
        ) : null}
        <StatusBar
          hidden={!this.state.statusbar}
          backgroundColor={this.state.backgroundColor}
        />
        <View style={this.state.jwplayer.styles}>
          {/* <JWPlayer
            ref={p => (this.JWPlayer = p)}
            style={{ flex: 1 }}
            playlistItem={
              this.props.navigation.state.params.libraryVideo
                ? libraryPlaylistItem
                : playlistItem
            } // Recommended - pass the playlistItem as a prop into the player
            // playlist={[playlistItem]}
            // onBeforePlay={() => this.onBeforePlay()}
            onPlay={() => console.log("playing")}
            onPause={() => console.log("pause called")}
            onIdle={() => console.log("onIdle")}
            onSetupPlayerError={event => console.log(event)}
            onPlayerError={event => console.log(event)}
            onBuffer={() => console.log("buffer")}
            onTime={event => {}}
            onFullScreen={() => this.onFullScreen()}
            onFullScreenExit={() => this.onFullScreenExit()}
          /> */}
          <VideoPlayer
            navigator={this.props.navigation}
            source={{ uri: this.props.navigation.state.params.url}}
            ref={(ref) => {
              this.player = ref;
            }} // Store reference
            onBuffer={this.onBuffer} // Callback when remote video is buffering
            onError={(err) => console.log(err)} // Callback when video cannot be loaded
            toggleResizeModeOnFullscreen={true}
            thumbnail={{ uri:  this.props.navigation.state.params.image }}
            resizeMode="contain"
            tapAnywhereToPause={true}
            onPause={() => console.log("pause called")}
            onEnterFullscreen={this.onFullScreen}
            onExitFullscreen={this.onFullScreenExit}

          />
        </View>

        {this.state.btnView ? (
          <View style={{ flex: 0.3 }}>
            {this.props.navigation.state.params.quizDelete === false ? (
              <ElementButton
                onPress={() => {
                  this.props.navigation.state.params.status
                    ? this.props.navigation.replace("Result", {
                        id: this.props.navigation.state.params.id,
                      })
                    : this.props.navigation.replace("Quiz", {
                        id: this.props.navigation.state.params.id,
                      });
                }}
                title={
                  this.props.navigation.state.params.status
                    ? "View Result"
                    : "Begin Quiz"
                }
                type="outline"
                titleStyle={{
                  color: "rgb(41,34,108)",
                  textAlign: "center",
                  textAlignVertical: "center",
                  alignSelf: "center",
                  fontSize: 16,
                  fontFamily: "Comfortaa-Bold",
                  paddingBottom: Platform.OS == "android" ? 5 : 0,
                }}
                buttonStyle={{
                  backgroundColor: "white",
                  borderWidth: 1,
                  alignItems: "center",
                  height: 47,
                  width: Dimensions.get("window").width - 50,
                  alignSelf: "center",
                  borderRadius: 8,
                }}
                // style={{backgroundColor:'white'}}
                containerStyle={{
                  marginTop: 60,
                  alignItems: "center",
                  // borderColor: "rgb(41,34,108)",
                  // width: wp("40%"),
                }}
              />
            ) : null}
          </View>
        ) : null}
      </View>
    );
  }
}

export default VideoPlayers;
