import React from "react";
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  Image,
  ImageBackground,
  TouchableOpacity,
  ActivityIndicator,
  Platform,
  Alert
} from "react-native";
import axios from "axios";
import RNPickerSelect from "react-native-picker-select";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import { MainRouter, NavigationService } from "../../../../navigator";
import { Images } from "../../../../assets/images";
import { LearnToEarnAPI } from "../../../../config/api";
import { connect } from "react-redux";
import i18n from "../../../../i18n";
import { NavigationEvents } from "react-navigation";

class LearnToEarn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: false,
      categoryData: [],
      selectedCategory: "",
      noVideo: false
    };
  }

  async componentDidMount() {
    if (this.props.accessToken) {
      if(this.state.data.length == 0){
      this.setState({ loading: true });
      }
      try {
        axios
          .get(LearnToEarnAPI.videoList + this.props.accessToken)
          .then(res => {
            console.log("VIDEOLIST", res.data);
            this.setState({ data: res.data.data, loading: false });
          });
      } catch (error) {
        console.log("ERROR", error);
        this.setState({ loading: false });
        Alert.alert("Something went wrong!", "Please try again later..");
      }
      try {
        axios
          .get(LearnToEarnAPI.categoryList + this.props.accessToken)
          .then(res => {
            let categoryData = res.data.data.map(item => {
              return {
                label: item.category_name,
                value: item.category_name,
                color: "rgb(51,51,51)"
              };
            });
            this.setState({ categoryData: categoryData, loading: false });
          });
      } catch (error) {
        this.setState({ loading: false });
        Alert.alert("Something went wrong!", "Please try again later..");
      }
    }
  }

  componentWillUnmount() {}

  renderNoVideo = () => {
    if (this.state.data && this.state.selectedCategory) {
      let videoSelected = this.state.data.filter(
        item => item.category == this.state.selectedCategory
      );
      console.log("VIDEO", videoSelected.length);
     return videoSelected.length == 0 ?  (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Text> {i18n.t("learnToEarn:noVideo")}</Text>
        </View>
      ) : null
    }
  }

  render() {
    const { navigate } = this.props.navigation;
    const placeholder = {
      label: "All",
      color: "rgb(51,51,51)"
    };

    return (
      <ImageBackground
        source={require("../../../../assets/loginLayer.jpg")}
        style={{ flex: 1 }}
      >
        <View style={{ flex: 1, alignItems: "center" }}>
          <View style={styles.cardStyle}>

          <NavigationEvents
              onWillFocus={async () => this.componentDidMount()}
            />

            {this.state.loading ? (
              <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignContent: "center"
                }}
              >
                <ActivityIndicator size={"large"} />
              </View>
            ) : (
              <View style={{ flex: 1 }}>
                <View style={{ margin: "1%" }} />
                <RNPickerSelect
                  onValueChange={value =>
                    this.setState({ selectedCategory: value })
                  }
                  placeholder={placeholder}
                  placeholderTextColor="rgb(51,51,51)"
                  Icon={() => {
                    return (
                      <Image
                        source={Images.downArrow}
                        resizeMode="contain"
                        style={{
                          height: hp("1%"),
                          top: Platform.OS == "ios" ? 5 : 0
                        }}
                      />
                    );
                  }}
                  style={{
                    fontFamily: "Comfortaa-Bold",
                    inputIOS: {
                      color: "rgb(51,51,51)",
                      paddingLeft: 10
                    },
                    viewContainer: {
                      height: hp("5.5%"),
                      width: wp("82%"),
                      fontFamily: "Comfortaa-Bold",
                      borderWidth: 0.6,
                      marginLeft: "auto",
                      marginRight: "auto",
                      justifyContent: "center",
                      borderColor: "rgba(200,200,200,1)",
                      borderRadius: 5,
                      padding: 10
                    }
                  }}
                  items={this.state.categoryData}
                />
                <View style={{ margin: "1%" }} />

                  {this.renderNoVideo()}

                {this.state.data.length > 0 ? (
                  <FlatList
                    style={{ marginTop: 8 }}
                    data={this.state.data}
                    extraData={this.state}
                    renderItem={({ item }) => {
                    
                      if (this.state.selectedCategory === item.category) {
                        return (
                          <View style={{ flex: 1 }}>
                            <TouchableOpacity
                              style={{
                                flex: 1,
                                flexDirection: "row",
                                alignItems: "center"
                              }}
                              onPress={() =>
                                NavigationService.navigate("VideoPlayer", {
                                  id: item.id,
                                  url: item.upload_video,
                                  videoName: item.video_name,
                                  decription: item.video_desc,
                                  status: item.quiz_status,
                                  quizDelete: item.quiz_delete_status,
                                  image: item.video_thumbnail
                                })
                              }
                            >
                              <ImageBackground
                                source={{
                                  uri: item.video_thumbnail
                                }}
                                style={styles.imageStyle}
                                imageStyle={{ borderRadius: 10 }}
                              >
                                <Image
                                  source={Images.play}
                                  resizeMode="contain"
                                  style={{ height: 25, width: 25 }}
                                />
                              </ImageBackground>
                              <View
                                style={{ flexDirection: "column", flex: 1 }}
                              >
                                <View style={{ flex: 0.1 }}>
                                  <Text style={styles.titleStyle}>
                                    {item.video_name}
                                  </Text>
                                </View>
                                <View style={{ flex: 0.8 }}>
                                  <Text style={styles.subtitle1Style}>
                                    {item.video_desc}
                                  </Text>
                                </View>
                                <View style={{ flex: 0.8 }}>
                                  <Text style={styles.subtitle1Style}>
                                    {item.video_length}
                                  </Text>
                                </View>
                                <View style={{ flex: 0.1 }}>
                                  <Text style={styles.subtitle2Style}>
                                    {i18n.t("title:rewardPoints")} :{" "}
                                    {item.reward_point}
                                  </Text>
                                </View>
                              </View>
                            </TouchableOpacity>
                            <View style={styles.marginStyle} />
                          </View>
                        );
                      }
                      if (!this.state.selectedCategory) {
                        console.log("NOT SELE");
                        return (
                          <View style={{ flex: 1 }}>
                            <TouchableOpacity
                              style={{
                                flex: 1,
                                flexDirection: "row",
                                alignItems: "center"
                              }}
                              onPress={() =>
                                NavigationService.navigate("VideoPlayer", {
                                  id: item.id,
                                  url: item.upload_video,
                                  videoName: item.video_name,
                                  decription: item.video_desc,
                                  status: item.quiz_status,
                                  quizDelete: item.quiz_delete_status,
                                  image: item.video_thumbnail
                                })
                              }
                            >
                              <ImageBackground
                                source={{
                                  uri: item.video_thumbnail
                                }}
                                style={styles.imageStyle}
                                imageStyle={{ borderRadius: 10 }}
                              >
                                <Image
                                  source={Images.play}
                                  resizeMode="contain"
                                  style={{ height: 25, width: 25 }}
                                />
                              </ImageBackground>
                              <View
                                style={{ flexDirection: "column", flex: 1 }}
                              >
                                <View style={{ flex: 0.1 }}>
                                  <Text style={styles.titleStyle}>
                                    {item.video_name}
                                  </Text>
                                </View>
                                <View style={{ flex: 0.8 }}>
                                  <Text style={styles.subtitle1Style}>
                                    {item.video_desc}
                                  </Text>
                                </View>
                                <View style={{ flex: 0.8 }}>
                                  <Text style={styles.subtitle1Style}>
                                    {item.video_length}
                                  </Text>
                                </View>
                                <View style={{ flex: 0.1 }}>
                                  <Text style={styles.subtitle2Style}>
                                    Reward Point : {item.reward_point}
                                  </Text>
                                </View>
                              </View>
                            </TouchableOpacity>
                            <View style={styles.marginStyle} />
                          </View>
                        );
                      }
                    }}
                    keyExtractor={item => item.id.toString()}
                    showsVerticalScrollIndicator={false}
                  />
                ) : !this.state.data ? (
                  <ActivityIndicator
                    size={"large"}
                    color={"rgb(41,34,108)"}
                    style={{ marginTop: "auto", marginBottom: "auto" }}
                  />
                ) : (
                  <View
                    style={{
                      flex: 1,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text> {i18n.t("learnToEarn:noVideo")}</Text>
                  </View>
                )}
              </View>
            )}
            
          </View>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    height: "95%",
    width: "90%",
    padding: "1%",
    paddingTop: 10,
    elevation: 5,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2
  },
  playStyle: {
    height: "30%",
    alignSelf: "center"
  },
  container: {
    flex: 1
  },
  body: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    padding: 30
  },
  vids: {
    paddingBottom: 30,
    width: 320,
    alignItems: "center",
    backgroundColor: "#fff",
    justifyContent: "center",
    borderBottomWidth: 0.6,
    borderColor: "#aaa"
  },
  titleStyle: {
    fontSize: 15,
    fontFamily: "Comfortaa-Bold",
    color: "rgb(51,51,51)",
    // marginTop: "4%",
    fontWeight: "bold"
  },
  imageStyle: {
    height: hp("10%"),
    width: wp("21%"),
    marginRight: "4%",
    marginLeft: "4%",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  subtitle1Style: {
    fontSize: 13,
    fontFamily: "Comfortaa-Bold",
    color: "rgb(51,51,51)"
    // marginTop: "3%",
  },
  subtitle2Style: {
    fontSize: 11,
    fontFamily: "Comfortaa-Bold"
    // marginBottom: "2%",
  },
  marginStyle: {
    margin: hp("2%"),
    borderWidth: 0.6,
    opacity: 0.1
  }
});

const mapStateToProps = ({ auth }) => {
  const { accessToken } = auth;
  return { accessToken };
};
export default connect(mapStateToProps, null)(LearnToEarn);
