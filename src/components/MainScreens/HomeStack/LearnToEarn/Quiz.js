import React from "react";
import {
  View,
  Text,
  ScrollView,
  ImageBackground,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  Image,
  Platform,
  Alert
} from "react-native";
import RadioButton from "react-native-radio-button";
// import {
//   heightPercentageToDP as hp,
//   widthPercentageToDP as wp
// } from "react-native-responsive-screen";
import { Images } from "../../../../assets/images";
import axios from "axios";
import { LearnToEarnAPI, BASE_URL } from "../../../../config/api";
import Modal from "@kalwani/react-native-modal";
import { connect } from "react-redux";
import Spinner from "react-native-spinkit";
import i18n from "../../../../i18n";

class Quiz extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      quizData: "",
      selectedId: "",
      answerList: [],
      errorModal: false,
      spinner: false,
      message:'',
      result:false
    };
  }

  componentDidMount() {
    if (this.props.accessToken) {
      axios
        .get(
          BASE_URL +
            "quiz_question/" +
            this.props.navigation.state.params.id +
            "?access_token=" +
            this.props.accessToken, {
              headers: {
                'Accept-Language': i18n.language.includes('es') ? 'es' : null
              }
            }
        )
        .then(res => {
          console.log("quizData", res);
          this.setState({ quizData: res.data.data });
        });
    }
  }

  renderAnswer = (index, option, answer, qid, oId) => {
    let { answerList } = this.state;
    if (answerList.filter(item => item.ID == index).length > 0) {
      answerList = answerList.map(item => {
        if (item.ID == index) {
          item.answer = option;
        }
        return item;
      });
    } else {
      answerList.push({ ID: index, answer: option, questionId: qid });
    }
    this.setState({ answerList });
  };

  submitAnswer = () => {
    console.log("MATCHING", this.state.answerList.length, this.state.quizData[0].questions.length)
    if (this.state.answerList.length === this.state.quizData[0].questions.length) {
      let answerList = this.state.answerList.map(item =>
        item.ID ? { question_id: item.questionId, answer: item.answer } : null
      );
      console.log("ANSWERER List.....", answerList, this.props.accessToken);
      this.setState({
        spinner: !this.state.spinner
      });
      axios(
        `${BASE_URL}quiz_ans/${this.props.navigation.state.params.id}`,
        {
          headers: {
            'Accept-Language': i18n.language.includes('es') ? 'es' : null
          },
          method: "POST",
          data: {
            access_token: this.props.accessToken,
            quiz_answers: answerList
          }
        }
      )
        .then(res => {
          this.setState({ spinner: !this.state.spinner });
          console.log("RESSPONES", res);
          if(res.data.status_code == 200){
            this.setState({message:res.data.message, result:true})
          }
          this.setState({ spinner: !this.state.spinner, errorModal:!this.state.errorModal});

        })
        .catch(res => {
          console.log("ERER", res.response.data)
          if(res.response.data.status_code == 400){
            this.setState({message:res.response.data.message, result:false})
          }
          this.setState({ spinner: !this.state.spinner, errorModal:!this.state.errorModal});
        });
    } else {
      Alert.alert(i18n.t("learnToEarn:attemptAll"))
    }
  };

  render() {
    const { answerList } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <Modal
          onModalHide={() => console.log("jide error")}
          isVisible={this.state.spinner}
          style={styles.errorModalStyle}
        >
          <Spinner
            style={styles.spinner}
            size={50}
            type={"Circle"}
            color={"white"}
          />
        </Modal>
        <Modal
          onModalHide={() => console.log("jide error")}
          isVisible={this.state.errorModal}
          style={styles.errorModalStyle}
        >
          <View style={[styles.errorCardStyle]}>
            <Image
              source={this.state.result === true ? Images.success :Images.Alert}
              resizeMode="contain"
              style={{
                height: 46,
                width: 46,
                alignSelf: "center"
              }}
            />
            <Text
              style={{
                height: 44,
                paddingHorizontal:10,
                textAlign: 'center',
                fontFamily: "Comfortaa-Bold",
                color: "rgb(51,51,51)",
              }}
            >
              {this.state.message}
            </Text>
            <TouchableOpacity
              style={[styles.errorButtonModal, { height: 47, width: 255 }]}
              onPress={() =>{ 
                this.props.navigation.navigate('Result',
                  {
                    id:this.props.navigation.state.params.id
                  }
                )
                this.setState({errorModal:!this.state.errorModal})}}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: 15,
                  fontFamily: "Comfortaa-Bold",
                  marginBottom: Platform.OS == "android" ? "2%" : 0
                }}
              >
                {i18n.t("learnToEarn:viewResult")}
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
        <ImageBackground
          source={require("../../../../assets/loginLayer.jpg")}
          style={{ flex: 1 }}
        >
          <View style={{ flex: 1, alignItems: "center" }}>
            <View style={styles.cardStyle}>
              {this.state.quizData ? (
                this.state.quizData.length == 0 ? (
                  <View
                    style={{
                      flex: 1,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: "Comfortaa-Bold",
                        textAlign: "center"
                      }}
                    >
                      {i18n.t("learnToEarn:noQuiz")}
                    </Text>
                  </View>
                ) : (
                  <ScrollView
                    style={{ flex: 1 }}
                    scrollEnabled
                    showsVerticalScrollIndicator={false}
                  >
                    <View style={{ flex: 1 }}>
                      <View style={{ flex: 1 }}>
                        <FlatList
                          data={this.state.quizData[0].questions}
                          extraData={this.state}
                          renderItem={({ item, index }) => {
                            index = index + 1;
                            let test = answerList.filter(
                              item => item.ID == index
                            );
                            return (
                              <View style={{ flex: 1, margin: "2%" }}>
                                <Text style={styles.questionStyle}>
                                  Q.{index} {item.question}
                                </Text>
                                <View
                                  style={{
                                    flexDirection: "row",
                                    justifyContent: "space-between"
                                  }}
                                >
                                  <View style={styles.answerContainer}>
                                    <RadioButton
                                      animation={"bounceIn"}
                                      isSelected={
                                        test.length > 0 &&
                                        test[0].answer == item.op1
                                      }
                                      onPress={() =>
                                        this.renderAnswer(
                                          index,
                                          item.op1,
                                          item.answer,
                                          item.question_id,
                                          item.op1Id
                                        )
                                      }
                                      innerColor="rgb(41,34,108)"
                                      outerColor="rgb(41,34,108)"
                                      size={10}
                                    />
                                    <View style={{ margin: "2%" }} />
                                    <Text style={styles.answerStyle}>
                                      {item.op1}
                                    </Text>
                                  </View>
                                  <View style={styles.answerContainer}>
                                    <RadioButton
                                      animation={"bounceIn"}
                                      isSelected={
                                        test.length > 0 &&
                                        test[0].answer == item.op2
                                      }
                                      onPress={() =>
                                        this.renderAnswer(
                                          index,
                                          item.op2,
                                          item.answer,
                                          item.question_id,
                                          item.op2Id
                                        )
                                      }
                                      innerColor="rgb(41,34,108)"
                                      outerColor="rgb(41,34,108)"
                                      size={10}
                                    />
                                    <View style={{ margin: "2%" }} />
                                    <Text style={styles.answerStyle}>
                                      {item.op2}
                                    </Text>
                                  </View>
                                </View>
                                <View
                                  style={{
                                    flexDirection: "row",
                                    justifyContent: "space-between"
                                  }}
                                >
                                  <View style={styles.answerContainer}>
                                    <RadioButton
                                      animation={"bounceIn"}
                                      isSelected={
                                        test.length > 0 &&
                                        test[0].answer == item.op3
                                      }
                                      onPress={() =>
                                        this.renderAnswer(
                                          index,
                                          item.op3,
                                          item.answer,
                                          item.question_id,
                                          item.op3Id
                                        )
                                      }
                                      innerColor="rgb(41,34,108)"
                                      outerColor="rgb(41,34,108)"
                                      size={10}
                                    />
                                    <View style={{ margin: "2%" }} />
                                    <Text style={styles.answerStyle}>
                                      {item.op3}
                                    </Text>
                                  </View>
                                  <View style={styles.answerContainer}>
                                    <RadioButton
                                      animation={"bounceIn"}
                                      isSelected={
                                        test.length > 0 &&
                                        test[0].answer == item.op4
                                      }
                                      onPress={() =>
                                        this.renderAnswer(
                                          index,
                                          item.op4,
                                          item.answer,
                                          item.question_id,
                                          item.op4Id
                                        )
                                      }
                                      innerColor="rgb(41,34,108)"
                                      outerColor="rgb(41,34,108)"
                                      size={10}
                                    />
                                    <View style={{ margin: "2%" }} />
                                    <Text style={styles.answerStyle}>
                                      {item.op4}
                                    </Text>
                                  </View>
                                </View>
                                <View
                                  style={{
                                    borderWidth: 0.6,
                                    opacity: 0.2,
                                    marginTop: "4%"
                                  }}
                                />
                              </View>
                            );
                          }}
                          keyExtractor={(item, index) => item.question_id}
                          showsVerticalScrollIndicator={false}
                        />
                      </View>
                      <View style={{ margin: "1.2%" }} />
                      <TouchableOpacity
                        style={styles.button}
                        onPress={() => this.submitAnswer()}
                      >
                        <Text
                          style={{
                            color: "white",
                            textAlign: "center",
                            fontSize: 17,
                            fontFamily: "Comfortaa-Bold"
                          }}
                        >
                          {i18n.t("common:submit")}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </ScrollView>
                )
              ) : (
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <ActivityIndicator size="large" />
                </View>
              )}
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    height: "97%",
    width: "90%",
    padding: "2%",
    paddingBottom: 0,
    paddingTop: 10,
    elevation: 5,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2
  },
  modalStyle: {
    justifyContent: "center",
    alignItems: "center"
  },
  buttonModal: {
    width: "40%",
    borderRadius: 8,
    justifyContent: "center",
    backgroundColor: "rgb(41,34,108)"
  },
  errorCardStyle: {
    // flex: 1,
    height: 221,
    justifyContent: "space-around",
    width: 295,
    backgroundColor: "white",
    borderRadius: 10
  },
  errorButtonModal: {
    // flex: 0.3,
    // height: "10%",
    width: "100%",
    borderRadius: 8,
    alignSelf: "center",
    justifyContent: "space-around",
    backgroundColor: "rgb(41,34,108)"
  },
  errorModalStyle: {
    justifyContent: "center",
    alignItems: "center"
  },

  questionStyle: {
    color: "black",
    fontFamily: "Comfortaa-Bold"
  },
  answerContainer: {
    flexDirection: "row",
    margin: "1%",
    marginRight: "auto",
    alignItems: "center"
  },
  answerStyle: {
    fontFamily: "Comfortaa-Bold",
    color: "rgb(102,102,102)",
    width: 120
  },
  button: {
    height: 47,
    width: 295,
    borderRadius: 8,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    backgroundColor: "rgb(41,34,108)",
    marginBottom: 10
  }
});

const mapStateToProps = ({ auth }) => {
  const { accessToken } = auth;
  return { accessToken };
};

export default connect(
  mapStateToProps,
  null
)(Quiz);
