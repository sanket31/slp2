import React from "react";
import {
  Text,
  View,
  ImageBackground,
  StyleSheet,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Platform,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { connect } from "react-redux";
import { Images } from "../../../../assets/images";
import { ScrollView } from "react-native-gesture-handler";
import i18n from "../../../../i18n/";
import { NavigationActions } from "react-navigation";
import { NavigationService } from "../../../../navigator";
import { changeRouteTechSupport , saveQuestionOne} from "../../../../Actions";
import { QRdata, BASE_URL } from "../../../../config/api";
class ProductActivation2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
    };
  }
  handleRecommendation = () => {
    this.setState(
      {
        loader: true,
      },
      () => {
        console.log("data...", this.props.barrelData);
        const { product_id, qr_id } = this.props.barrelData;
        let access_token = this.props.accessToken;
        const barrelDataDummy = {
          access_token,
          product_id,
          save_and_continue: "true",
          qr_id,
          // a_side_batch: "",
          // start_drum_temp: "",
          // a_side_set_temp: "",
          // b_side_set_temp: "",
          // hose_set_temp: "",
          // pressure_set: "",
          // mixing_chamber_size: "",
        };
        const data = new FormData();
        Object.keys(barrelDataDummy).forEach((key) => {
          data.append(key, barrelDataDummy[key]);
        });
        console.log("finaldata..", data);
        fetch(`${BASE_URL}${QRdata.activateProduct}`, {
          method: "POST",
          body: data,
        })
          .then((res) => res.json())
          .then((res) => {
            console.log('sprayparamter data...', res.data.product_name);
            if (res.status_code == 200) {
              this.setState(
                {
                  loader: false,
                },
                () => {
                  this.props.changeRouteTechSupport("SprayParameters");
                  NavigationService.navigate("TechScreen");
                  this.props.saveQuestionOne(res.data.product_name)
                  
                }
              );
            }
         else{  
           this.setState({
             loader: false
           })
         }
          })
          .catch((error) => {
            console.log(error);
            this.setState({
              loader: false,
            });
          });
      }
    );
  };
  render() {
    return (
      <ImageBackground
        source={require("../../../../assets/loginLayer.jpg")}
        style={{ flex: 1 }}
      >
        <View style={{ flex: 1, alignItems: "center" }}>
          <View style={styles.cardStyle}>
            <ScrollView
              style={{ flex: 1 }}
              showsVerticalScrollIndicator={false}
            >
              <Image
                source={
                  this.props.barrelData
                    ? { uri: this.props.barrelData.product_image }
                    : Images.productImage
                }
                style={styles.productImageStyle}
              />
              <View style={{ marginLeft: "3%", marginTop: 10 }}>
                <Text style={styles.productTitleStyle}>
                  {i18n.t("sprayPoints:productName")}
                </Text>
                {this.props.barrelData ? (
                  <Text style={{ fontFamily: "Comfortaa-Bold" }}>
                    {this.props.barrelData.product_name}
                  </Text>
                ) : (
                  <ActivityIndicator size={"small"} color={"rgb(41,34,108)"} />
                )}
                <View
                  style={{
                    borderBottomWidth: 0.6,
                    opacity: 0.1,
                    marginVertical: 5,
                  }}
                />
                <Text style={styles.productTitleStyle}>
                  {i18n.t("sprayPoints:manufacturerName")}
                </Text>
                {this.props.barrelData ? (
                  <Text style={{ fontFamily: "Comfortaa-Bold" }}>
                    {this.props.barrelData.merchant_name}
                  </Text>
                ) : (
                  <ActivityIndicator size={"small"} color={"rgb(41,34,108)"} />
                )}
                <View
                  style={{
                    borderBottomWidth: 0.6,
                    opacity: 0.1,
                    marginVertical: 5,
                  }}
                />
                <Text style={styles.productTitleStyle}>
                  {i18n.t("sprayPoints:pointsToBeEarned")}
                </Text>
                {this.props.barrelData ? (
                  <Text style={{ fontFamily: "Comfortaa-Bold" }}>
                    {this.props.barrelData.product_total_points}
                  </Text>
                ) : (
                  <ActivityIndicator size={"small"} color={"rgb(41,34,108)"} />
                )}
                <View
                  style={{
                    borderBottomWidth: 0.6,
                    opacity: 0.1,
                    marginVertical: 5,
                  }}
                />
              </View>
              <TouchableOpacity
                style={styles.button}
                onPress={() =>
                  this.props.navigation.navigate("ProductActivation")
                }
              >
                <Text
                  style={{
                    color: "white",
                    textAlign: "center",
                    fontSize: 17,
                    fontFamily: "Comfortaa-Bold",
                    paddingBottom: Platform.OS == "ios" ? 0 : 5,
                  }}
                >
                  {/* {i18n.t("sprayPoints:completeActivationProcess")}
                   */}
                   Activate Points
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.button}
                onPress={() => {
                  // this.props.navigation.navigate("Support");
                  this.props.changeRouteTechSupport("Library");
                  NavigationService.navigate("TechScreen");
                }}
              >
                <Text
                  style={{
                    color: "white",
                    textAlign: "center",
                    fontSize: 17,
                    fontFamily: "Comfortaa-Bold",
                    paddingBottom: Platform.OS == "ios" ? 0 : 5,
                  }}
                >
                 Explore This Product
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.button}
                onPress={() => this.handleRecommendation()}
              >
                {this.state.loader ? (
                  <ActivityIndicator size="large" color="#fff" />
                ) : (
                  <Text
                    style={{
                      color: "white",
                      textAlign: "center",
                      fontSize: 17,
                      fontFamily: "Comfortaa-Bold",
                      paddingBottom: Platform.OS == "ios" ? 0 : 5,
                    }}
                  >
                    Get Your Recomendation
                  </Text>
                )}
              </TouchableOpacity>
            </ScrollView>
          </View>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    height: "95%",
    width: "90%",
    padding: "2%",
    paddingBottom: 0,
    paddingTop: 10,
    elevation: 5,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
  },
  productImageStyle: {
    height: hp("45%"),
    width: wp("82%"),
    borderRadius: 10,
    alignSelf: "center",
    marginTop: 5,
  },
  productTitleStyle: {
    fontFamily: "Comfortaa-Bold",
    fontSize: 16,
    marginVertical: 5,
  },
  button: {
    height: 47,
    width: "90%",
    borderRadius: 8,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    paddingHorizontal: 5,
    backgroundColor: "rgb(41,34,108)",
    marginVertical: Platform.OS == "ios" ? 5 : 10,
  },
});

const mapStateToProps = ({ barrel, auth }) => {
  const { barrelData } = barrel;
  const { userData, accessToken, userId } = auth;
  return { barrelData, accessToken };
};

export default connect(mapStateToProps, {
  changeRouteTechSupport,
  saveQuestionOne
})(ProductActivation2);
