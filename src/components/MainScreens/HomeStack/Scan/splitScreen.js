import React from "react";
import {
  Text,
  View,
  Image,
  ImageBackground,
  StyleSheet,
  FlatList,
  TextInput,
  TouchableOpacity,
  Alert,
  ScrollView,
  Platform,
} from "react-native";
import Modal from "@kalwani/react-native-modal";
import { Icon } from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Autocomplete from "react-native-autocomplete-input";
import { connect } from "react-redux";
import { MainStyles } from "../../../styles";
import {
  userNameChanged,
  submitSplitAction,
  hideModal,
  splitUserSuccess,
  setDashboardData,
  splitUserFail,
} from "../../../../Actions";
import { clearSelectHistoyList } from "../../../../Actions/qrhistoryActions";
import { Images } from "../../../../assets/images";
import { create } from "apisauce";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Spinner from "react-native-spinkit";
import { NavigationService } from "../../../../navigator";
import i18n from "../../../../i18n";
import { BASE_URL, QRdata } from "../../../../config/api";
import axios from "axios";

const api = create({
  baseURL: BASE_URL,
});

class SplitScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      isNoUserError: false,
      userNames: "",
      totalPercentage: 100,
      userId: null,
      isVisibleUserName: false,
      percentage: "",
      errorPercentage: false,
      errorUser: false,
      newData: [],
      data: [],
      errorMessage: "",
      render: false,
      splitConfirmModal: false,
      isSplitSuccessModal: false,
      isSplitUserFailModal: false,
      zIndex: 0,
      loaderState: false,
      uploadProgress: null,
      quizmodal: false,
    };
  }

  addUserFunc = () => {
    const checkUser = (user) => {
      return user.full_name === this.state.userNames;
    };
    // this.counterPercentage();
    if (
      parseInt(this.state.percentage, 10) <=
        parseInt(this.state.totalPercentage, 10) &&
      parseFloat(this.state.percentage, 10) <= 100 &&
      parseInt(this.state.percentage, 10) > 0
    ) {
      this.setState({ errorPercentage: false });
      // console.log(this.props.splitUserList.some(checkUser));
      if (
        this.state.userNames !== "" &&
        this.props.splitUserList.some(checkUser)
      ) {
        const checkIfAdded = (userData) => {
          if (userData.length > 0) {
            const tempUserData = userData.filter(
              (user) => user.name === this.state.userNames
            );
            if (tempUserData.length > 0) {
              return false;
            }
            return true;
          }
          return true;
        };
        if (checkIfAdded(this.state.data)) {
          const percentConvert =
            this.state.percentage !== ""
              ? parseInt(this.state.percentage, 10)
              : 0;
          const addedData = this.addData(
            this.state.userNames,
            parseInt(this.state.percentage),
            this.state.userId
          );
          if (addedData) {
            this.setState({
              totalPercentage: this.state.totalPercentage - percentConvert,
              percentage: "",
              errorPercentage: false,
              errorUser: false,
              userNames: "",
              // itemList: []
            });
          }
        } else {
          Alert.alert(
            i18n.t("splitPoints:alert"),
            `${i18n.t("splitPoints:exist")} ${this.state.userNames} ${i18n.t(
              "splitPoints:exist1"
            )}`
          );
        }
      } else {
        this.setState({ errorUser: true });
      }
    } else {
      this.setState({ errorPercentage: true });
    }
    // console.log('user', this.state.totalPercentage);
  };

  submitSplitAction = (
    usersData,
    barrelData,
    access_token,
    splitActivationData
  ) => {
    this.setState({ loaderState: true });
    api
      .post(QRdata.activateProduct, splitActivationData, {
        onUploadProgress: (e) => {
          console.log(e);
          const progress = (e.loaded / e.total) * 100;
          console.log(progress);
          this.setState({ uploadProgress: Math.round(progress) });
        },
      })
      .then(async (resSplitAct) => {
        console.log("response split activation", resSplitAct.data.data);
        let percentData = [];
        usersData.forEach((item) => {
          percentData.push({
            user: item.id,
            percentage: parseInt(item.percentage, 10),
          });
        });
        const data = {
          access_token: access_token,
          product_id: resSplitAct.data.data.product_id,
          user_rewards_id: resSplitAct.data.data.user_rewards_id,
          users: percentData,
        };
        await axios
          .post(BASE_URL + QRdata.split, data, {
            headers: {
              "Accept-Language": i18n.language.includes("es") ? "es" : null,
            },
          })
          .then((responseSplit) => {
            console.log("response Split", responseSplit.data);
            // getUserRewards(access_token);
            const rewardsDataArray = [];
            console.log("response User Rewards", responseSplit.data);
            responseSplit.data.data.rewards_history.forEach((item) => {
              if (item.qr_status === "Completed") {
                rewardsDataArray.push(item);
              }
            });

            this.props.splitUserSuccess(rewardsDataArray);
            this.setState({ loaderState: false });
            delete responseSplit.data.data.rewards_history;
            this.props.setDashboardData(responseSplit.data.data);
          })
          .catch((err) => {
            console.log("error Split Action", err.response.data);
            this.props.setUserFail(err.response.data);
          });
      })
      .catch((err) => {
        console.log("split error2", err);
      });
  };

  splitSubmit = () => {
    this.setState({
      splitConfirmModal: !this.state.splitConfirmModal,
    });

    this.submitSplitAction(
      this.state.data,
      this.props.barrelData,
      this.props.accessToken,
      this.props.navigation.state.params.splitActivationData
    );
  };

  spinner = (loaderState) => (
    <View>
      <Modal
        onModalHide={() => {
          if (this.props.splitUserSuccess) {
            this.setState({
              isSplitSuccessModal: !this.state.isSplitSuccessModal,
            });
          } else {
            this.setState({
              isSplitUserFailModal: !this.state.isSplitUserFailModal,
            });
          }
        }}
        hideModalContentWhileAnimating
        animationIn={"flash"}
        animationOut={"flash"}
        isVisible={this.state.loaderState}
        style={styles.modalStyle}
      >
        <Spinner
          isVisible={this.state.loaderState}
          size={50}
          type={"Circle"}
          color={"white"}
        />
        {this.state.uploadProgress ? (
          <Text
            style={{
              fontSize: 20,
              color: "white",
              marginTop: Platform.OS == "ios" ? 10 : 0,
              marginLeft: Platform.OS == "ios" ? 15 : 0,
            }}
          >
            {this.state.uploadProgress} %
          </Text>
        ) : null}
      </Modal>
    </View>
  );

  searchFilterFunction = (text) => {
    this.setState({
      zIndex: 1,
    });
    const newUserData = this.props.splitUserList.filter((item) => {
      const itemData = `${item.full_name.toUpperCase()}`;
      const textData = text.toUpperCase();

      return itemData.includes(textData);
    });
    if (text !== "") {
      if (newUserData.length > 0) {
        this.setState({
          newData: newUserData,
          userNames: text,
        });
      } else {
        this.setState({
          newData: newUserData,
          userNames: text,
          zIndex: 0,
        });
      }
    } else {
      this.setState({
        newData: [],
        userNames: "",
        zIndex: 0,
      });
    }
  };

  addData = (name, percentage, id) => {
    this.state.data.push({
      name,
      percentage,
      id,
    });
    this.setState({ render: !this.state.render });
    return true;
  };

  deleteUser = (item, index) => {
    const deletedPercent =
      item.percentage !== "" ? parseInt(item.percentage, 10) : 0;
    this.state.data.splice(index, 1);
    this.setState({
      render: !this.state.render,
      totalPercentage: this.state.totalPercentage + deletedPercent,
    });
  };
  showQuizModal = () => {
    if (this.state.quizmodal) {
      return (
        <Modal
          onModalHide={() => {}}
          animationIn={"bounceIn"}
          animationOut={"bounceOut"}
          animationInTiming={200}
          animationOutTiming={200}
          isVisible={this.state.quizmodal}
          isVisible={true}
          style={styles.modalStyle}
        >
          <View style={styles.cardStyleNoUserError}>
            <Image
              source={require("../../../../assets/Icons/success.png")}
              resizeMode="contain"
              style={{
                height: "25%",
                alignSelf: "center",
              }}
            />
            <Text
              style={{
                textAlign: "center",
                fontFamily: "Comfortaa-Bold",
                color: "rgb(51,51,51)",
                margin: "5%",
                lineHeight: 16,
              }}
            >
              Play quiz to earn more point
            </Text>
            <TouchableOpacity
              style={[styles.buttonModal, { height: 47, marginTop: 10 }]}
              onPress={() => {
                this.setState(
                  {
                    quizmodal: false,
                  },
                  () => {
                    // this.props.navigation.navigate("Quiz", {
                    //   id: this.props.barrelData.video_id_if_quiz_available
                    // });
                    this.props.navigation.navigate("VideoScreen");
                    this.props.clearSelectHistoyList();
                  }
                );
              }}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: 15,
                  fontFamily: "Comfortaa-Bold",
                  paddingBottom: Platform.OS == "ios" ? 0 : 5,
                }}
              >
                Go To Quiz
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.buttonModal, { height: 47 }]}
              onPress={() => {
                this.setState({
                  quizmodal: false,
                });
                setTimeout(() => {
                  if (
                    this.props.navigation.state.params.screendata !==
                    "ScanScreen"
                  ) {
                    this.props.navigation.popToTop();
                    NavigationService.navigate("RewardPoints");
                  } else {
                    NavigationService.navigate("HomeScreen");

                    NavigationService.navigate("RewardPoints");
                  }
                }, 200);
              }}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: 15,
                  fontFamily: "Comfortaa-Bold",
                  paddingBottom: Platform.OS == "ios" ? 0 : 5,
                }}
              >
                Cancel
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
      );
    }
  };
  renderSplitResponse = () => {
    if (this.props.splitUserSuccess) {
      return (
        <Modal
          onModalHide={() => {}}
          animationIn={"bounceIn"}
          animationOut={"bounceOut"}
          animationInTiming={200}
          animationOutTiming={200}
          isVisible={this.state.isSplitSuccessModal}
          style={styles.modalStyle}
        >
          <View style={styles.cardStyleNoUserError}>
            <Image
              source={require("../../../../assets/Icons/success.png")}
              resizeMode="contain"
              style={{
                height: "25%",
                alignSelf: "center",
              }}
            />
            <Text
              style={{
                textAlign: "center",
                fontFamily: "Comfortaa-Bold",
                color: "rgb(51,51,51)",
                margin: "5%",
                lineHeight: 16,
              }}
            >
              {i18n.t("splitPoints:successMessage")}
            </Text>
            <TouchableOpacity
              style={[styles.buttonModal, { height: 47 }]}
              onPress={() => {
                if (
                  this.props.qrSelectedHistory.hasOwnProperty(
                    "is_quiz_available"
                  )
                ) {
                  console.log('form history...')
                  if (this.props.qrSelectedHistory.is_quiz_available) {
                    this.setState({
                      quizmodal: true,
                      isSplitSuccessModal: !this.state.isSplitSuccessModal,
                    });
                  } else {
                    this.setState({
                      isSplitSuccessModal: !this.state.isSplitSuccessModal,
                    });
                    setTimeout(() => {
                      if (
                        this.props.navigation.state.params.screendata !==
                        "ScanScreen"
                      ) {
                        this.props.navigation.popToTop();
                        NavigationService.navigate("RewardPoints");
                      } else {
                        NavigationService.navigate("HomeScreen");

                        NavigationService.navigate("RewardPoints");
                      }
                      this.props.clearSelectHistoyList();
                    }, 200);
                  }
                }
                else if (this.props.barrelData.hasOwnProperty("is_quiz_available")) {
                  console.log('form scan...')
                  if (this.props.barrelData.is_quiz_available) {
                    this.setState({
                      quizmodal: true,
                      isSplitSuccessModal: !this.state.isSplitSuccessModal,
                    });
                  } else {
                    this.setState({
                      isSplitSuccessModal: !this.state.isSplitSuccessModal,
                    });
                    setTimeout(() => {
                      if (
                        this.props.navigation.state.params.screendata !==
                        "ScanScreen"
                      ) {
                        this.props.navigation.popToTop();
                        NavigationService.navigate("RewardPoints");
                      } else {
                        NavigationService.navigate("HomeScreen");

                        NavigationService.navigate("RewardPoints");
                      }
                    }, 200);
                  }
                }
              }}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: 15,
                  fontFamily: "Comfortaa-Bold",
                  paddingBottom: Platform.OS == "ios" ? 0 : 5,
                }}
              >
                OK
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
      );
    }
    return (
      <Modal
        onModalHide={() => {}}
        isVisible={this.state.isSplitUserFailModal}
        style={styles.modalStyle}
      >
        <View style={styles.cardStyleNoUserError}>
          <Image
            source={require("../../../../assets/Icons/Alert.png")}
            resizeMode="contain"
            style={{
              height: "25%",
              alignSelf: "center",
            }}
          />
          <Text
            style={{
              textAlign: "center",
              fontFamily: "Comfortaa-Bold",
              color: "rgb(51,51,51)",
              margin: "5%",
            }}
          >
            {this.props.splitUsersError ? this.props.splitUsersError : ""}
          </Text>
          <TouchableOpacity
            style={[styles.buttonModal, { height: 47 }]}
            onPress={() => {
              this.setState({
                isSplitUserFailModal: !this.state.isSplitUserFailModal,
              });
            }}
          >
            <Text
              style={{
                color: "white",
                textAlign: "center",
                fontSize: 15,
                fontFamily: "Comfortaa-Bold",
                paddingBottom: Platform.OS == "ios" ? 0 : 5,
              }}
            >
              OK
            </Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  };

  renderSplitConfrm = () => {
    console.log("render split confirm called........");
    return (
      <Modal
        // hideModalContentWhileAnimating
        onModalHide={() => {
          this.spinner(this.props.splitScreenLoader);
        }}
        animationIn={"bounceIn"}
        animationOut={"bounceOut"}
        animationInTiming={200}
        animationOutTiming={200}
        isVisible={this.state.splitConfirmModal}
        style={styles.modalStyle}
      >
        <View
          style={[
            styles.cardStyle,
            {
              height: 228,
              width: 295,
              justifyContent: "center",
              alignItems: "center",
            },
          ]}
        >
          {/* <Image
          source={Images.success}
          resizeMode="contain"
          style={{
            height: 50,
            alignSelf: "center",
          }}
        /> */}
          <Text
            style={{
              textAlign: "center",
              fontFamily: "Comfortaa-Bold",
              color: "rgb(51,51,51)",
              margin: "5%",
              lineHeight: 24,
            }}
          >
            {i18n.t("splitPoints:noOfUsers")}
            {this.state.data.length} {i18n.t("splitPoints:users")}
          </Text>
          <TouchableOpacity
            style={[
              styles.buttonModal,
              { height: 47, alignSelf: "center", alignItems: "center" },
            ]}
            onPress={() => {
              this.splitSubmit();
              // this.props.navigation.navigate("SplitScreen");
            }}
          >
            <Text
              style={{
                color: "white",
                textAlign: "center",
                fontSize: 16,
                fontFamily: "Comfortaa-Bold",
                marginBottom: Platform.OS == "android" ? "2%" : 0,
              }}
            >
              {i18n.t("splitPoints:yes")}
            </Text>
          </TouchableOpacity>
          <View style={{ margin: "2%" }} />
          <TouchableOpacity
            style={[
              styles.buttonModal,
              {
                height: 47,
                backgroundColor: "white",
                borderWidth: 1,
                borderColor: "rgb(41,34,108)",
                alignSelf: "center",
              },
            ]}
            onPress={() => {
              // this.props.hideModal();
              this.setState({
                splitConfirmModal: !this.state.splitConfirmModal,
              });
              // this.toggleModal("DontSplit");
            }}
          >
            <Text
              style={{
                color: "rgb(41,34,108)",
                textAlign: "center",
                fontSize: 16,
                fontFamily: "Comfortaa-Bold",
                marginBottom: Platform.OS == "android" ? "2%" : 0,
              }}
            >
              {i18n.t("splitPoints:no")}
            </Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  };

  render() {
    return (
      <ImageBackground
        source={require("../../../../assets/loginLayer.jpg")}
        style={MainStyles.container}
      >
        <View style={MainStyles.cardStyle}>
          {this.state.data.length > 0 && this.renderSplitConfrm()}
          {this.renderSplitResponse()}
          {this.showQuizModal()}
          {this.spinner(this.props.splitScreenLoader)}
          <Text
            style={{
              fontFamily: "Comfortaa-Bold",
              fontSize: 17,
              color: "rgb(52,52,52)",
              textAlign: "center",
            }}
          >
            Allfoamtech
          </Text>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              margin: hp("1%"),
            }}
          >
            <Text style={[MainStyles.titleStyle, { marginLeft: hp("1.8%") }]}>
              {i18n.t("splitPoints:splitPoints")}
            </Text>
          </View>
          <View style={[MainStyles.userCard]}>
            <View
              style={[
                styles.autocompleteContainer,
                { zIndex: this.state.zIndex },
              ]}
            >
              <Autocomplete
                // autoCapitalize="none"
                keyExtractor={(item) => item.id.toString()}
                autoCorrect={false}
                scrollEnabled
                inputContainerStyle={{ borderColor: "transparent" }}
                // containerStyle={styles.autocompleteContainer}
                listContainerStyle={{
                  marginTop: "1.5%",
                  maxHeight: "80%",
                  minHeight: "40%",
                  width: wp("74%"),
                  alignSelf: "center",
                  backgroundColor: "transparent",
                  zIndex: 1,
                }}
                listStyle={{
                  maxHeight:
                    Platform.OS == "android"
                      ? "80%"
                      : this.state.newData
                      ? this.state.newData.length == 2
                        ? "60%"
                        : this.state.newData.length == 1
                        ? "90%"
                        : "40%"
                      : null,
                  minHeight: Platform.OS == "android" ? "40%" : 0,
                  borderWidth: 0.6,
                  width: wp("74%"),
                  alignSelf: "center",
                  borderColor: "rgba(200,200,200,1)", // borderColor: "rgb(41,34,108)",
                  padding: 5,
                  borderBottomLeftRadius: 5,
                  borderBottomRightRadius: 5,
                  opacity: 1,
                  backgroundColor: "white",
                }}
                data={this.state.newData}
                renderItem={({ item, index }) => {
                  console.log(item);
                  return (
                    <TouchableOpacity
                      onPress={() => {
                        setTimeout(() => {
                          this.setState({
                            errorUser: false,
                            userNames: item.full_name,
                            userId: item.id,
                            newData: [],
                            zIndex: 0,
                          });
                        }, 500);
                      }}
                      style={{ width: "100%" }}
                    >
                      {console.log("daatatat,", item)}
                      <Text
                        style={{
                          fontSize: 15,
                          flexWrap: "wrap",
                          // maxHeight: 35,
                          fontWeight: "normal",
                          color: "black",
                        }}
                      >
                        {item.full_name}
                      </Text>
                      <Text
                        style={{
                          fontSize: 12,
                          flexWrap: "wrap",
                          // maxHeight: 35,
                          // marginLeft: "auto",
                          fontWeight: "normal",
                          color: "black",
                        }}
                      >
                        {item.email}
                      </Text>
                      <View
                        style={{
                          borderWidth: 0.6,
                          marginVertical: 5,
                          borderColor: "rgba(200,200,200,1)",
                        }}
                      />
                    </TouchableOpacity>
                  );
                }}
                renderTextInput={() => (
                  <View
                    style={{
                      height: 45,
                      paddingLeft: 5,
                      flexDirection: "row",
                      width: wp("74%"),
                      //   fontFamily: "Comfortaa-Bold",
                      borderWidth: 0.6,
                      //   fontWeight: "normal",
                      marginLeft: "auto",
                      marginRight: "auto",
                      alignItems: "center",
                      borderColor: "rgba(200,200,200,1)",
                      borderRadius: 5,
                      marginTop: 12,
                    }}
                  >
                    <TextInput
                      placeholder={i18n.t("splitPoints:selectUser")}
                      // editable={false}
                      value={this.state.userNames}
                      onChangeText={(text) => this.searchFilterFunction(text)}
                      style={{
                        color: "black",
                        width: "100%",
                        height: "100%",
                      }}
                    />
                    <Icon
                      name={"user"}
                      type={"antdesign"}
                      size={20}
                      containerStyle={{
                        marginLeft: "auto",
                        marginRight: "2%",
                      }}
                    />
                  </View>
                )}
              />
            </View>
            <View
              style={{
                height: 15,
                transform: [{ translateY: 50 }, { translateX: 15 }],
              }}
            >
              {this.state.errorUser ? (
                <Text style={{ color: "red" }}>
                  {this.state.userNames === ""
                    ? i18n.t("splitPoints:userNameBlank")
                    : i18n.t("splitPoints:userNameNot")}
                </Text>
              ) : null}
            </View>
            <View
              style={{
                height: 45,
                paddingLeft: 5,
                flexDirection: "row",
                width: wp("74%"),
                borderWidth: 0.6,
                marginLeft: "auto",
                marginRight: "auto",
                alignItems: "center",
                borderColor: "rgba(200,200,200,1)",
                borderRadius: 5,
                marginTop: 55,
                justifyContent: "center",
              }}
            >
              <TextInput
                placeholder={i18n.t("splitPoints:percent")}
                keyboardType={"number-pad"}
                // style={MainStyles.textInputStyle}
                style={{ color: "black", width: "100%" }}
                value={this.state.percentage}
                onChangeText={(amt) => this.setState({ percentage: amt })}
                // onEndEditing={amt => this.setState({ percentage: amt })}
              />
              <Icon
                name={"sack-percent"}
                type={"material-community"}
                size={20}
                containerStyle={{ marginLeft: "auto", marginRight: "2%" }}
              />
            </View>
            <View style={{ height: 20, transform: [{ translateX: 15 }] }}>
              {this.state.errorPercentage ? (
                <Text style={{ color: "red" }}>
                  {this.state.percentage === ""
                    ? i18n.t("splitPoints:percentageBlank")
                    : i18n.t("splitPoints:percentage") +
                      this.state.totalPercentage}
                </Text>
              ) : null}
            </View>
            <TouchableOpacity
              onPress={() => this.addUserFunc()}
              style={{
                height: 45,
                width: wp("75%"),
                backgroundColor: "rgb(41,34,108)",
                alignSelf: "center",
                borderRadius: 10,
                justifyContent: "center",
                paddingBottom: Platform.OS == "ios" ? 0 : 5,
              }}
            >
              {/* <Image
                  source={Images.addUser}
                  style={MainStyles.addUserIconStyle}
                  resizeMode="contain"
                /> */}
              <Text style={MainStyles.buttonText}>
                {i18n.t("splitPoints:addUser")}
              </Text>
            </TouchableOpacity>
          </View>
          <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
            <FlatList
              data={this.state.data}
              extraData={this.state.render}
              renderItem={({ item, index }) => (
                <View
                  style={{
                    height: hp("8%"),
                    width: wp("80%"),
                    alignSelf: "center",
                    elevation: 5,
                    backgroundColor: "white",
                    borderRadius: 10,
                    shadowColor: "#000",
                    shadowOffset: { width: 0, height: 1 },
                    shadowOpacity: 0.5,
                    shadowRadius: 2,
                    marginBottom: hp("1%"),
                    marginTop: hp("0.5%"),
                    borderWidth: 0.6,
                    borderColor: "rgba(41,34,108,0.1)",
                    justifyContent: "center",
                  }}
                >
                  <View
                    style={{
                      flexDirection: "column",
                      marginLeft: 10,
                    }}
                  >
                    {/* <TextInput
                          editable={false}
                          value={item.name}
                          style={{ color: "black" }}
                          // style={MainStyles.textInputStyle}
                        /> */}
                    <View
                      style={{ flexDirection: "row", alignItems: "center" }}
                    >
                      <Icon name={"user"} type={"antdesign"} size={15} />
                      <Text
                        style={{
                          marginLeft: "4%",
                          fontSize: 17,
                          fontWeight: "400",
                        }}
                      >
                        {item.name}
                      </Text>
                      <TouchableOpacity
                        style={{
                          alignItems: "center",
                          marginLeft: "auto",
                          height: 25,
                          width: 30,
                          marginRight: 15,
                          transform: [{ translateY: hp("2%") }],
                        }}
                        // hitSlop={{top:200, left:200, right:200, bottom:200}}
                        onPress={() => {
                          Alert.alert(
                            "Delete User?",
                            "Are you sure you want to remove this user?",
                            [
                              {
                                text: "Cancel",
                                onPress: () => console.log("Cancel Pressed"),
                                style: "cancel",
                              },
                              {
                                text: "OK",
                                onPress: () => this.deleteUser(item, index),
                              },
                            ],
                            { cancelable: false }
                          );
                        }}
                      >
                        <Icon name={"delete"} type={"antdesign"} size={20} />
                      </TouchableOpacity>
                    </View>
                    <View
                      style={{ flexDirection: "row", alignItems: "center" }}
                    >
                      <Icon
                        name={"sack-percent"}
                        type={"material-community"}
                        size={15}
                      />
                      <Text
                        style={{
                          marginLeft: "4%",
                          fontSize: 17,
                          fontWeight: "400",
                        }}
                      >
                        {item.percentage}%
                      </Text>
                    </View>
                    {/* <Icon
                          name={"user"}
                          type={"antdesign"}
                          containerStyle={{
                            marginLeft: "auto",
                            marginRight: "2%",
                          }}
                        /> */}
                  </View>
                  {/* <TextInput
                        // style={MainStyles.textInputStyle}
                        style={{ color: "black" }}
                        value={item.percentage}
                        editable={false}
                      /> */}
                  {/* <Icon
                        name={"sack-percent"}
                        type={"material-community"}
                        containerStyle={{
                          marginLeft: "auto",
                          marginRight: "2%",
                        }}
                      /> */}
                </View>
              )}
              keyExtractor={(item, index) => index.toString()}
              showsVerticalScrollIndicator={false}
            />
            <View style={{ justifyContent: "flex-end", paddingBottom: 15 }}>
              <Text
                style={{
                  textAlign: "center",
                  fontSize: 15,
                  marginBottom: 10,
                  marginTop: 10,
                }}
              >
                {i18n.t("splitPoints:percentRemaining")}:{" "}
                {this.state.totalPercentage}
              </Text>
              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    splitConfirmModal: !this.state.splitConfirmModal,
                  })
                }
                style={[
                  MainStyles.button,
                  {
                    backgroundColor:
                      this.state.data.length > 0
                        ? "rgb(41,34,108)"
                        : "rgba(41,34,108,0.4)",
                  },
                ]}
                activeOpacity={this.state.data.length > 0 ? 1 : 0}
              >
                <Text
                  style={[
                    MainStyles.buttonText,
                    { marginBottom: Platform.OS == "android" ? "2%" : 0 },
                  ]}
                >
                  {i18n.t("productActivation:split")}
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
        {console.log("state of Data", this.state.data)}
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  autocompleteContainer: {
    // flex: 1,
    left: 0,
    top: 0,
    bottom: 0,
    // alignSelf:'center',
    // height: '100%',
    position: "absolute",
    right: 0,
    //  bottom: 200,
  },
  modalStyle: {
    justifyContent: "center",
    alignItems: "center",
  },
  cardStyle: {
    width: "90%",
    padding: "4%",
    paddingBottom: 0,
    paddingTop: 10,
    elevation: 5,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
  },
  cardStylePercentage: {
    width: "90%",
    padding: "4%",
    // paddingBottom: 0,
    // paddingTop: 10,
    // elevation: 5,
    backgroundColor: "rgb(41,34,108)",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
  },
  containerPicker: {
    flex: 1,
    justifyContent: "center",
    borderRadius: 10,
    alignItems: "center",
    // backgroundColor: 'rgb(41,34,108)',
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    borderBottomColor: "#FFFFFF",
    flex: 1,
  },
  searchIcon: {
    height: "40%",
    width: "6%",
    transform: [{ translateY: -10 }, { translateX: -10 }],
    alignSelf: "flex-end",
  },
  titleHeader: {
    color: "black",
    fontSize: 25,
    alignSelf: "flex-start",
    fontWeight: "bold",
  },
  titleSubHeader: {
    color: "#000000",
    fontSize: 15,
    alignSelf: "flex-start",
    fontWeight: "bold",
  },
  border: {
    borderWidth: 1.5,
    marginRight: hp("2%"),
    marginTop: hp("2%"),
    borderColor: "rgb(0, 203, 142)",
  },
  itemTitle: {
    fontSize: 15,
    flexWrap: "wrap",
    paddingLeft: 15,
    // maxHeight: 35,
    paddingTop: wp("1%"),
    fontWeight: "bold",
    color: "black",
  },
  formContent: {
    flexDirection: "row",
  },
  inputContainer: {
    backgroundColor: "#FFFFFF",
    borderRadius: 30,
    height: 45,
    borderWidth: 1,
    borderColor: "black",
    flexDirection: "row",
    alignItems: "center",
    flex: 1,
    margin: 10,
  },
  icon: {
    width: 30,
    height: 30,
  },
  iconBtnSearch: {
    alignSelf: "center",
  },
  inputIcon: {
    marginLeft: 15,
    justifyContent: "center",
  },
  notificationBox: {
    padding: 20,
    marginTop: 5,
    marginBottom: 5,
    backgroundColor: "#FFFFFF",
    flexDirection: "row",
    borderRadius: 10,
  },
  image: {
    width: 45,
    height: 45,
  },
  description: {
    fontSize: 18,
    color: "#3498db",
    marginLeft: 10,
  },
  buttonContainer: {
    height: 45,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    width: 100,
    borderRadius: 30,
  },
  sendButton: {
    backgroundColor: "rgb(255, 133, 51)",
  },
  buttonText: {
    color: "white",
  },
  cardStyleNoUserError: {
    height: 228,
    width: 295,
    justifyContent: "center",
    padding: "5%",
    paddingBottom: 0,
    elevation: 6,
    backgroundColor: "white",
    borderRadius: 10,
  },
  buttonModal: {
    height: "10%",
    width: "100%",
    borderRadius: 8,
    justifyContent: "center",
    backgroundColor: "rgb(41,34,108)",
  },
});

const mapStateToProps = ({ barrel, auth, qrhistory }) => {
  const {
    splitScreenLoader,
    splitUserList,
    barrelData,
    isSplitSuccessModal,
    splitUsersError,
    splitUserSuccess,
    uploadProgress,
  } = barrel;
  const { userData, accessToken, userId } = auth;
  const { qrSelectedHistory } = qrhistory;
  return {
    accessToken,
    userId,
    splitScreenLoader,
    splitUserList,
    userData,
    barrelData,
    isSplitSuccessModal,
    splitUsersError,
    splitUserSuccess,
    uploadProgress,
    qrSelectedHistory,
  };
};

const actionCreater = {
  userNameChanged,
  submitSplitAction,
  hideModal,
  setDashboardData,
  splitUserSuccess,
  splitUserFail,
  clearSelectHistoyList
};

export default connect(mapStateToProps, actionCreater)(SplitScreen);
