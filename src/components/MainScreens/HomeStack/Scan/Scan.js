import React from "react";
import {
  Text,
  View,
  ImageBackground,
  Image,
  StyleSheet,
  TouchableOpacity,
  Alert,
  Dimensions,
  SafeAreaView,
  PermissionsAndroid,
  Platform,
} from "react-native";
import { connect } from "react-redux";
import { RNCamera } from "react-native-camera";
import Permissions from "react-native-permissions";
import Geolocation from "react-native-geolocation-service";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import {  
  fetchQRdata,
  showModal,
  hideModal,
  barrelFail,
  changeRouteTechSupport,
} from "../../../../Actions";
import { NavigationEvents } from "react-navigation";
import BarcodeMask from "react-native-barcode-mask";
import { Images } from "../../../../assets/images";
import Modal from "@kalwani/react-native-modal";
import Spinner from "react-native-spinkit";
import { NavigationService } from "../../../../navigator";
import i18n from "../../../../i18n";

let done = false;

class Scan extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: (
      <View
        style={{
          flexDirection: "row",
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <TouchableOpacity
          style={{ flex: 0.1 }}
          onPress={() => {
            navigation.navigate("HomeScreen");
          }}
        >
          <Image
            source={require("../../../../assets/settingsIcon/back.png")}
            resizeMode="contain"
            style={{
              height: hp("3%"),
              marginLeft: hp("1%"),
              alignSelf: "center",
              marginTop: "7%",
            }}
          />
        </TouchableOpacity>
        {/* <View style={{flex:0.8}} > */}
        <Text
          style={{
            flex: 0.8,
            color: "white",
            fontSize: 20,
            alignSelf: "center",
            textAlign: "center",
            marginLeft: "auto",
            marginRight: "auto",
            fontFamily: "Comfortaa-Bold",
          }}
        >
          {i18n.t("common:scan")}
        </Text>
        {/* </View> */}
        <View style={{ flex: 0.1 }} />
      </View>
    ),

    headerStyle: {
      backgroundColor: "rgb(41,34,108)",
      borderBottomWidth: 0,
      height: hp("7%"),
      elevation: 0,
    },
  });
  constructor(props) {
    super(props);
    this.state = {
      isFocused: false,
      isModalVisible: false,
      name: "",
      product: "",
      points: "",
      isAuthorizedCam: false,
      isAuthorizedLoc: false,
      location: "",
      isFailScanModal: false,
      showRecommendationModal: false,
    };
  }
  componentDidMount() {
    // setTimeout(() => {
    //   done = true
    // }, 2000);
  }
  componentWillUnmount() {
    this.setState(
      {
        showRecommendationModal: false,
      },
      () => {
        done = false;
      }
    );
  }
  onSuccess = async (QRdata) => {
    console.log("QR-response", QRdata);
    let dataJSON = null;
    try {
      dataJSON = JSON.parse(QRdata);
    } catch (error) {
      done = false;
      console.log("fdfdfdfdf", error);
    }
    const checkProp = (dataObjectQR) => {
      if (Object.keys(dataObjectQR).includes("UID")) {
        if (
          dataObjectQR.UID ===
          "42ef9ba1c50cfc760c70a6f52ba6c8dd2acb127d6e1ba648d6d0eaff2d4135ac"
        ) {
          return true;
        }
        return false;
      }
      return false;
    };
    if (dataJSON !== null) {
      if (checkProp(dataJSON)) {
        // console.log('Sa', QRdata, typeof e);
        const { navigation } = this.props;
        Geolocation.getCurrentPosition(
          (loc) => {
            this.props.fetchQRdata(
              dataJSON,
              loc.coords,
              this.props.accessToken,
              navigation
            );
          },
          (error) => {
            console.log(error.code, error.message);
            Alert.alert(
              i18n.t("Scan:locationNeeded"),
              i18n.t("Scan:locationNeeded2"),
              [
                {
                  text: "OK",
                  onPress: () => {
                    done = false;
                  },
                },
              ],
              { cancelable: false }
            );
          },
          { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
      } else {
        this.setState({ isFailScanModal: !this.state.isFailScanModal });
        console.log("ERR");
      }
    }
  };

  onDidFocus = (payload) => {
    done = false;
    this._requestCameraPermission();
    this.setState({ isFocused: true });
  };

  onDidBlur = (payload) => {
    this.setState({ isFocused: false, isAuthorizedLoc: false });
  };

  openSettings() {
    Permissions.openSettings().then(() => {
      this._requestLocationPermission();
    });
  }

  _requestLocationPermissionIOS() {
    Permissions.request("location").then((response) => {
      if (response === "authorized") {
        this.setState({ isAuthorizedLoc: true });
      } else {
        Alert.alert(
          i18n.t("Scan:location"),
          i18n.t("Scan:enableLocation"),
          [{ text: "OK", onPress: () => this.openSettings() }],
          { cancelable: false }
        );
      }
    });
  }

  _requestCameraPermission = () => {
    Permissions.request("camera")
      .then((response) => {
        console.log("permission", response);
        if (response === "authorized") {
          this.setState({ isAuthorizedCam: true });
          this._requestLocationPermission();
        } else {
          NavigationService.navigate("Home");
        }
        // Returns once the user has chosen to 'allow' or to 'not allow' access
        // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
        // this.setState({photoPermission: response});
      })
      .catch((err) => {
        console.log(err);
      });
  };

  _requestLocationPermission = async () => {
    const checkLoc = await Permissions.check("location");
    if (Platform.OS === "ios") {
      if (checkLoc === "authorized") {
        this.setState({ isAuthorizedLoc: true });
      } else if (checkLoc === "undetermined") {
        this._requestLocationPermissionIOS();
      } else {
        Alert.alert(
          i18n.t("Scan:location"),
          i18n.t("Scan:enableLocation"),
          [{ text: "OK", onPress: () => this.openSettings() }],
          { cancelable: false }
        );
      }
    } else {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
        // {
        //   title: 'Access Location',
        //   message: 'Sprayer loyalty App needs access to your location ',
        // }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the location");
        this.setState({ isAuthorizedLoc: true });
      } else {
        console.log("location permission denied");
        // Alert.alert('Location permission denied');
        Alert.alert(
          i18n.t("Scan:location"),
          i18n.t("Scan:enableLocation"),
          [{ text: "OK", onPress: () => NavigationService.navigate("Home") }],
          { cancelable: false }
        );
      }
    }
  };

  toggleModal = () => {
    // this.props.hideModal();
    done = false;
  };
  showRecommendationModal = () => {
    return (
      <Modal
        onModalHide={() => {}}
        // hideModalContentWhileAnimating
        animationIn={"bounceIn"}
        animationOut={"bounceOut"}
        animationInTiming={200}
        animationOutTiming={200}
        isVisible={this.state.showRecommendationModal}
        style={styles.modalStyle}
      >
        <View
          style={[
            styles.cardStyle,
            {
              height: 150,
              justifyContent: "space-between",
              padding: 15,
            },
          ]}
        >
          <Text
            style={{
              fontFamily: "Comfortaa-Bold",
              fontSize: 16,
              color: "rgb(51,51,51)",
              alignSelf: "center",
              textAlign: "center",
            }}
          >
            Do you know about Recommendation
          </Text>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-around",
            }}
          >
            <TouchableOpacity
              style={[styles.buttonModal, { height: 47, width: "30%" }]}
              onPress={() => {
                done = false;
                this.setState({
                  showRecommendationModal: false,
                });
              }}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: 16,
                  fontFamily: "Comfortaa-Bold",
                  marginBottom: Platform.OS == "android" ? "2%" : 0,
                }}
              >
                Yes
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.buttonModal, { height: 47, width: "30%" }]}
              onPress={() => {
                // this.props.changeRouteTechSupport('SprayParameters');
                done = false;
                this.setState(
                  {
                    showRecommendationModal: false,
                  },
                  () => {
                    // this.props.navigation.popToTop();
                    this.props.navigation.navigate("TechScreen");
                  }
                );
              }}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: 16,
                  fontFamily: "Comfortaa-Bold",
                  marginBottom: Platform.OS == "android" ? "2%" : 0,
                }}
              >
                No
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };
  spinner = () => (
    <View>
      <Modal
        onModalHide={() => {
          if (this.props.QrScanFail) {
            this.setState({ isFailScanModal: !this.state.isFailScanModal });
          }
        }}
        hideModalContentWhileAnimating
        animationIn={"flash"}
        animationOut={"flash"}
        isVisible={this.props.QrScanLoader}
        style={styles.errorModalStyle}
      >
        <Spinner
          style={styles.spinner}
          isVisible={this.props.QrScanLoader}
          size={50}
          type={"Circle"}
          color={"white"}
        />
      </Modal>
    </View>
  );

  render() {
    const { isFocused, isAuthorizedCam, isAuthorizedLoc } = this.state;

    return (
      <View style={{ flex: 1 }}>
        {this.showRecommendationModal()}
        <Modal
          onModalHide={() => null}
          isVisible={this.state.isFailScanModal}
          style={styles.errorModalStyle}
        >
          <TouchableOpacity
            onPress={() => {
              done = false;
              this.setState({ isFailScanModal: !this.state.isFailScanModal });
            }}
            style={{
              alignItems: "flex-end",
              marginBottom: -12,
              zIndex: 1,
              width: 295,
              marginLeft: 18,
            }}
          >
            <Image
              source={Images.close}
              style={{ height: 27, width: 27 }}
              resizeMode="contain"
            />
          </TouchableOpacity>

          <View style={[styles.errorCardStyle]}>
            <Image
              source={require("../../../../assets/Icons/Alert.png")}
              resizeMode="contain"
              style={{
                height: 46,
                width: 46,
                alignSelf: "center",
              }}
            />
            <Text
              style={{
                height: "auto",
                width: 250,
                textAlign: "center",
                alignSelf: "center",
                fontFamily: "Comfortaa-Bold",
                color: "rgb(51,51,51)",
              }}
            >
              {this.props.QrError ? this.props.QrError.message : ""}
            </Text>
            <TouchableOpacity
              style={[styles.errorButtonModal, { height: 47, width: 255 }]}
              onPress={() => {
                // this.toggleModal();
                this.setState({ isFailScanModal: !this.state.isFailScanModal });
                done = false;
                this.props.navigation.navigate("Dispute");
              }}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: 15,
                  fontFamily: "Comfortaa-Bold",
                  marginBottom: Platform.OS == "android" ? "2%" : 0,
                }}
              >
                {this.props.QrError
                  ? i18n.t("Scan:raiseDispute")
                  : i18n.t("Scan:retry")}
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
        {this.spinner()}
        <ImageBackground
          source={require("../../../../assets/loginLayer.jpg")}
          style={{ flex: 1 }}
        >
          <View
            style={{
              flex: 1,
              margin: "3%",
              overflow: "hidden",
              borderRadius: 15,
            }}
          >
            <NavigationEvents
              onDidFocus={this.onDidFocus}
              onDidBlur={this.onDidBlur}
            />
            {isAuthorizedCam && isFocused && isAuthorizedLoc && (
              <RNCamera
                ref={(ref) => {
                  this.camera = ref;
                }}
                captureAudio={false}
                style={{
                  height: "97%",
                  width: "90%",
                  marginBottom: "4%",
                  borderRadius: 15,
                  elevation: 5,
                  shadowColor: "#000",
                  shadowOffset: { width: 0, height: 1 },
                  shadowOpacity: 0.5,
                  shadowRadius: 2,
                  alignSelf: "center",
                  overflow: "hidden",
                }}
                type={RNCamera.Constants.Type.back}
                onBarCodeRead={(event) => {
                  if (!done) {
                    done = true;
                    console.log("qrrCode", event.data);
                    this.onSuccess(event.data);
                    // this.props.navigation.navigate('SprayPoints');
                  } else {
                    console.log(event);
                  }
                  return () => {};  
                }}
              >
                <BarcodeMask
                  edgeColor={"lime"}
                  showAnimatedLine
                  height={"95%"}
                  width={"95%"}
                />
              </RNCamera>
            )}
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    height: "95%",
    width: "90%",
    elevation: 6,
    backgroundColor: "white",
    borderRadius: 10,
  },
  modalStyle: {
    justifyContent: "center",
    alignItems: "center",
  },
  buttonModal: {
    width: "40%",
    borderRadius: 8,
    justifyContent: "center",
    backgroundColor: "rgb(41,34,108)",
  },
  errorCardStyle: {
    // flex: 1,
    height: 221,
    justifyContent: "space-around",
    width: 295,
    backgroundColor: "white",
    borderRadius: 10,
  },
  errorButtonModal: {
    // flex: 0.3,
    // height: "10%",
    width: "100%",
    borderRadius: 8,
    alignSelf: "center",
    justifyContent: "space-around",
    backgroundColor: "rgb(41,34,108)",
  },
  errorModalStyle: {
    justifyContent: "center",
    alignItems: "center",
  },
});

const mapStateToProperties = ({ barrel, auth }) => {
  const { userData, accessToken, userId } = auth;
  const {
    barrelData,
    isModalVisible,
    QrScanFail,
    QrScanLoader,
    QrError,
  } = barrel;
  return {
    accessToken,
    userId,
    barrelData,
    isModalVisible,
    QrScanFail,
    QrScanLoader,
    userData,
    QrError,
  };
};

export default connect(mapStateToProperties, {
  fetchQRdata,
  barrelFail,
  showModal,
  hideModal,
  changeRouteTechSupport,
})(Scan);
