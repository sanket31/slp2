import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  StyleSheet,
  Platform,
  ActivityIndicator
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import { connect } from "react-redux";
import { Images } from "../../../../assets/images";
import i18n from "../../../../i18n";

class Dispute extends React.Component {
  render() {
    return (
      <ImageBackground
        style={{ flex: 1, alignItems: "center" }}
        source={require("../../../../assets/loginLayer.jpg")}
      >
        <View style={styles.cardStyle}>
          <View style={styles.userCard}>
            {this.props.alreadyScannedData ? (
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between"
                }}
              >
                <Image
                  source={{ uri: this.props.alreadyScannedData.user_image }}
                  style={styles.imageStyle}
                />
                <View
                  style={{
                    flexDirection: "column",
                    marginRight: "auto",
                    marginLeft: hp("1%")
                  }}
                >
                  <Text style={styles.nameStyle}>
                    {this.props.alreadyScannedData.scanned_by}
                  </Text>
                  <Text style={styles.emailStyle}>
                    {this.props.alreadyScannedData.email}
                  </Text>
                  <View style={{ flexDirection: "row" }}>
                    <Image
                      source={Images.location}
                      style={{
                        height: 12,
                        width: 12,
                        marginRight: 2,
                        marginTop: 4
                      }}
                      resizeMode="contain"
                    />
                    <Text style={styles.emailStyle}>
                      {this.props.alreadyScannedData.scanned_at}
                    </Text>
                  </View>
                </View>
              </View>
            ) : (
              <ActivityIndicator
                size={"large"}
                color={"rgb(41,34,108)"}
                style={{ marginTop: "auto", marginBottom: "auto" }}
              />
            )}
            <View
              style={{ borderWidth: 0.6, opacity: 0.1, margin: hp("1.5%") }}
            />
            <TouchableOpacity
              style={styles.button}
              onPress={() => this.props.navigation.navigate("DisputeRequest")}
            >
              <Text
                style={{
                  fontFamily: "Comfortaa-Bold",
                  fontSize: 16,
                  color: "rgb(255,255,255)",
                  textAlign: "center",
                  paddingBottom: Platform.OS == "ios" ? 0 : 5
                }}
              >
                {i18n.t("Scan:disputeRequest")}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    height: "95%",
    width: "90%",
    padding: "2%",
    paddingBottom: 0,
    paddingTop: 10,
    elevation: 5,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2
  },
  userCard: {
    flex: 1,
    width: "92%",
    alignSelf: "center",
    marginVertical: 15
  },
  nameStyle: {
    fontSize: 16,
    color: "rgb(51,51,51)",
    fontFamily: "Comfortaa-Bold",
    marginVertical: Platform.OS == "ios" ? 3 : 0
  },
  imageStyle: {
    height: 70,
    width: 70,
    marginRight: 5,
    borderRadius: Platform.OS == "ios" ? 35 : 100
  },
  emailStyle: {
    fontFamily: "Comfortaa-Bold",
    fontSize: 13,
    width: "80%",
    textAlign: "justify",
    marginVertical: Platform.OS == "ios" ? 3 : 0
  },
  pointTextStyle: {
    fontFamily: "Comfortaa-Bold",
    fontSize: 13
  },
  button: {
    height: 47,
    width: 295,
    borderRadius: 8,
    justifyContent: "center",
    backgroundColor: "rgb(41,34,108)",
    alignSelf: "center"
  }
});

const mapStateToProps = ({ barrel }) => {
  const { alreadyScannedData } = barrel;
  return { alreadyScannedData };
};

export default connect(mapStateToProps)(Dispute);
