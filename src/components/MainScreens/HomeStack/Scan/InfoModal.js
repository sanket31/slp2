import React, { Component } from 'react';
import { Text } from 'react-native'
export default function InfoModal(props) {
    let msg=""
    if (props.infobtntext === 'A_Side_Batch') {
        msg="The A-Side Batch Number can be found on the label on the side A-Drum (red) next to the date of manufacture."
   }
    else if (props.infobtntext === "Starting_Drum_Temperature") {
        msg="The starting drum temperature can be determined by using an infrared laser thermometer and taking the drum temperature along the bottom third of the drum. Generally the drum temperature must be at least 70⁰F before starting to spray."
    }
    else if (props.infobtntext === "A_Side_Set_Temperature") {
        msg= "The A-Side Temperature setting is set on the proportioner for the A-Side primary heater. Check the product Technical Data Sheet for temperature set guidelines. The temperature settings may have to be adjusted to find optimal setting.  The A-Side temperature is typically 2⁰F to 5⁰F lower than the B-Side.When optimal setting is found, this is the value that should be entered.  Temperature may have to be adjusted throughout the spray day to accomodate for fluctuations in ambient conditions. "
    }
    else if (props.infobtntext === "B_Side_Set_Temperature") {
        msg="The B-Side Temperature setting is set on the proportioner for the B-Side primary heater. Check the product Technical Data Sheet for temperature set guidelines. The temperature settings may have to be adjusted to find optimal setting.  When optimal setting is found, this is the value that should be entered.  The B-Side temperature is typically 2⁰F to 5⁰F higher than the A-Side. Temperature may have to be adjusted throughout the spray day to accomodate for fluctuations in ambient conditions. "
    }
    else if (props.infobtntext === "Hose_Set_Temperature") {
        msg="The Hose Temperature setting is set on the proportioner. Check the product Technical Data Sheet for temperature set guidelines. The temperature settings may have to be adjusted to find optimal setting.  When optimal setting is found, this is the value that should be entered. The hose temperature should never be set higher than the A-Side primary heater, and typically is 2⁰F to 5⁰F lower than the A-Side Set Temperature.Temperature may have to be adjusted throughout the spray day to accomodate for fluctuations in ambient conditions. "
    }
    else if (props.infobtntext === "Pressure_Set_Temperature") {
        msg="The Pressure setting is set on the proportioner. Check the product Technical Data Sheet for pressure set guidelines. The pressure settings may have to be adjusted to find optimal setting.  When optimal setting is found, this is the value that should be entered. The pressure settings may have to be adjusted throughout the spray day to accomodate for application conditions.Higher pressure typically leads to more overspray and lower yields. Pressurs settings should be set as low as practical."
    }
    else if (props.infobtntext === "Mixing_Chamber_Size") {
        msg='Please select your mixing chamber that is a close as possible to one of the four most common mixing chamber sizes listed.  An "00" is a 2020, an "01" is a 4242, an "02" is a 5252, an "03" is a 6060.  In general, the larger the mixing chamber the lower the resulting yield.  Avoid larger mixing chambers in cold weather, and consult with proportioner manual to determine if mixing chamber size is appropriate for the equipment you are using. '
    }
    return <RenderText infomsg={msg} />

}
function RenderText(props) {
    return (
        <Text
            style={{
                // textAlign: "center",
                fontFamily: "Comfortaa-Bold",
                color: "rgb(51,51,51)",
                lineHeight: 24,
                marginVertical: 10
            }}
        >
            {props.infomsg}
        </Text>
    )
}