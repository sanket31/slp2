import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  StyleSheet,
  Platform,
  TextInput,
  Image,
  Alert,
  ActivityIndicator,
} from "react-native";
import { connect } from "react-redux";
import axios from "axios";
import Modal from "@kalwani/react-native-modal";
import { QRdata } from "../../../../config/api";
import { Images } from "../../../../assets/images";
import i18n from "../../../../i18n";
import { NavigationService } from "../../../../navigator";

class DisputeRequest extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      visible: false,
      textData: "",
      disputeSuccess: false,
      errorMessage: "",
    };
  }

  //   modal = () => (
  //   );
  submitDisputeRequest = async () => {
    if (this.state.textData !== "") {
      this.setState({ loading: true });
      const data = {
        access_token: this.props.accessToken,
        qr_id: this.props.alreadyScannedData.qr_id,
        message: this.state.textData,
      };
      await axios
        .post(QRdata.dispute, data, {
          headers: {
          'Accept-Language': i18n.language.includes('es') ? 'es' : null
          }
        })
        .then(resDispute => {
          console.log("resDispute", resDispute.data);
          this.setState({
            visible: !this.state.visible,
            disputeSuccess: true,
            loading: false,
          });
        })
        .catch(errDispute => {
          console.log("errDispute", errDispute.response.data);
          this.setState({
            visible: !this.state.visible,
            errorMessage: errDispute.response.data.message,
            loading: false,
          });
        });
    } else {
      Alert.alert(i18n.t("Scan:msgEmpty"));
    }
  };

  renderButton = () => {
    if (this.state.loading) {
      return (
        <View style={styles.button}>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    return (
      <TouchableOpacity
        style={styles.button}
        onPress={() => this.submitDisputeRequest()}
      >
        <Text
          style={{
            fontFamily: "Comfortaa-Bold",
            fontSize: 16,
            color: "rgb(255,255,255)",
            textAlign: "center",
            paddingBottom: Platform.OS == "ios" ? 0 : 5,
          }}
        >
          {i18n.t("common:submit")}
        </Text>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <ImageBackground
        style={{ flex: 1, alignItems: "center" }}
        source={require("../../../../assets/loginLayer.jpg")}
      >
        <Modal
          onModalHide={() => {}}
          isVisible={this.state.visible}
          style={{ flex: 1 }}
        >
          <View style={styles.modalCard}>
            <Image
              source={Images.success}
              resizeMode="contain"
              style={{
                height: 47,
                alignSelf: "center",
              }}
            />
            <View
              style={{
                width: 230,
                alignSelf: "center",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  fontFamily: "Comfortaa-Bold",
                  color: "rgb(51,51,51)",
                }}
              >
                {this.state.disputeSuccess
                  ? i18n.t("Scan:submitMsg")
                  : this.state.errorMessage}
              </Text>
            </View>
            <TouchableOpacity
              style={[styles.button, { width: 255, height: 47 }]}
              onPress={() => {
                this.setState({ visible: !this.state.visible });
                setTimeout(() => {
                  NavigationService.navigate("HomeScreen");
                }, 300);
              }}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: 16,
                  fontFamily: "Comfortaa-Bold",
                }}
              >
                OK
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
        <View style={styles.cardStyle}>
          <Text
            style={{
              fontFamily: "Comfortaa-Bold",
              color: "rgb(51,51,51)",
              fontSize: 15,
              marginVertical:Platform.OS == 'ios' ? 10 : 0
            }}
          >
            {i18n.t("Scan:message")}
          </Text>
          <TextInput
            placeholder="Type Here.."
            numberOfLines={8}
            multiline={true}
            style={{
              height:'25%',
              marginVertical: 10,
              textAlignVertical: "top",
              borderRadius: 8,
              borderWidth: 0.6,
              borderColor: "rgba(200,200,200,1)",
              padding:Platform.OS == 'ios' ? 10 : 5
            }}
            onChangeText={text => this.setState({ textData: text })}
          />
          {this.renderButton()}
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    height: "95%",
    width: "90%",
    padding: 15,
    paddingBottom: 0,
    paddingTop: 10,
    elevation: 5,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
  },
  button: {
    height: 47,
    width: 295,
    borderRadius: 8,
    justifyContent: "center",
    backgroundColor: "rgb(41,34,108)",
    alignSelf: "center",
    marginVertical: 10,
  },
  modalCard: {
    height: 228,
    width: 295,
    backgroundColor: "rgb(255,255,255)",
    alignSelf: "center",
    justifyContent: "space-evenly",
    borderRadius: 8,
  },
});

const mapStateToProps = ({ auth, barrel }) => {
  const { alreadyScannedData } = barrel;
  const { accessToken } = auth;
  return { alreadyScannedData, accessToken };
};

export default connect(mapStateToProps)(DisputeRequest);
