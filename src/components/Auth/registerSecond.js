import React, { Component } from "react";
import {
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  Platform,
  Alert
} from "react-native";
import { connect } from "react-redux";
import { Icon } from "react-native-elements";
import NetInfo from "@react-native-community/netinfo";
import NoConnection from "../../utility/NoConnectionAuth";
import Modal from "@kalwani/react-native-modal";
import Spinner from "react-native-spinkit";
import { TextInput } from "react-native-paper";
import CheckBox from "react-native-check-box";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import {
  registerUser,
  addr1Changed,
  addr2Changed,
  cityChanged,
  stateChanged,
  zipChanged,
  referralChanged,
  toggleCheckBox,
  unCheckBox,
  showAuthModal,
  hideAuthModal,
  emailChanged,
  companyNameChanged,
  numberChanged,
  passwordChanged,
  nameChanged
} from "../../Actions";
import i18n from "../../i18n";
import { NavigationService } from "../../navigator";
import {SafeAreaView} from 'react-navigation';
class RegisterSecond extends Component {
  constructor(props) {
    super(props);
    this.state = {
      add1Err: false,
      cityErr: false,
      stateErr: false,
      zipErr: false,
      connected: true,
      isRegisteredSuccessModal: false,
      isRegisteredFailModal: false
    };
  }

  componentWillMount() {
    this.props.unCheckBox();
  }

  componentDidMount() {
    this.unsubscribe = NetInfo.addEventListener(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);
      if (!state.isConnected) {
        setTimeout(() => {
          this.setState({ connected: false });
        }, 500);
      } else {
        setTimeout(() => {
          this.setState({ connected: true });
        }, 500);
      }
    });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  onAddr1Change(text) {
    this.props.addr1Changed(text);
  }

  onAddr2Change(text) {
    this.props.addr2Changed(text);
  }

  onCityChange(text) {
    this.props.cityChanged(text);
  }

  onStateChange(text) {
    this.props.stateChanged(text);
  }

  onZipChange(text) {
    this.props.zipChanged(text);
  }

  onReferralChange(text) {
    this.props.referralChanged(text);
  }

  spinner = () => (
    <View>
      <Modal
        onModalHide={() => {
          if (this.props.registerSuccess) {
            this.setState({
              isRegisteredSuccessModal: !this.state.isRegisteredSuccessModal
            });
          } else {
            this.setState({
              isRegisteredFailModal: !this.state.isRegisteredFailModal
            });
          }
        }}
        hideModalContentWhileAnimating
        animationIn={"flash"}
        animationOut={"flash"}
        isVisible={this.props.registerLoading}
        style={styles.modalStyle}
      >
        <Spinner
          isVisible={this.props.loading}
          size={50}
          type={"Circle"}
          color={"white"}
        />
      </Modal>
    </View>
  );

  toggleModal = val => {
    // this.setState({ isModalVisible: !this.state.isModalVisible, name: '', points: '', product: '' });
    // this.props.hideAuthModal();

    if (val === "success") {
      this.setState({
        isRegisteredSuccessModal: !this.state.isRegisteredSuccessModal
      });
      setTimeout(() => {
        this.props.navigation.navigate("Login");
      }, 300);
    } else {
      this.setState({
        isRegisteredFailModal: !this.state.isRegisteredFailModal
      });
    }
  };

  submit = async () => {
    const {
      name,
      lastname,
      email,
      number,
      companyName,
      password,
      termsChecked,
      add1,
      add2,
      city,
      state,
      zip,
      referral
    } = this.props;
    if (add1.length > 1) {
      this.setState({ add1Err: false });
      if (city.length > 1) {
        this.setState({ cityErr: false });
        if (state.length > 1) {
          this.setState({ stateErr: false });
          if (zip.length > 1) {
            this.setState({ zipErr: false });
            if (termsChecked) {
              await this.props.registerUser(
                name,
                lastname,
                email,
                number,
                companyName,
                password,
                add1,
                add2,
                city,
                state,
                zip,
                referral
              );
            } else {
              Alert.alert(i18n.t("register:pleaseAgree"));
            }
          } else {
            this.setState({ zipErr: true });
          }
        } else {
          this.setState({ stateErr: true });
        }
      } else {
        this.setState({ cityErr: true });
      }
    } else {
      this.setState({ add1Err: true });
    }
  };

  renderButton() {
    // if (this.props.loading) {
    //   return (
    //     <View style={styles.button}>
    //       <ActivityIndicator size="large" />
    //     </View>
    //   );
    // }
    return (
      <TouchableOpacity
        onPress={this.submit}
        style={styles.button}
        activeOpacity={0.6}
      >
        <Text style={styles.loginText}>{i18n.t("register:register")}</Text>
      </TouchableOpacity>
    );
  }

  renderSuccess() {
    // if (this.props.success) {
    return (
      <View>
        <Modal
          onModalHide={() => {}}
          hideModalContentWhileAnimating
          useNativeDriver
          isVisible={this.state.isRegisteredSuccessModal}
          style={styles.modalStyle}
        >
          <View style={[styles.cardStyle]}>
            <Image
              source={require("../../assets/Icons/success.png")}
              resizeMode="contain"
              style={{
                height: "20%",
                alignSelf: "center"
              }}
            />
            <Text
              style={{
                textAlign: "center",
                textTransform: "capitalize",
                fontFamily: "Comfortaa-Bold",
                color: "rgb(51,51,51)",
                margin: "5%"
              }}
            >
              {i18n.t("register:registeredSuccess")}
            </Text>
            <TouchableOpacity
              style={[
                styles.buttonModal,
                { height: "25%", justifyContent: "center" }
              ]}
              activeOpacity={0.6}
              onPress={() => this.toggleModal("success")}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: 15,
                  fontFamily: "Comfortaa-Bold",
                  paddingBottom: Platform.OS == "ios" ? 0 : 8
                }}
              >
                {i18n.t("login:login")}
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
    // }
    // return null;
  }

  renderError() {
    // if (this.props.fail) {
    return (
      <View>
        <Modal
          onModalHide={() => {}}
          hideModalContentWhileAnimating
          useNativeDriver
          isVisible={this.state.isRegisteredFailModal}
          style={styles.modalStyle}
        >
          <View style={[styles.cardStyle]}>
            <Image
              source={require("../../assets/Icons/Alert.png")}
              resizeMode="contain"
              style={{
                height: "20%",
                alignSelf: "center"
              }}
            />
            <Text
              style={{
                textAlign: "center",
                textTransform: "capitalize",
                fontFamily: "Comfortaa-Bold",
                color: "rgb(51,51,51)",
                margin: "5%",
                fontSize: 18
              }}
            >
              {this.props.errorRegister ? this.props.errorRegister : ""}
            </Text>
            <TouchableOpacity
              style={[
                styles.buttonModal,
                { height: "25%", justifyContent: "center" }
              ]}
              onPress={() => this.toggleModal()}
              activeOpacity={0.6}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: 15,
                  fontFamily: "Comfortaa-Bold",
                  paddingBottom: Platform.OS == "ios" ? 0 : 8
                }}
              >
                {i18n.t("common:retry")}
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
    // }
    // return null;
  }

  render() {
    if (this.state.connected) {
      return (
        <SafeAreaView style={{flex:1, backgroundColor:'rgb(41,34,108)'}} forceInset={{bottom:'never'}}>

        <KeyboardAwareScrollView
          enableOnAndroid
          extraHeight={125}
          keyboardShouldPersistTaps={"handled"}
          style={{backgroundColor:'white'}}
        >
          <View style={{ flex: 1 }}>
            <ImageBackground
              style={{ flex: 1, height: hp("19.5%") }}
              source={require("../../assets/registerbg.png")}
              resizeMode="stretch"
            >
              <TouchableOpacity
                style={{
                  flex: 1,
                  height: 200,
                  marginTop: Platform.OS == "ios" ? 25 : 20
                }}
                activeOpacity={0.8}
              >
                <Icon
                  name="left"
                  type="antdesign"
                  iconStyle={{ color: "white" }}
                  onPress={() => this.props.navigation.goBack()}
                  underlayColor="rgba(255, 255, 255, 0)"
                  containerStyle={{
                    flex: 1,
                    width: 60,
                    alignSelf: "flex-start"
                  }}
                />
              </TouchableOpacity>
              <View
                style={{
                  flex: 3,
                  justifyContent: "flex-start",
                  transform: [{ translateY: -15 }]
                }}
              >
                <Image
                  source={require("../../assets/logo.png")}
                  style={styles.logoStyle}
                  resizeMode="contain"
                />
              </View>
            </ImageBackground>
            <View style={{ flex: 1 }}>
              <Text style={styles.titleStyle}>
                {i18n.t("register:register")}
              </Text>
              <View
                style={{
                  flex: 1,
                  margin: "5%",
                  marginTop: 0,
                  marginBottom: 0,
                  justifyContent: "center"
                }}
              >
                <TextInput
                  autoCapitalize="sentences"
                  placeholder={i18n.t("register:enterAddress")}
                  label={
                    this.state.add1Err
                      ? i18n.t("register:pleaseEnterAddress")
                      : i18n.t("register:addressLine1")
                  }
                  value={this.props.add1}
                  onChangeText={this.onAddr1Change.bind(this)}
                  style={styles.textInputStyle}
                  theme={{ colors: { primary: "#29226c" } }}
                  error={this.state.add1Err}
                />
                <TextInput
                  autoCapitalize="sentences"
                  placeholder={i18n.t("register:enterAddress")}
                  label={i18n.t("register:addressLine2")}
                  value={this.props.add2}
                  onChangeText={this.onAddr2Change.bind(this)}
                  style={styles.textInputStyle}
                  theme={{ colors: { primary: "#29226c" } }}
                />
                <TextInput
                  autoCapitalize="sentences"
                  placeholder={i18n.t("register:enterCity")}
                  label={
                    this.state.cityErr
                      ? i18n.t("register:validCity")
                      : i18n.t("register:city")
                  }
                  value={this.props.city}
                  onChangeText={this.onCityChange.bind(this)}
                  style={styles.textInputStyle}
                  theme={{ colors: { primary: "#29226c" } }}
                  error={this.state.cityErr}
                />
                <TextInput
                  autoCapitalize="sentences"
                  placeholder={i18n.t("register:enterState")}
                  label={
                    this.state.stateErr
                      ? i18n.t("register:validState")
                      : i18n.t("register:state")
                  }
                  value={this.props.state}
                  onChangeText={this.onStateChange.bind(this)}
                  style={styles.textInputStyle}
                  theme={{ colors: { primary: "#29226c" } }}
                  error={this.state.stateErr}
                />
                <TextInput
                  placeholder={i18n.t("register:enterZipcode")}
                  label={
                    this.state.zipErr
                      ? i18n.t("register:validZipcode")
                      : i18n.t("register:zipcode")
                  }
                  value={this.props.zip}
                  keyboardType="number-pad"
                  onChangeText={this.onZipChange.bind(this)}
                  style={styles.textInputStyle}
                  theme={{ colors: { primary: "#29226c" } }}
                  error={this.state.zipErr}
                />
                <TextInput
                  placeholder={i18n.t("register:enterReferral")}
                  label={i18n.t("register:referralCode")}
                  value={this.props.referral}
                  onChangeText={this.onReferralChange.bind(this)}
                  style={[styles.textInputStyle, { marginBottom: 0 }]}
                  theme={{ colors: { primary: "#29226c" } }}
                />
              </View>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  marginLeft: "5%",
                  marginVertical: 11
                }}
              >
                <CheckBox
                  checkBoxColor="rgb(41,34,108)"
                  onClick={() => {
                    this.props.toggleCheckBox();
                  }}
                  isChecked={this.props.termsChecked}
                />
                {/* <Checkbox
                  status={this.props.termsChecked ? "checked" : "unchecked"}
                  onPress={() => {
                    this.props.toggleCheckBox();
                  }}
                  theme={{ dark: true, colors: { accent: "rgb(41,34,108)"}}}
                /> */}
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "center",
                    marginLeft: "2%"
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Comfortaa-Bold",
                      color: "black",
                      fontSize: 13
                    }}
                  >
                    {i18n.t("register:agree")}{" "}
                  </Text>
                  <TouchableOpacity
                    onPress={() =>
                      NavigationService.navigate("TermsAndCondAuth")
                    }
                    activeOpacity={0.6}
                  >
                    <Text
                      style={{
                        width: "100%",
                        color: "rgb(41,34,108)",
                        textDecorationLine: "underline",
                        fontFamily: "Comfortaa-Bold",
                        fontSize: 13
                      }}
                      ellipsizeMode="tail"
                      numberOfLines={1}
                    >
                      {i18n.t("register:terms")}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              {this.renderButton()}
              {this.renderError()}
              {this.renderSuccess()}
              {this.spinner()}
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  marginTop: 5,
                  marginBottom: 10
                }}
              >
                <Text
                  style={{
                    fontFamily: "Comfortaa-Bold",
                    color: "black",
                    fontSize: 15
                  }}
                >
                  {i18n.t("register:already")} {""}
                </Text>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("Login")}
                  activeOpacity={0.6}
                >
                  <Text
                    style={{
                      color: "rgb(41,34,108)",
                      textDecorationLine: "underline",
                      fontFamily: "Comfortaa-Bold",
                      fontSize: 15
                    }}
                  >
                    {i18n.t("login:login")}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
        </SafeAreaView>
      );
    }
    return (
      <NoConnection
        onPress={() => {
          this.componentDidMount();
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  logoStyle: {
    height: "80%",
    width: "100%"
  },
  subTitle: {
    color: "white",
    margin: "2%",
    marginTop: 0,
    alignSelf: "center",
    fontFamily: "Comfortaa-Bold",
    fontSize: 15,
    fontWeight: "bold"
  },
  textInputStyle: {
    textAlign: "center",
    fontFamily: "Comfortaa-Bold",
    height: Dimensions.get("screen").height / 12,
    backgroundColor: "white"
  },
  titleStyle: {
    bottom: "2%",
    textAlign: "center",
    fontSize: 25,
    color: "black",
    fontFamily: "Comfortaa-Bold"
  },
  button: {
    height: 47,
    width: "90%",
    alignSelf: "center",
    borderRadius: 8,
    justifyContent: "center",
    backgroundColor: "rgb(41,34,108)"
  },
  loginText: {
    color: "white",
    fontFamily: "Comfortaa-Bold",
    fontSize: 20,
    alignSelf: "center",
    paddingBottom: Platform.OS == "ios" ? 0 : 8
  },
  cardStyle: {
    height: hp("30%"),
    width: "90%",
    padding: "5%",
    paddingBottom: 0,
    elevation: 6,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    borderRadius: 10
  },
  buttonModal: {
    height: "10%",
    width: "100%",
    borderRadius: 8,
    justifyContent: "center",
    backgroundColor: "rgb(41,34,108)"
  },
  modalStyle: {
    justifyContent: "center",
    alignItems: "center"
  }
});

const mapStateToProps = ({ auth }) => {
  const {
    registerLoading,
    errorRegister,
    added,
    registerSuccess,
    name,
    lastname,
    number,
    companyName,
    termsChecked,
    email,
    password,
    add1,
    add2,
    city,
    state,
    zip,
    fail,
    referral,
    isModalVisible
  } = auth;
  return {
    registerLoading,
    errorRegister,
    added,
    registerSuccess,
    name,
    lastname,
    number,
    companyName,
    termsChecked,
    email,
    password,
    add1,
    add2,
    city,
    fail,
    state,
    zip,
    referral,
    isModalVisible
  };
};

const actionCreators = {
  registerUser,
  addr1Changed,
  addr2Changed,
  cityChanged,
  stateChanged,
  zipChanged,
  referralChanged,
  toggleCheckBox,
  unCheckBox,
  showAuthModal,
  hideAuthModal,
  emailChanged,
  companyNameChanged,
  numberChanged,
  passwordChanged,
  nameChanged
};

export default connect(mapStateToProps, actionCreators)(RegisterSecond);
