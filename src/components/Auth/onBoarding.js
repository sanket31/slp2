import React from "react";
import {
  View,
  Text,
  ImageBackground,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  SafeAreaView
} from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import NetInfo from "@react-native-community/netinfo";
import NoConnection from "../../utility/NoConnectionAuth";
import { heightPercentageToDP } from "react-native-responsive-screen";
import { Images } from "../../assets/images";
import i18n from "../../i18n";
import { withTranslation } from "react-i18next";

const { width: viewportWidth, height: viewportHeight } = Dimensions.get(
  "window"
);

function wp(percentage) {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
}

const slideWidth = wp(95);
const itemHorizontalMargin = wp(2);
const itemWidth = slideWidth + itemHorizontalMargin * 2;

class onBoarding extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      connected: true,
      langLoaded: false,
      entries: [],
      activeSlide: 0
    };
  }

  componentDidMount() {
    this.unsubscribe = NetInfo.addEventListener(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);
      if (!state.isConnected) {
        setTimeout(() => {
          this.setState({ connected: false });
        }, 500);
      } else {
        this.setState({
          connected: true,
          entries: [
            {
              id: "1",
              image: Images.welcome1,
              title: i18n.t("onBoardingMessage:msgTitle1"),
              content: i18n.t("onBoardingMessage:msg1")
            },
            {
              id: "2",
              image: Images.welcome2,
              title: i18n.t("onBoardingMessage:msgTitle2"),
              content: i18n.t("onBoardingMessage:msg2")
            },
            {
              id: "3",
              image: Images.welcome3,
              title: i18n.t("onBoardingMessage:msgTitle3"),
              content: i18n.t("onBoardingMessage:msg3")
            },
            {
              id: "4",
              image: Images.welcome4,
              title: i18n.t("onBoardingMessage:msgTitle4"),
              content: i18n.t("onBoardingMessage:msg4")
            },
            {
              id: "5",
              image: Images.welcome5,
              title: i18n.t("onBoardingMessage:msgTitle5"),
              content: i18n.t("onBoardingMessage:msg5")
            },
            {
              id: "6",
              image: Images.welcome6,
              title: i18n.t("onBoardingMessage:msgTitle6"),
              content: i18n.t("onBoardingMessage:msg6")
            }
          ]
        });
      }
    });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  get pagination() {
    const { entries, activeSlide } = this.state;
    return (
      <Pagination
        dotsLength={entries.length}
        activeDotIndex={activeSlide}
        dotStyle={{
          width: 10,
          height: 10,
          borderRadius: 5,
          backgroundColor: "rgba(255, 255, 255, 0.92)"
        }}
        inactiveDotStyle={{}}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    );
  }

  renderItem = ({ item, index }) => (
    <SafeAreaView
      style={{
        height: heightPercentageToDP("80%"),
        justifyContent: "center",
        alignItems: "center"
      }}
    >
      <View
        style={{
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Image
          source={item.image}
          style={{
            height: 270,
            width: 270,
            marginTop: 30,
            borderRadius: 135,
            alignSelf: "center"
          }}
        />
      </View>
      <ScrollView>
        <Text
          style={{
            color: "white",
            fontSize: 27,
            fontFamily: "Comfortaa-Bold",
            textAlign: "center",
            margin: "4%",
            lineHeight: 30
          }}
        >
          {item.title}
        </Text>
        <Text
          style={{
            color: "white",
            fontSize: 17,
            fontFamily: "Comfortaa-Bold",
            textAlign: "center",
            lineHeight: 30,
            margin: "4%",
            marginTop: 0
          }}
        >
          {item.content}
        </Text>
      </ScrollView>
    </SafeAreaView>
  );

  render() {
    if (this.state.connected && this.state.entries) {
      return (
        <View style={{ flex: 1 }}>
          <ImageBackground
            source={require("../../assets/onBoardingLayer.jpg")}
            style={{ flex: 1 }}
            resizeMode="stretch"
          >
            <View style={{ flex: 1 }}>
              <View style={{ flex: 10 }}>
                <Carousel
                  data={this.state.entries}
                  renderItem={this.renderItem}
                  onSnapToItem={index => this.setState({ activeSlide: index })}
                  sliderWidth={slideWidth}
                  itemWidth={itemWidth}
                  contentContainerCustomStyle={{
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                />
              </View>
              <View style={{ flex: 1.5 }}>{this.pagination}</View>
              <TouchableOpacity
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center"
                }}
                activeOpacity={0.6}
                onPress={() => this.props.navigation.navigate("Login")}
              >
                <Text
                  style={{
                    textDecorationLine: "underline",
                    fontSize: 18,
                    color: "black",
                    transform: [{ translateY: -10 }]
                  }}
                >
                  {i18n.t("onBoardingMessage:getStarted")}
                </Text>
              </TouchableOpacity>
            </View>
          </ImageBackground>
        </View>
      );
    }
    return (
      <NoConnection
        onPress={() => {
          this.componentDidMount();
        }}
      />
    );
  }
}

export default withTranslation(
  ["onBoardingMessage"],
  { wait: true },
  { withRef: true }
)(onBoarding);
