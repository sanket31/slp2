import React, { Component } from "react";
import {
  ActivityIndicator,
  Alert,
  Text,
  View,
  ImageBackground,
  Image,
  StyleSheet,
  TouchableOpacity,
  Platform,
  ScrollView
} from "react-native";
import { TextInput } from "react-native-paper";
import DeviceInfo from "react-native-device-info";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import SplashScreen from "react-native-splash-screen";
import AsyncStorage from "@react-native-community/async-storage";
import { Card } from "react-native-elements";
import Modal from "react-native-modal";
import NetInfo from "@react-native-community/netinfo";
import NoConnection from "../../utility/NoConnectionAuth";
import { connect } from "react-redux";
import {
  loginUser,
  emailLogChanged,
  passwordLogChanged,
  showAuthModal,
  hideAuthModal,
  saveTokenData
} from "../../Actions";
import { NavigationService } from "../../navigator";
import i18n from "../../i18n";
import { SafeAreaView } from "react-navigation";
import { Images } from "../../assets/images";

const fcmToken = "";
const deviceOS = Platform.OS;

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emailErr: false,
      passErr: false,
      email: "",
      password: "",
      uniqueId: null,
      connected: true
    };
  }

  async componentDidMount() {
    AsyncStorage.getItem("userAccessToken", (err, resAccessToken) => {
      if (resAccessToken !== null) {
        AsyncStorage.getItem("userIdToken", async (errId, resIdToken) => {
          console.log(resAccessToken, resIdToken);
          await this.props.saveTokenData({
            accessToken: resAccessToken,
            userId: parseInt(resIdToken, 10)
          });
          NavigationService.navigate("Main");
          setTimeout(() => {
            SplashScreen.hide();
          }, 300);
        });
      } else {
        setTimeout(() => {
          SplashScreen.hide();
        }, 300);
      }
    });
    AsyncStorage.getItem("checkFirstTimeToken", (err, resFirstTime) => {
      console.log("resF", resFirstTime);
      if (resFirstTime === null) {
        AsyncStorage.setItem("checkFirstTimeToken", "firstDone");
      }
    });
    DeviceInfo.getUniqueId().then(uniqId => {
      this.setState({ uniqueId: uniqId });
    });
    this.unsubscribe = NetInfo.addEventListener(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);
      if (!state.isConnected) {
        setTimeout(() => {
          this.setState({ connected: false });
        }, 500);
      } else {
        setTimeout(() => {
          this.setState({ connected: true });
        }, 500);
      }
    });
  }

  componentWillUnmount() {
    this.props.emailLogChanged("");
    this.props.passwordLogChanged("");
    this.unsubscribe();
  }

  onEmailChange(text) {
    this.props.emailLogChanged(text);
  }

  onPasswordChange(text) {
    this.props.passwordLogChanged(text);
  }

  ValidateEmail = mail => {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
      return true;
    }
    return false;
  };

  toggleModal = () => {
    // this.setState({ isModalVisible: !this.state.isModalVisible, name: '', points: '', product: '' });
    this.props.hideAuthModal();
  };

  submit = async () => {
    const { emailLog, passwordLog, navigation } = this.props;
    let fcmValue = await AsyncStorage.getItem("FCMTOKEN");
    console.log("FCM", fcmValue);
    if (this.ValidateEmail(emailLog)) {
      this.setState({ emailErr: false });
      if (passwordLog.length > 5) {
        this.setState({ passErr: false });
        if (this.state.uniqueId !== null) {
          this.props.loginUser(
            emailLog,
            passwordLog,
            this.state.uniqueId,
            deviceOS,
            fcmValue,
            navigation
          );
        } else {
          console.log("ID is null!!");
        }
      } else {
        this.setState({ passErr: true });
      }
    } else {
      this.setState({ emailErr: true });
    }
  };

  renderButton() {
    if (this.props.loading) {
      return (
        <View style={styles.button}>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    return (
      <TouchableOpacity
        style={styles.button}
        onPress={() => this.submit()}
        activeOpacity={0.6}
      >
        <Text style={styles.loginText}>{i18n.t("login:login")}</Text>
      </TouchableOpacity>
    );
  }

  renderError() {
    if (this.props.fail) {
      return (
        <View>
          <Modal
            isVisible={this.props.isModalVisible}
            style={styles.modalStyle}
          >
            <View style={styles.cardStyle}>
              <Image
                source={require("../../assets/Icons/Alert.png")}
                resizeMode="contain"
                style={{
                  height: "25%",
                  alignSelf: "center"
                }}
              />
              <Text
                style={{
                  textAlign: "center",
                  fontFamily: "Comfortaa-Bold",
                  color: "rgb(51,51,51)",
                  margin: "5%"
                }}
              >
                {this.props.error}
              </Text>
              <TouchableOpacity
                style={[styles.buttonModal, { height: "25%" }]}
                onPress={() => this.toggleModal()}
                activeOpacity={0.6}
              >
                <Text
                  style={{
                    color: "white",
                    textAlign: "center",
                    fontSize: 15,
                    fontFamily: "Comfortaa-Bold"
                  }}
                >
                  {i18n.t("common:retry")}
                </Text>
              </TouchableOpacity>
            </View>
          </Modal>
        </View>
      );
    }
    return null;
  }

  render() {
    if (this.state.connected) {
      return (
        <SafeAreaView
          style={{ flex: 1, backgroundColor: "rgb(41,34,108)" }}
          forceInset={{ bottom: "never" }}
        >
          <ImageBackground source={Images.loginLayer} style={styles.loginLayer}>
            {this.renderError()}
            <View style={{ justifyContent: "center", flex: 0.74 }}>
              <Image
                source={Images.logo}
                style={styles.logo}
                resizeMode="contain"
              />
            </View>
            <View style={{ flex: 1 }}>
              <View
                style={{ flex: 1, justifyContent: "center", marginTop: 10 }}
              >
                <Text style={styles.titleStyle}>{i18n.t("login:login")}</Text>
              </View>
              <View style={{ flex: 5, marginHorizontal: "5%" }}>
                <TextInput
                  placeholder={i18n.t("login:enterEmail")}
                  label={
                    this.state.emailErr
                      ? i18n.t("login:enterValidEmail")
                      : i18n.t("login:emailAddress")
                  }
                  autoCapitalize="none"
                  keyboardType="email-address"
                  value={this.props.emailLog}
                  onChangeText={this.onEmailChange.bind(this)}
                  style={styles.textInputStyle}
                  theme={{ colors: { primary: "#29226c" } }}
                  error={this.state.emailErr}
                />
                <TextInput
                  label={
                    this.state.passErr
                      ? i18n.t("login:passwordContain")
                      : i18n.t("login:password")
                  }
                  placeholder="Enter password"
                  value={this.props.passwordLog}
                  onChangeText={this.onPasswordChange.bind(this)}
                  style={styles.textInputStyle}
                  theme={{ colors: { primary: "#29226c" } }}
                  error={this.state.passErr}
                  secureTextEntry
                />

                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("ForgotPassword")
                  }
                  activeOpacity={0.6}
                >
                  <Text style={styles.forgetPassStyle}>
                    {i18n.t("login:forgetPassword")}
                  </Text>
                </TouchableOpacity>
                {this.renderButton()}
                {console.log("iddd", this.state.uniqueId, deviceOS)}
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "center",
                    marginTop: 15
                  }}
                >
                  <Text
                    style={{
                      fontFamily: "Comfortaa-Bold",
                      color: "black",
                      fontSize: 15
                    }}
                  >
                    {i18n.t("login:notAMemberYet")}{" "}
                  </Text>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate("Register")}
                    activeOpacity={0.6}
                  >
                    <Text
                      style={{
                        color: "rgb(41,34,108)",
                        textDecorationLine: "underline",
                        fontFamily: "Comfortaa-Bold",
                        fontSize: 15
                      }}
                    >
                      {i18n.t("login:registerHere")}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </ImageBackground>
        </SafeAreaView>
      );
    }
    return (
      <NoConnection
        onPress={() => {
          this.componentDidMount();
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  loginLayer: {
    flex: 1
  },
  logo: {
    height: "60%",
    width: "100%"
  },
  titleStyle: {
    fontFamily: "Comfortaa-Bold",
    alignSelf: "center",
    fontSize: 27,
    color: "black"
  },
  subTitle: {
    color: "white",
    marginTop: "1%",
    alignSelf: "center",
    fontSize: 22,
    fontFamily: "Comfortaa-Bold"
  },
  textInputStyle: {
    fontFamily: "Comfortaa-Bold",
    backgroundColor: "white",
    height: hp("9%"),
    // fontSize: 15,
    justifyContent: "center"
    // includeFontPadding: true
  },
  forgetPassStyle: {
    alignSelf: "flex-end",
    fontFamily: "Comfortaa-Bold",
    color: "black",
    marginVertical: 10
  },
  button: {
    height: 47,
    width: "100%",
    borderRadius: 8,
    backgroundColor: "rgb(41,34,108)",
    justifyContent: "center"
  },
  loginText: {
    color: "white",
    fontFamily: "Comfortaa-Bold",
    fontSize: 20,
    textAlign: "center",
    paddingBottom: Platform.OS == "ios" ? 0 : 8
  },
  cardStyle: {
    height: hp("30%"),
    justifyContent: "center",
    width: "90%",
    padding: "5%",
    paddingBottom: 0,
    elevation: 6,
    backgroundColor: "white",
    borderRadius: 10
  },
  buttonModal: {
    height: "10%",
    width: "100%",
    borderRadius: 8,
    justifyContent: "center",
    backgroundColor: "rgb(41,34,108)"
  },
  modalStyle: {
    justifyContent: "center",
    alignItems: "center"
  }
});

const mapStateToProps = ({ auth }) => {
  const { loading, error, fail, emailLog, passwordLog, isModalVisible } = auth;
  return {
    loading,
    error,
    emailLog,
    fail,
    passwordLog,
    isModalVisible
  };
};

const actionCreators = {
  loginUser,
  emailLogChanged,
  passwordLogChanged,
  showAuthModal,
  hideAuthModal,
  saveTokenData
};

export default connect(mapStateToProps, actionCreators)(Login);
