import React, { Component } from "react";
import { View, Text, Modal } from "react-native";
import Spinner from "react-native-spinkit";

export const ModalSpinner = props => {
  return (
    <Modal animationType={"fade"} transparent={true} visible={props.visible}>
      <View
        style={{
          flex: 1,
          // flexDirection: "column",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <View
          style={{
            flex: 1,
            width: "100%",
            height: "100%",
            backgroundColor: "rgba(51,51,51,0.6)",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Spinner
            isVisible={props.visible}
            size={50}
            type={"Circle"}
            color={"white"}
          />
          <Text
            style={{
              color: "white",
              fontWeight: "bold",
              fontSize: 14
            }}
          >
            {props.spinnerText}
          </Text>
        </View>
      </View>
    </Modal>
  );
};
