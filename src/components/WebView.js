import React from "react";
import { Text, View, ActivityIndicator } from "react-native";
import { WebView } from "react-native-webview";

export const WebViewComponent = props => {
  return (
    <WebView
      source={{ uri: props.url }}
      renderLoading={() => (
        <ActivityIndicator
          size="large"
          color={"rgb(41,34,108)"}
          style={{
            position: "absolute",
            alignSelf: "center",
            marginTop: "70%"
          }}
        />
      )}
      startInLoadingState
      style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
    />
  );
};
