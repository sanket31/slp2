import i18n from 'i18next';
import locale from 'react-native-locale-detector';
import AsyncStorage from '@react-native-community/async-storage';
import { initReactI18next } from 'react-i18next';
import en from './en.json';
import es from './es.json';

const STORAGE_KEY = '@APP:languageCode';

const languageDetector = {
  init: Function.prototype,
  type: 'languageDetector',
  async: true,
  detect: async (callback) => {
    const savedDataJSON = await AsyncStorage.getItem(STORAGE_KEY);
    const lng = savedDataJSON || null;
    const selectedLanguage = lng || locale;
    console.log('detect - selectLanguage:', selectedLanguage);
    callback(selectedLanguage);
  },
  cacheUserLanguage: () => {},
};

i18n
  .use(languageDetector)
  .use(initReactI18next)
  .init({ 
    fallbackLng: 'en',
    resources: { en, es },

    ns: ['common'],
    defaultNS: 'common',

    debug: true,
    react: {
      wait: true,
      bindI18n: 'languageChanged loaded',
      useSuspense: false,
    }
  });

export default i18n;
