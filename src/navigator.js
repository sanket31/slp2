import React from "react";
import {
  View,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  Platform
} from "react-native";
import {
  createStackNavigator,
  createSwitchNavigator,
  createAppContainer,
  createBottomTabNavigator,
  NavigationActions,
  StackActions
} from "react-navigation";
import { Avatar, Badge, Icon, withBadge } from "react-native-elements";
import AsyncStorage from "@react-native-community/async-storage";
import Login from "./components/Auth/login";
import Register from "./components/Auth/Register";
import RegisterSecond from "./components/Auth/registerSecond";
import ForgotPassword from "./components/Auth/forgetPassword";
import onBoarding from "./components/Auth/onBoarding";
import Home from "./components/MainScreens/HomeStack/Home";
import RewardPoints from "./components/MainScreens/RewardsStack/rewardPoints";
import Scan from "./components/MainScreens/HomeStack/Scan/Scan";
import Settings from "./components/MainScreens/SettingsStack/Settings";
import Profile from "./components/MainScreens/SettingsStack/SettingsScreen/Profile";
import AboutUs from "./components/MainScreens/SettingsStack/SettingsScreen/Aboutus";
import LearnToEarn from "./components/MainScreens/HomeStack/LearnToEarn/LearnToEarn";
import LeaderBoard from "./components/MainScreens/SettingsStack/SettingsScreen/LeaderBoard";
import PrivacyPolicy from "./components/MainScreens/SettingsStack/SettingsScreen/PrivacyPolicy";
import TermsAndCond from "./components/MainScreens/SettingsStack/SettingsScreen/Terms&Cond";
import VideoPlayer from "./components/MainScreens/HomeStack/LearnToEarn/VideoPlayer";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import Quiz from "./components/MainScreens/HomeStack/LearnToEarn/Quiz";
import ProductActivation from "./components/MainScreens/HomeStack/Scan/productActivation";
import Product from "./components/MainScreens/ProductStack/Product";
import TechAndSupp from "./components/MainScreens/HomeStack/TechAndSupport/Tech";
import Referral from "./components/MainScreens/HomeStack/Referral/referral";
import ProductActivation2 from "./components/MainScreens/HomeStack/Scan/productActivation2";
import ProductDetails from "./components/MainScreens/ProductStack/ProductDetails";
import PdfViewer from "./components/MainScreens/ProductStack/pdfViewer";
import SplitScreen from "./components/MainScreens/HomeStack/Scan/splitScreen";
import QRHistory from "./components/MainScreens/QRHistoryStack/History";
import i18n from "i18next";

import {
  fromLeft,
  fromRight,
  zoomIn,
  fadeIn,
  zoomOut,
  fromBottom
} from "react-navigation-transitions";
import Dispute from "./components/MainScreens/HomeStack/Scan/Dispute";
import DisputeRequest from "./components/MainScreens/HomeStack/Scan/DisputeRequest";
import Gift from "./components/MainScreens/RewardsStack/Gift";
import Result from "./components/MainScreens/HomeStack/LearnToEarn/Result";
import Notifications from "./components/MainScreens/HomeStack/Notifications";
import ContestList from "./components/MainScreens/SettingsStack/SettingsScreen/ContestList";
import Contest from "./components/MainScreens/SettingsStack/SettingsScreen/Contest";
import ContestWinners from "./components/MainScreens/SettingsStack/SettingsScreen/ContestWinners";
import { Images } from "./assets/images";
import AssignedJobs from "./components/MainScreens/SettingsStack/SettingsScreen/AssignedJobs";
import JobDetails from "./components/MainScreens/SettingsStack/SettingsScreen/JobDetails";

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params
    })
  );
}

const styles = StyleSheet.create({
  iconStyle: {
    height: 20,
    width: 25,
    marginTop: "10%"
  }
});

const MainNavigator = createStackNavigator({
  Login: {
    screen: Login,
    navigationOptions: {
      header: null
    }
  },
  Register: {
    screen: Register,
    navigationOptions: {
      header: null
    }
  },
  RegisterSecond: {
    screen: RegisterSecond,
    navigationOptions: {
      header: null
    }
  },
  ForgotPassword: {
    screen: ForgotPassword,
    navigationOptions: {
      header: null
    }
  },
  TermsAndCondAuth: {
    screen: TermsAndCond,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: (
          <View
            style={{
              flexDirection: "row",
              flex: 1,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <TouchableOpacity
              style={{ flex: 0.1 }}
              onPress={() => navigation.goBack()}
            >
              <Image
                source={require("./assets/settingsIcon/back.png")}
                resizeMode="contain"
                style={{
                  height: hp("3%"),
                  marginLeft: hp("2%"),
                  alignSelf: "center",
                  marginTop: "7%"
                }}
              />
            </TouchableOpacity>
            {/* <View style={{flex:0.8}} > */}
            <Text
              style={{
                flex: 0.8,
                color: "white",
                fontSize: 20,
                alignSelf: "center",
                textAlign: "center",
                marginLeft: "auto",
                marginRight: "auto",
                fontFamily: "Comfortaa-Bold"
              }}
            >
              {i18n.t("settings:terms")}
            </Text>
            {/* </View> */}
            <View style={{ flex: 0.1 }} />
          </View>
        ),
        headerLeft: null,
        headerStyle: {
          backgroundColor: "rgb(41,34,108)",
          borderBottomWidth: 0,
          height: hp("7%"),
          elevation: 0
        }
      };
    }
  }
});

const Video_Quiz = createStackNavigator(
  {
    LearnToEarn: {
      screen: LearnToEarn,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => navigation.navigate("HomeScreen")}
              >
                <Image
                  source={require("./assets/settingsIcon/back.png")}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {i18n.t("title:learnToEarn")}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    },
    VideoPlayer: {
      screen: VideoPlayer,
      navigationOptions: {
        header: null
      }
    },

    Result: {
      screen: Result,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => navigation.goBack()}
              >
                <Image
                  source={require("./assets/settingsIcon/back.png")}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {i18n.t("title:result")}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    },
    Quiz: {
      screen: Quiz,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => navigation.goBack()}
              >
                <Image
                  source={require("./assets/settingsIcon/back.png")}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {i18n.t("title:quiz")}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    }
  },
  {
    transparentCard: true,
    transitionConfig: () => {
      if (Platform.OS == "android") {
        return fromBottom(800);
      }
    },
    mode: "modal",
    cardStyle: {
      opacity: 1
    }
  }
);

const RewardStack = createStackNavigator({
  RewardPoints: RewardPoints,
  Gift: Gift
});

const ProductStack = createStackNavigator(
  {
    Product: {
      screen: Product
    },
    ProductDetails: {
      screen: ProductDetails,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => navigation.goBack()}
              >
                <Image
                  source={require("./assets/settingsIcon/back.png")}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {i18n.t("title:productDetails")}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    },
    PDF: {
      screen: PdfViewer,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => navigation.goBack()}
              >
                <Image
                  source={Images.back}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {"PDF Reader"}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    },
  },
  {
    transitionConfig: () => {
      if (Platform.OS == "android") {
        return fromBottom(800);
      }
    }
  }
);

const ScanStack = createStackNavigator(
  {
    Scan: Scan,
    Dispute: {
      screen: Dispute,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => navigation.goBack()}
              >
                <Image
                  source={require("./assets/settingsIcon/back.png")}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {i18n.t("title:result")}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    },
    DisputeRequest: {
      screen: DisputeRequest,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => navigation.goBack()}
              >
                <Image
                  source={require("./assets/settingsIcon/back.png")}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {i18n.t("Scan:disputeRequest")}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    },
    SprayPoints: {
      screen: ProductActivation2,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => navigation.goBack()}
              >
                <Image
                  source={require("./assets/settingsIcon/back.png")}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {i18n.t("title:sprayPoints")}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    },
    SplitScreen: {
      screen: SplitScreen,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => navigation.goBack()}
              >
                <Image
                  source={require("./assets/settingsIcon/back.png")}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {i18n.t("title:splitPoints")}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    },
    ProductActivation: {
      screen: ProductActivation,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => navigation.goBack()}
              >
                <Image
                  source={require("./assets/settingsIcon/back.png")}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {i18n.t("title:productActivation")}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    }
  },
  {
    initialRouteName: "Scan",
    transparentCard: true,
    transitionConfig: () => {
      if (Platform.OS == "android") {
        return fadeIn(200);
      }
    },
    mode: "card",
    cardStyle: {
      opacity: 1
    }
  }
);

const TechStack = createStackNavigator(
  {
    PDFLibrary: {
      screen: PdfViewer,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => navigation.goBack()}
              >
                <Image
                  source={Images.back}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {"PDF Reader"}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    },
    VideoPlayerLibrary: {
      screen: VideoPlayer,
      navigationOptions: {
        header: null
      }
    },
    Tech: {
      screen: TechAndSupp,
      navigationOptions: ({ navigation }) => {
        console.log('navigaion tech ===>', navigation)
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => NavigationService.navigate("HomeScreen")}
              >
                <Image
                  source={require("./assets/settingsIcon/back.png")}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {i18n.t("title:techSupport")}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    }
  },
  {
    initialRouteName: "Tech"
  }
);

const HomeTab = createStackNavigator({
  HomeScreen: {
    screen: Home,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: (
          <View
            style={{
              flexDirection: "row",
              flex: 1,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Text
              style={{
                color: "white",
                fontSize: i18n.language.includes("es") ? 16 : 20,
                alignSelf: "center",
                fontFamily: "Comfortaa-Bold",
                flexShrink: 1
              }}
            >
              {i18n.t("common:SLP")}
            </Text>
            <TouchableOpacity
              onPress={() => navigation.navigate("Notifications")}
              activeOpacity={0.6}
            >
              <Image
                source={Images.bell}
                resizeMode="contain"
                style={{ height: 20, width: 20, marginLeft: 10, marginTop: 5 }}
              />
              {navigation.getParam("badgeValue", "0") > 0 ? (
                <Badge
                  value={navigation.getParam("badgeValue", "0")}
                  containerStyle={{
                    position: "absolute",
                    bottom: "25%",
                    left: "55%",
                    borderRadius: 50
                  }}
                  badgeStyle={{ backgroundColor: "red" }}
                />
              ) : null}
            </TouchableOpacity>
          </View>
        ),
        headerStyle: {
          backgroundColor: "rgb(41,34,108)",
          borderBottomWidth: 0,
          height: hp("7%"),
          elevation: 0
        }
      };
    }
  }
});

const HomeStack = createStackNavigator(
  {
    HomeScreen: {
      screen: HomeTab,
      navigationOptions: {
        header: null
      }
    },
    Notifications: Notifications,
    TechScreen: {
      screen: TechStack,
      navigationOptions: {
        header: null
      }
    },
    VideoScreen: {
      screen: Video_Quiz,
      navigationOptions: {
        header: null
      }
    },
    ScanScreen: {
      screen: ScanStack,
      navigationOptions: {
        header: null
      }
    },
    Contest: {
      screen: Contest,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => navigation.goBack()}
              >
                <Image
                  source={require("./assets/settingsIcon/back.png")}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {i18n.t("contest:contest")}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    },
    ContestList: {
      screen: ContestList,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => navigation.goBack()}
              >
                <Image
                  source={require("./assets/settingsIcon/back.png")}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {i18n.t("contest:contestList")}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    },

  },
  {
    transitionConfig: () => {
      if (Platform.OS == "android") {
        return fromBottom(800);
      }
    }
  }
);

const QRHistoryStack = createStackNavigator({
  QRHistoryPage: {
    screen: QRHistory
  },
  ProductActivationHistory: {
    screen: ProductActivation,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: (
          <View
            style={{
              flexDirection: "row",
              flex: 1,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <TouchableOpacity
              style={{ flex: 0.1 }}
              onPress={() => navigation.goBack()}
            >
              <Image
                source={require("./assets/settingsIcon/back.png")}
                resizeMode="contain"
                style={{
                  height: hp("3%"),
                  marginLeft: hp("2%"),
                  alignSelf: "center",
                  marginTop: "7%"
                }}
              />
            </TouchableOpacity>
            {/* <View style={{flex:0.8}} > */}
            <Text
              style={{
                flex: 0.8,
                color: "white",
                fontSize: 20,
                alignSelf: "center",
                textAlign: "center",
                marginLeft: "auto",
                marginRight: "auto",
                fontFamily: "Comfortaa-Bold"
              }}
            >
              {i18n.t("title:productActivation")}
            </Text>
            {/* </View> */}
            <View style={{ flex: 0.1 }} />
          </View>
        ),
        headerLeft: null,
        headerStyle: {
          backgroundColor: "rgb(41,34,108)",
          borderBottomWidth: 0,
          height: hp("7%"),
          elevation: 0
        }
      };
    }
  },
  SplitScreenQrHistory: {
    screen: SplitScreen,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: (
          <View
            style={{
              flexDirection: "row",
              flex: 1,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <TouchableOpacity
              style={{ flex: 0.1 }}
              onPress={() => navigation.goBack()}
            >
              <Image
                source={require("./assets/settingsIcon/back.png")}
                resizeMode="contain"
                style={{
                  height: hp("3%"),
                  marginLeft: hp("2%"),
                  alignSelf: "center",
                  marginTop: "7%"
                }}
              />
            </TouchableOpacity>
            {/* <View style={{flex:0.8}} > */}
            <Text
              style={{
                flex: 0.8,
                color: "white",
                fontSize: 20,
                alignSelf: "center",
                textAlign: "center",
                marginLeft: "auto",
                marginRight: "auto",
                fontFamily: "Comfortaa-Bold"
              }}
            >
              {i18n.t("title:splitPoints")}
            </Text>
            {/* </View> */}
            <View style={{ flex: 0.1 }} />
          </View>
        ),
        headerLeft: null,
        headerStyle: {
          backgroundColor: "rgb(41,34,108)",
          borderBottomWidth: 0,
          height: hp("7%"),
          elevation: 0
        }
      };
    }
  }
});

const settingsNavigator = createStackNavigator(
  {
    Settings: {
      screen: Settings,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <View style={{ flex: 0.1 }}></View>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {i18n.t("bottomTab:settings")}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    },
    ReferralScreen: {
      screen: Referral,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => navigation.goBack()}
              >
                <Image
                  source={require("./assets/settingsIcon/back.png")}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {i18n.t("title:referralPoints")}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    },
    Profile: {
      screen: Profile,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => navigation.goBack()}
              >
                <Image
                  source={require("./assets/settingsIcon/back.png")}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {i18n.t("title:viewProfile")}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    },
    PDFLibrary: {
      screen: PdfViewer,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => navigation.goBack()}
              >
                <Image
                  source={Images.back}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {"PDF Reader"}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    },
    AssignedJobs: {
      screen: AssignedJobs,
      navigationOptions: ({ navigation }) => {

          return {
            headerTitle: (
              <View
                style={{
                  flexDirection: "row",
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <TouchableOpacity
                  style={{ flex: 0.1 }}
                  onPress={() => navigation.goBack()}
                >
                  <Image
                    source={require("./assets/settingsIcon/back.png")}
                    resizeMode="contain"
                    style={{
                      height: hp("3%"),
                      marginLeft: hp("2%"),
                      alignSelf: "center",
                      marginTop: "7%"
                    }}
                  />
                </TouchableOpacity>
                {/* <View style={{flex:0.8}} > */}
                <Text
                  style={{
                    flex: 0.8,
                    color: "white",
                    fontSize: 20,
                    alignSelf: "center",
                    textAlign: "center",
                    marginLeft: "auto",
                    marginRight: "auto",
                    fontFamily: "Comfortaa-Bold"
                  }}
                >
                  Your Tasks
                  {/* {i18n.t("title:viewProfile")} */}
                </Text>
                {/* </View> */}
                <View style={{ flex: 0.1 }} />
              </View>
            ),
            headerLeft: null,
            headerStyle: {
              backgroundColor: "rgb(41,34,108)",
              borderBottomWidth: 0,
              height: hp("7%"),
              elevation: 0
            }
          };
        }
    },
    JobDetails: {
      screen: JobDetails,
      navigationOptions: ({ navigation }) => {

          return {
            headerTitle: (
              <View
                style={{
                  flexDirection: "row",
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <TouchableOpacity
                  style={{ flex: 0.1 }}
                  onPress={() => navigation.goBack()}
                >
                  <Image
                    source={require("./assets/settingsIcon/back.png")}
                    resizeMode="contain"
                    style={{
                      height: hp("3%"),
                      marginLeft: hp("2%"),
                      alignSelf: "center",
                      marginTop: "7%"
                    }}
                  />
                </TouchableOpacity>
                {/* <View style={{flex:0.8}} > */}
                <Text
                  style={{
                    flex: 0.8,
                    color: "white",
                    fontSize: 20,
                    alignSelf: "center",
                    textAlign: "center",
                    marginLeft: "auto",
                    marginRight: "auto",
                    fontFamily: "Comfortaa-Bold"
                  }}
                >
                  Job Details
                </Text>
                {/* </View> */}
                <View style={{ flex: 0.1 }} />
              </View>
            ),
            headerLeft: null,
            headerStyle: {
              backgroundColor: "rgb(41,34,108)",
              borderBottomWidth: 0,
              height: hp("7%"),
              elevation: 0
            }
          };
        }
    },
    ContestWinners: {
      screen: ContestWinners,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => navigation.goBack()}
              >
                <Image
                  source={require("./assets/settingsIcon/back.png")}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {i18n.t("title:winner")}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    },
    AboutUs: {
      screen: AboutUs,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => navigation.goBack()}
              >
                <Image
                  source={require("./assets/settingsIcon/back.png")}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {i18n.t("settings:aboutUs")}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    },
    LeaderBoard: {
      screen: LeaderBoard,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => navigation.goBack()}
              >
                <Image
                  source={require("./assets/settingsIcon/back.png")}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {i18n.t("settings:leaderboard")}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    },
    PrivacyPolicy: {
      screen: PrivacyPolicy,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => navigation.goBack()}
              >
                <Image
                  source={require("./assets/settingsIcon/back.png")}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {i18n.t("title:privacyPolicy")}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    },
    TermsAndCond: {
      screen: TermsAndCond,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle: (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity
                style={{ flex: 0.1 }}
                onPress={() => navigation.goBack()}
              >
                <Image
                  source={require("./assets/settingsIcon/back.png")}
                  resizeMode="contain"
                  style={{
                    height: hp("3%"),
                    marginLeft: hp("2%"),
                    alignSelf: "center",
                    marginTop: "7%"
                  }}
                />
              </TouchableOpacity>
              {/* <View style={{flex:0.8}} > */}
              <Text
                style={{
                  flex: 0.8,
                  color: "white",
                  fontSize: 20,
                  alignSelf: "center",
                  textAlign: "center",
                  marginLeft: "auto",
                  marginRight: "auto",
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                {i18n.t("settings:terms")}
              </Text>
              {/* </View> */}
              <View style={{ flex: 0.1 }} />
            </View>
          ),
          headerLeft: null,
          headerStyle: {
            backgroundColor: "rgb(41,34,108)",
            borderBottomWidth: 0,
            height: hp("7%"),
            elevation: 0
          }
        };
      }
    }
  },
  {
    transitionConfig: () => {
      if (Platform.OS == "android") {
        return fromBottom(800);
      }
    }
  }
);

const BottomTabNavigator = createBottomTabNavigator(
  {
    Home: {
      screen: HomeStack,
      navigationOptions: ({ navigation }) => {
        let tabBarVisible = true;
        if (navigation.state.index > 0) {
          tabBarVisible = false;
        }
        return {
          tabBarVisible,
          header: null,
          tabBarLabel: ({ focused }) => (
            <Text
              style={{
                fontSize: 12,
                color: focused ? "rgb(41,34,108)" : "grey",
                alignSelf: "center",
                marginBottom: 5,
                fontFamily: "Comfortaa-Bold"
              }}
            >
              {i18n.t("bottomTab:home")}
            </Text>
          ),
          tabBarIcon: ({ focused }) => (
            <Image
              source={
                focused
                  ? require("./assets/Icons/homeActive.png")
                  : require("./assets/Icons/homeInActive.png")
              }
              resizeMode="contain"
              style={styles.iconStyle}
            />
          ),
          tabBarOnPress: () =>
            StackActions.reset({
              index: 0,
              actions: NavigationService.navigate("Home")
            })
        };
      }
    },
    RewardPoints: {
      screen: RewardStack,
      navigationOptions: ({ navigation }) => {
        let tabBarVisible = true;
        if (navigation.state.index > 0) {
          tabBarVisible = false;
        }
        return {
          tabBarVisible,
          tabBarLabel: ({ focused }) => (
            <Text
              style={{
                fontSize: i18n.language.includes("es") ? 11 : 12,
                color: focused ? "rgb(41,34,108)" : "grey",
                alignSelf: "center",
                marginBottom: 5,
                fontFamily: "Comfortaa-Bold"
              }}
              ellipsizeMode="tail"
              numberOfLines={1}
            >
              {i18n.t("bottomTab:rewards")}
            </Text>
          ),
          tabBarIcon: ({ focused }) => (
            <Image
              source={
                focused
                  ? require("./assets/Icons/dollarActive.png")
                  : require("./assets/Icons/dollar.png")
              }
              resizeMode="contain"
              style={styles.iconStyle}
            />
          )
        };
      }
    },
    Product: {
      screen: ProductStack,
      navigationOptions: ({ navigation }) => {
        let tabBarVisible = true;
        if (navigation.state.index > 0) {
          tabBarVisible = false;
        }
        return {
          tabBarVisible,
          tabBarLabel: ({ focused }) => (
            <Text
              style={{
                fontSize: 12,
                color: focused ? "rgb(41,34,108)" : "grey",
                alignSelf: "center",
                marginBottom: 5,
                fontFamily: "Comfortaa-Bold"
              }}
            >
              {i18n.t("bottomTab:product")}
            </Text>
          ),
          tabBarIcon: ({ focused }) => (
            <Image
              source={
                focused
                  ? require("./assets/Icons/productActive.png")
                  : require("./assets/Icons/productInActive.png")
              }
              resizeMode="contain"
              style={styles.iconStyle}
            />
          )
        };
      }
    },
    QRHistory: {
      screen: QRHistoryStack,
      navigationOptions: ({ navigation }) => {
        let tabBarVisible = true;
        if (navigation.state.index > 0) {
          tabBarVisible = false;
        }
        return {
          tabBarVisible,
          tabBarLabel: ({ focused }) => (
            <Text
              style={{
                fontSize: 12,
                color: focused ? "rgb(41,34,108)" : "grey",
                alignSelf: "center",
                marginBottom: 5,
                fontFamily: "Comfortaa-Bold"
              }}
            >
              {i18n.t("bottomTab:history")}
            </Text>
          ),
          tabBarIcon: ({ focused }) => (
            <Image
              source={
                focused
                  ? require("./assets/Icons/scanActive.png")
                  : require("./assets/Icons/scan.png")
              }
              resizeMode="contain"
              style={styles.iconStyle}
            />
          )
        };
      }
    },

    Settings: {
      screen: settingsNavigator,
      navigationOptions: ({ navigation }) => {
        let tabBarVisible = true;
        if (navigation.state.index > 0) {
          tabBarVisible = false;
        }
        return {
          tabBarVisible,
          tabBarLabel: ({ focused }) => (
            <Text
              style={{
                fontSize: 12,
                color: focused ? "rgb(41,34,108)" : "grey",
                alignSelf: "center",
                marginBottom: 5,
                fontFamily: "Comfortaa-Bold"
              }}
            >
              {i18n.t("bottomTab:settings")}
            </Text>
          ),
          tabBarIcon: ({ focused }) => (
            <Image
              source={
                focused
                  ? require("./assets/Icons/settingsActive.png")
                  : require("./assets/Icons/settings.png")
              }
              resizeMode="contain"
              style={styles.iconStyle}
            />
          )
        };
      }
    }
  },
  {
    tabBarOptions: {
      style: {
        backgroundColor: "rgb(255,255,255)",
        borderTopWidth: 0,
        elevation: 5,
        shadowOffset: { width: 0, height: 1 },
        shadowRadius: 2,
        shadowOpacity: 0.6
      }
    }
  }
);

const MainScreens = createStackNavigator(
  {
    BottomTabNavigator: {
      screen: BottomTabNavigator
    }
  },
  {
    headerMode: "none"
  }
);

const TopLevelNavigator = createSwitchNavigator({
  OnBoard: onBoarding,
  Auth: MainNavigator,
  Main: MainScreens
});

export const MainRouter = createAppContainer(TopLevelNavigator);

export const NavigationService = {
  navigate,
  setTopLevelNavigator
};
