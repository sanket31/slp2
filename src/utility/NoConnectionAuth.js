import React from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
} from "react-native";
import { Images } from "../assets/images";

export default class NoInternetAuth extends React.Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
              <ImageBackground
                source={Images.loginLayer}
                style={{ flex: 1 }}
              >
                <View
                  style={{
                    justifyContent: "flex-end",
                    alignItems: "center",
                    flex: 0.6,
                  }}
                >
                  <Text
                    style={{
                      color: "white",
                      fontSize: 20,
                      textAlign: "center",
                      fontFamily: "Comfortaa-Bold",
                    }}
                  >
                    Sprayer Loyalty Program
                  </Text>
                </View>
                <View style={styles.cardStyle}>
                  <View
                    style={{
                      justifyContent: "center",
                      flexDirection: "column",
                      alignItems: "center",
                    }}
                  >
                    <Image
                      source={Images.noConnection}
                      style={{
                        height: "45%",
                        width: "70%",
                      }}
                      resizeMode={"contain"}
                    />
                    <Text
                      style={{
                        color: "black",
                        fontSize: 27,
                        fontFamily: "Comfortaa-Bold",
                        textAlign: "center",
                        lineHeight: 30,
                      }}
                    >
                      OOPS!{"\n"} NO INTERNET
                    </Text>
                    <Text
                      style={{
                        color: "black",
                        fontSize: 15,
                        fontFamily: "Comfortaa-Bold",
                        textAlign: "center",
                        lineHeight: 30,
                        marginBottom: "5%",
                      }}
                    >
                      Please check your network connection.
                    </Text>
                    <TouchableOpacity
                      style={styles.button}
                      onPress={this.props.onPress}
                    >
                      <Text style={styles.retryText}>Try Again</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </ImageBackground>
            </View>
          );
    }
}

const styles = StyleSheet.create({
    cardStyle: {
      justifyContent: "center",
      flex: 9,
      height: "95%",
      width: "90%",
      padding: "4%",
      paddingTop: 8,
      paddingBottom: 12,
      elevation: 6,
      backgroundColor: "white",
      borderRadius: 10,
      shadowColor: "#000",
      shadowOffset: { width: 0, height: 1 },
      shadowOpacity: 0.5,
      shadowRadius: 2,
      margin: "5%",
    },
    retryText: {
      color: "white",
      fontFamily: "Comfortaa-Bold",
      fontSize: 20,
      paddingBottom: 5,
      alignSelf: "center",
    },
    button: {
      height: "12%",
      width: "100%",
      borderRadius: 8,
      justifyContent: "center",
      backgroundColor: "rgb(41,34,108)",
    },
  });
  