import {
  BARREL_DATA,
  SHOW_MODAL,
  HIDE_MODAL,
  SCAN_USER_FAIL,
  BARREL_SEARCH,
  PRODUCT_ACTIVATION_DATA,
  ACTIVATION_FAIL,
  PRODUCT_SAVE_DATA,
  DONT_SPLIT_DATA,
  DONT_SPLIT_ERROR,
  SPLIT_USER_DATA,
  SPLIT_USER_LIST_ERROR,
  USER_NAME_CHANGED_FOR_SPLIT,
  SPLIT_USERS_SUCCESS,
  SPLIT_USERS_FAIL,
  USER_REWARDS_DATA,
  USER_REWARDS_FAIL,
  GET_REWARDS_LOADER,
  CANCEL_QR_SUCCESS,
  CANCEL_QR_FAIL,
  CANCEL_QR,
  PRODUCT_ACTIVATION_LOADER,
  SPLIT_USERS_LOADER,
  UPLOAD_PROGRESS_ACTIVATION
} from "../Actions/types";

const INITIAL_STATE = {
  error: null,
  fail: false,
  loading: false,
  barrelData: null,
  isModalVisible: false,
  productActivateData: null,
  saveNContinueModal: false,
  submitModal: false,
  dontSplitModal: false,
  failActivate: false,
  productSaveData: null,
  dontSplitData: null,
  userNames: "",
  splitUserList: null,
  splitUserSuccessData: null,
  isSplitSuccessModal: false,
  rewardsData: "",
  rewardsFail: false,
  rewardLoading: false,
  rewardsFailMessage: "",
  cancelError: false,
  QrScanLoader: false,
  QrScanFail: false,
  QrError: null,
  productActivationLoader: false,
  dontSplitSuccess: false,
  dontSpliterror: null,
  userListError: null,
  splitScreenLoader: false,
  splitUsersError: null,
  splitUserSuccess: false,
  uploadProgress: null, 
  saveContinueSucess : false,
  splitsucess : false,
  dontsplitsucess : false,
  alreadyScannedData: null
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case BARREL_SEARCH:
      return {
        ...state,
        QrScanLoader: true,
        QrError: null,
        dontSplitSuccess: false,
        QrScanFail: false,
        alreadyScannedData: null
      };
    case PRODUCT_ACTIVATION_LOADER:
      return {
        ...state,
        dontSplitSuccess: false,
        saveContinueSucess : false,
        productActivationLoader: true,
      };
    case SPLIT_USERS_LOADER:
      return {
        ...state,
        splitScreenLoader: true,
        dontSplitSuccess: false,
        splitUserSuccess: false
      };
      case UPLOAD_PROGRESS_ACTIVATION:
        console.log('UPload dont split', action.payload);
        return {
          ...state,
          uploadProgress: action.payload
        };
    case CANCEL_QR:
      return {
        ...state,
        cancelError: false,
        productActivationLoader: true,
      };
    case CANCEL_QR_SUCCESS:
      console.log("CANCEL_QR_SUCCESS");
      return {
        ...state,
        cancelError: false,
        productActivationLoader: false,
      };
    case CANCEL_QR_FAIL:
      console.log("CANCEL_QR_FAIL", action.payload);
      return {
        ...state,
        cancelError: true,
        productActivationLoader: false,
      };
    case USER_NAME_CHANGED_FOR_SPLIT:
      console.log("USER_NAME_CHANGED_FOR_SPLIT", action.payload);
      return {
        ...state,
        userNames: action.payload,
      };
    case BARREL_DATA:
      console.log("barrrrrrreeell", action.payload);
      return {
        ...state,
        barrelData: action.payload,
        QrScanFail: false,
        QrScanLoader: false,
        QrError: null,
      };
    case PRODUCT_ACTIVATION_DATA:
      console.log("activation data", action.payload);
      return {
        ...state,
        productActivateData: action.payload,
        // isModalVisible: true,
        fail: false,
        submitModal: true,
        loading: false,
      };
    case PRODUCT_SAVE_DATA:
      console.log("save data", action.payload);
      return {
        ...state,
        saveContinueSucess :true,
        uploadProgress: null,
        productActivationLoader: false,
      };
    case DONT_SPLIT_DATA:
      console.log("DONT_SPLIT_DATA", action.payload);
      return {
        ...state,
        rewardsData: action.payload,
        uploadProgress: null,
        dontSplitSuccess: true,
        productActivationLoader: false,
        dontsplitsucess : true
      };
    case DONT_SPLIT_ERROR:
      console.log("DONT_SPLIT_ERROR", action.payload);
      return {
        ...state,
        dontSpliterror: action.payload,
        uploadProgress: null,
        dontSplitSuccess: false,
        productActivationLoader: false,
      };
    case SPLIT_USER_DATA:
      console.log("SPLIT_USER_DATA", action.payload);
      return {
        ...state,
        splitUserList: action.payload,
        productActivationLoader: false,
        userListError: null,
        splitsucess : true
      };
    case SPLIT_USER_LIST_ERROR:
      console.log("USER_LIST_ERROR", action.payload);
      return {
        ...state,
        userListError: action.payload,
        productActivationLoader: false,
      };
    case SHOW_MODAL:
      return { ...state, isModalVisible: true };
    case HIDE_MODAL:
      return {
        ...state,
        isModalVisible: false,
        submitModal: false,
        dontSplitModal: false,
        isSplitSuccessModal: false,
        fail: false,
      };
    case SCAN_USER_FAIL:
      console.log("faiiil barrel");
      return {
        ...state,
        QrScanFail: true,
        QrScanLoader: false,
        QrError: action.payload,
        alreadyScannedData: action.payload.data
      };
    case ACTIVATION_FAIL:
      console.log("faiiil ACTIVATION");
      return {
        ...state,
        uploadProgress: null,
        productActivationLoader: false,
        error: action.payload,
      };
    case SPLIT_USERS_SUCCESS:
      console.log("SPLIT_USERS_SUCCESS", action.payload);
      return {
        ...state,
        splitUserSuccess: true,
        rewardsData: action.payload,
        splitScreenLoader: false,
        uploadProgress: null,
        splitUsersError: null,
      };
    case SPLIT_USERS_FAIL:
      console.log("SPLIT_USERS_FAIL", action.payload);
      return {
        ...state,
        splitUserSuccess: false,
        uploadProgress: null,
        splitUsersError: action.payload.message,
        splitScreenLoader: false,
      };
      case GET_REWARDS_LOADER:
      return {
        ...state,
        rewardLoading: true,
        // isModalVisible: true,
        rewardsFail: false,
      };
    case USER_REWARDS_DATA:
      const rewardData = (action.payload).reverse();
      console.log("USER_REWARDS_DATA", action.payload);
      return {
        ...state,
        rewardsData: rewardData,
        rewardLoading: false,
        rewardsFail: false,
      };
    case USER_REWARDS_FAIL:
      console.log("USER_REWARDS_FAIL", action.payload);
      return {
        ...state,
        rewardsFailMessage: action.payload,
        rewardLoading: false,
        rewardsFail: true,
      };
    default:
      return state;
  }
};
