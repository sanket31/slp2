import {
  GET_JOBLIST,
  SET_JOBLIST_LOADER
} from "../Actions/types";

const INITIAL_STATE = {
  joblist: [],
  jobDetails: "",
  listloader : false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_JOBLIST:
      return {
        ...state,
        joblist: action.payload,
        listloader : false
      };
    case SET_JOBLIST_LOADER : 
    return {
      ...state,
      listloader : action.payload
    }
    case "GET_JOBDETAILS":
      return {
        ...state,
        jobDetails: action.payload,
        listloader: false
      };
    case "RESET_JOBDETAILS":
      return {
        ...state,
        jobDetails: ""
      };
    default:
      return state;
  }
};
