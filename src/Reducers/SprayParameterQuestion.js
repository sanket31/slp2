import {
  SET_BASIC_SETTINGS,
  SET_ADVANCE_SETTINGS,
  GET_RECOMMENDATION,
  SET_LOADER,
  SET_RECOMMENDATION_ERROR,
  SAVE_QUESTION_ONE
} from "../Actions/types";

const INITIAL_STATE = {
  basicQuestions: [],
  advanceQuestions: [],
Recommendationdata: {},
  loader: false,
  error: false,
  error_msg: "",
  save_question_one : null
};

export default (state = INITIAL_STATE, action) => {
   switch (action.type) {
    
    case SET_BASIC_SETTINGS:
      let { qid, question, selected_option } = action.payload;
      let filterArray = [];
      if (selected_option == null) {
        filterArray = state.basicQuestions.filter(item => item.qid != qid);
      } else if (state.basicQuestions.length > 0 && selected_option != null) {
        filterArray = state.basicQuestions.filter(
          (item, index) => item.qid !== qid
        );
        filterArray.push(action.payload);
        console.log("add condition called ", filterArray);
      } else {
        filterArray.push(action.payload);
      }
      return {
        ...state,
        basicQuestions: filterArray
      };
    case SET_ADVANCE_SETTINGS:
      console.log('set advance called', action.payload)
      let filterArray2 = [];
      if (action.payload.selected_option == null) {
        console.log('null contidition called')
        filterArray2 = state.advanceQuestions.filter(item => item.qid != action.payload.qid);
      } else if(state.advanceQuestions.length > 0 && action.payload.selected_option != null) {
        console.log('condition true')
        filterArray2 = state.advanceQuestions.filter(
          (item, index) => item.qid !== action.payload.qid
        );
        filterArray2.push(action.payload);
        console.log("add condition called ", filterArray2);
      } else {
        filterArray2.push(action.payload);
      }
      return {
        ...state,
        advanceQuestions: filterArray2
      };
    case GET_RECOMMENDATION:
      return {
        ...state,
        loader: false,
        basicQuestions : [],
        advanceQuestions : [],
        Recommendationdata: action.payload
      };
    case SET_LOADER:
      return {
        ...state,
        loader: action.payload
      };
    case SET_RECOMMENDATION_ERROR:
      return {
        ...state,
        loader: false,
        error: true,
        error_msg: action.payload
      };
    case SAVE_QUESTION_ONE:
       return {
         ...state,
         save_question_one : action.payload
       }
    default:
      return { ...state };
  }
};
