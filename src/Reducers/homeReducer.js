import { DASHBOARD_DATA, DASHBOARD_DATA_ERROR, CHANGE_ROUTE_TECH_SUPPORT } from "../Actions/types";

const INITIAL_STATE = {
  dashboardData: null,
  dashboardDataError: null,
  loading: false,
  dashboardFail: false,
  routeTechSupport: ''
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case DASHBOARD_DATA:
      return { ...state, dashboardData: action.payload, dashboardFail: false };
      case DASHBOARD_DATA_ERROR:
          console.log("DASHBOARD_DATA_ERROR", action.payload);
          return { ...state, dashboardDataError: action.payload, dashboardFail: true };
          case CHANGE_ROUTE_TECH_SUPPORT:
            return { ...state, routeTechSupport: action.payload };    
    default:
      return state;
  }
};
