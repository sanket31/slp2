import {
  PRODUCTS_LIST_DATA,
  PRODUCTS_LIST_ERROR,
  PRODUCT_DETAIL_FETCHED,
  PRODUCT_DETAIL_ERROR,
  RESET_PRODUCT_DETAIL,
} from "../Actions/types";

const INITIAL_STATE = {
  productListData: "",
  productListError: null,
  productDetail: null,
  productDetailError: null,
  productListFail: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PRODUCTS_LIST_DATA:
      return {
        ...state,
        productListData: action.payload,
        productListError: null,
        productListFail: false
      };
    case PRODUCTS_LIST_ERROR:
      return {
        ...state,
        productListError: action.payload,
        productListFail: true
      };
    case PRODUCT_DETAIL_FETCHED:
      return {
        ...state,
        productDetail: action.payload,
      };
    case PRODUCT_DETAIL_ERROR:
      return {
        ...state,
        productDetailError: action.payload,
      };
    case RESET_PRODUCT_DETAIL:
      return {
        ...state,
        productDetail: null,
        productDetailError: null,
      };
    default:
      return state;
  }
};
