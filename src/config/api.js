// export const BASE_URL = "http://18.221.187.15:8000/";  
export const BASE_URL = "http://18.221.187.15/";


export const authApi = {
  register: "user/register/",
  login: "user/login/",
  companyList: "user/company",
  forgotPass: "user/password/",
  logout: "user/logout/"
};

export const settingsApi = {
  userProfile: "user/data?access_token=",
  leaderBoard: access_token => {
    return `${BASE_URL}user/leaderboard?access_token=${access_token}`;
  },
  getUserJobList : access_token => {
    return `${BASE_URL}user/tasks?access_token=${access_token}`
  },
  getJobDetails: (accessToken, jobId) => {
    return `${BASE_URL}user/tasks/${jobId}?access_token=${accessToken}`
  }
};

export const homeApi = {
  dashboard: "user/dashboard?access_token=",
  referral: access_token => {
    return `${BASE_URL}user/referral?access_token=${access_token}`;
  },
  notification: (userId, access_token) => {
    return `${BASE_URL}notification/${userId}?access_token=${access_token}`;
  }
};

export const QRdata = {
  scanQR: "user/qr/scan/",
  activateProduct: "user/product/activation/",
  splitGetUser: "user/filter?access_token=",
  dontSplit: "user/dont/split/",
  split: "user/split/point/",
  userRewards: "user/rewards?access_token=",
  cancelQr: "user/qr/cancel/",
  dispute: `${BASE_URL}user/dispute/`
};

export const LearnToEarnAPI = {
  videoList: `${BASE_URL}video_list?access_token=`,
  categoryList: `${BASE_URL}category_list?access_token=`
  //API calls are also made internally inside the component in this module.
};

export const ProductsApi = {
  productList: access_token => {
    return `${BASE_URL}product_list_app?access_token=${access_token}`;
  },
  productDetail: (productID, access_token) => {
    return `${BASE_URL}product_list_each/${productID}?access_token=${access_token}`;
  }
};

export const TechSupportApi = {
  submitRequest: `${BASE_URL}user/support/request/`,
  getMerchant: `${BASE_URL}user/merchant?access_token=`,
  getProducts: (access_token, id) =>
    `${BASE_URL}user/merchant/${id}/products?access_token=${access_token}`,
  getDataSheets: (access_token, id) =>
    `${BASE_URL}user/merchant/products/${id}?access_token=${access_token}`,
  getSprayParametersQuestion: access_token => {
    return `${BASE_URL}upc/questions?access_token=${access_token}`;
  },
  getRecommendationData: `${BASE_URL}upc/questions/submit/`,
  rating : `${BASE_URL}upc/userRatig/`
};

export const QrHistoryApi = {
  qrHistoryList: access_token => {
    return `${BASE_URL}user/qr/history?access_token=${access_token}`;
  }
};
export const SelectedQrHistoryApi = {
  selectedQrHistoryApi: (access_token, id) => {
    return `${BASE_URL}user/qr/history/${id}?access_token=${access_token}`;
  }
};

export const ProfileApi = {
  saveProfileApi: () => {
    return `${BASE_URL}user/edit/`;
  },
  getProfileApi: access_token => {
    return `${BASE_URL}user/data?access_token=${access_token}`;
  }
};
//CMS Pages

export const CmsPages = {
  PrivacyPolicy: `${BASE_URL}each_cms/1`,
  TermsAndConditions: `${BASE_URL}each_cms/2`,
  AboutUs: `${BASE_URL}each_cms/3`,
  AboutUsSpanish: `${BASE_URL}each_cms/4`,
  PrivacyPolicySpanish: `${BASE_URL}each_cms/5`,
  TermsAndConditionsSpanish: `${BASE_URL}each_cms/6`
};

//Contest APIS

export const contestAPI = {
  contestList: (userId, accessToken) =>
    `${BASE_URL}contest_list/${userId}?access_token=${accessToken}`,
  contestDetails: (contestId, accessToken) =>
    `${BASE_URL}contest/${contestId}?access_token=${accessToken}`,
  participateContest: () => `${BASE_URL}participate`,
  contestWinner: (contestId, accessToken) =>
    `${BASE_URL}contest_user_board/${contestId}?access_token=${accessToken}`,
  rankList: (contestId, accessToken) =>
    `${BASE_URL}contest_user_board_admin/${contestId}?access_token=${accessToken}`
};

//
